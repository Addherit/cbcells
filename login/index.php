<!DOCTYPE html>
<html lang="en">    
    <?php include "../header.html" ?>    
    <link rel="stylesheet" href="/crm/login/css/login.css">    
    <body>    
        <div class="wrapper fadeInDown">
            <div id="formContent">                

                <!-- Icon -->
                <div class="fadeIn first">
                    <img src="/crm/assets/img/logo.png" id="icon" alt="User Icon"/>
                </div>         
                <!-- Login Form -->
     
                
                <form id="form-login" name="form-login">
                    <input type="text" id="login" class="fadeIn second" name="login" placeholder="User">
                    <input type="password" id="password" class="fadeIn third" name="password" placeholder="Password">
                    <input type="submit" class="fadeIn fourth" value="Log In">
                </form>

                <!-- Remind Passowrd -->
                <div id="formFooter">
                    <a class="underlineHover" href="#">Olvidaste tu Password?</a>
                </div>
            </div>
        </div>        
    </body>
    
    <footer>        
        <script src="/crm/assets/js/core/jquery.min.js"></script>
        <script src="/crm/assets/js/core/popper.min.js"></script>
        <script src="/crm/assets/js/plugins/moment.min.js"></script>   
        <script src="/crm/assets/js/core/bootstrap-material-design.min.js"></script>
        <script src="/crm/assets/js/plugins/bootstrap-notify.js"></script>
        
        <!-- Things that a made -->
        <script src="/crm/assets/js/classes/userSession.js"></script>
        <script src="/crm/assets/js/classes/message.js"></script>
        <script src="/crm/login/js/login.js"></script>
    </footer>
</html> 