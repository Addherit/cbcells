<!DOCTYPE html>
<html lang="en">

<head>
  <?php include '../header.html' ?>
</head>

<body class="">
    <div class="wrapper ">
        <?php include '../sidebar/sidebar.html' ?>
        <div class="main-panel">
        <?php include '../sidebar/navbar.html' ?>
        <div class="content">
            <div class="container-fluid">     
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title ">Historial de movimientos</h4>
                                <p class="card-category">Muestra todos los movimientos hechos por los usuarios.</p>                      
                            </div>
                            <div class="card-body">
                                <div class="table-responsive" style="min-height: 100%">
                                    <table class="table table-hover" id="tbl-historial-movimientos">
                                        <thead class=" text-primary">                            
                                            <th>N°</th>
                                            <th>Usuario</th>
                                            <th>Fecha de movimiento</th>
                                            <th>Tipo de movimiento</th>                           
                                        </thead>
                                        <tbody>                                      
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </div> 
</body>
<?php include '../footer.html' ?>
<script src="js/historial.js"></script>

</html>