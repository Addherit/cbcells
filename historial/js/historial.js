document.addEventListener('DOMContentLoaded', () => {  setRowsHistorial() })

function getHistorial() {

    return fetch('php/select-historial.php')
    .then(data => data.json())
    .then(data => {
        return data;
    })

}

async function setRowsHistorial() {

    const DATA = await getHistorial(); 
    const ROWS = getHistorialRows(DATA);
    document.querySelector('#tbl-historial-movimientos tbody').innerHTML = ROWS;

}

function getHistorialRows(data) {
    let cont = 1;
    let linea = ``
    data.forEach(element => {
        linea += `
        <tr>
            <td>${cont++}</td>
            <td>${element.Usuario}</td>
            <td>${element.Datecreated}</td>
            <td>${element.Mensaje}</td>
        </tr>`
    });
    return linea;

}