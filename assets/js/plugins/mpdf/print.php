<?php
ini_set('max_execution_time', 300); //300 seconds = 5 minutes
set_time_limit(300);
$plantilla_type = $_POST['plantilla_type'];

use Mpdf\Mpdf;
require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__.'/formatos/'.$plantilla_type.'.php';

// este if escoge que stylesheet usar. El background-image es lo unico que cambia.
$plantilla_type == 'medico' || $plantilla_type == 'paciente-celulas' ? $stylesheet = 'celulas' : $stylesheet = 'inmuno';

$css = file_get_contents(__DIR__.'/css/'.$stylesheet.'-style.css');
$mpdf = new Mpdf(["format" => 'Letter']);
$html = $plantilla;
$mpdf->WriteHTML($css, \Mpdf\HTMLParserMode::HEADER_CSS);
$mpdf->WriteHTML($html, \Mpdf\HTMLParserMode::HTML_BODY);

$mpdf->Output('formato.pdf', "F");
