<?php

        $plantilla = '
        <body>
            <br>
            <h1 class="title">'.$_POST["title"].'</h1>
            <br>
            <table class="table">
            
                <tbody>
                    <tr class="fecha">
                        <td colspan="7"></td>
                        <td colspan="3">
                            <p>Fecha</p>
                            <label>'.$_POST["fecha"].'</label>
                            <hr>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <p>Nombres</p>
                            <label>'.$_POST["nombres"].'</label>
                            <hr>
                        </td>
                        <td colspan="5">
                            <p>Apellidos</p>
                            <label>'.$_POST["apellidos"].'</label>
                            <hr>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <p>Cédula Profesional</p>
                            <label>'.$_POST["cedula-profesional"].'</label>
                            <hr>
                        </td>
                        <td colspan="4">
                            <p>Especialidad</p>
                            <label>'.$_POST["especialidad"].'</label>
                            <hr>
                        </td>
                        <td colspan="3">
                            <p>sup / especialidad</p>
                            <label>'.$_POST["sup-especialidad"].'</label>
                            <hr>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <p>Domicilio / Calle y Número / Ciudad / Estado</p>
                            <label>'.$_POST["domicilio"].'</label>
                            <hr>
                        </td>
                        <td colspan="2">
                            <p>Teléfono</p>
                            <label>'.$_POST["telefono"].'</label>
                            <hr>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <p>Domicilio del consultorio</p>
                            <label>'.$_POST["domicilio-consultorio"].'</label>
                            <hr>
                        </td>
                        <td colspan="4">
                            <p>Correo electrónico</p>
                            <label>'.$_POST["email"].'</label>
                            <hr>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <p>Tipo de descuento</p>
                            <label>'.$_POST["descuento"].'</label>
                            <hr>
                        </td>
                        <td colspan="4">
                            <p>Fuente de ingreso</p>
                            <label>'.$_POST["ingreso"].'</label>
                            <hr>
                        </td>
                        <td colspan="4">
                            <p>Vendedor asignado</p>
                            <label>'.$_POST["vendedorAsig"].'</label>
                            <hr>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <p>Nombre fiscal</p>
                            <label>'.$_POST["nombre-fiscal"].'</label>
                            <hr>
                        </td>
                        <td colspan="2">
                            <p>RFC</p>
                            <label>'.$_POST["rfc"].'</label>
                            <hr>
                        </td>
                        <td colspan="2">
                            <p>Régimen fiscal</p>
                            <label>'.$_POST["regimen-fiscal"].'</label>
                            <hr>
                        </td>
                        <td colspan="3">
                            <p>Correo electrónico 1</p>
                            <label>'.$_POST["email-factura"].'</label>
                            <hr>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <p>Dirección completa</p>
                            <label>'.$_POST["direccion-fiscal"].'</label>
                            <hr>
                         </td>
                        <td colspan="4">
                            <p>Correo electrónico 2</p>
                            <label>'.$_POST["email-factura2"].'</label>
                            <hr>
                        </td>
                    </tr>
                </tbody>
            </table>
        </body>
        ';

