<?php

        $plantilla = '
        <body>
            <br>
            <h1 class="title">'.$_POST["title"].'</h1>
            <br>
            <table class="table">
            
                <tbody>
                    <tr class="fecha">
                        <td colspan="7"></td>
                        <td colspan="3">
                            <p>Fecha</p>
                            <label>'.$_POST["fecha"].'</label>
                            <hr>
                        </td>
                    </tr>
                    <tr><td colspan="10"><h1>Datos del médico</h1></td></tr>
                    <tr>                        
                        <td colspan="5">
                            <p>Nombre del médico</p>
                            <label>'.$_POST["medico"].'</label>
                            <hr>
                        </td>
                        <td colspan="5">
                            <p>Cédula Profesional</p>
                            <label>'.$_POST["cedula-profesional"].'</label>
                            <hr>
                        </td>
                    </tr>
                    <tr><td colspan="10"><h1>Datos del paciente</h1></td></tr>
                    <tr>
                        <td colspan="4">
                            <p>Nombres</p>
                            <label>'.$_POST["nombres"].'</label>
                            <hr>
                        </td>
                        <td colspan="4">
                            <p>Apellidos</p>
                            <label>'.$_POST["apellidos"].'</label>
                            <hr>
                        </td>
                        <td colspan="2">
                            <p>Edad</p>
                            <label>'.$_POST["edad"].'</label>
                            <hr>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <p>Diagnostico</p>
                            <label>'.$_POST["diagnostico"].'</label>
                            <hr>
                        </td>
                        <td colspan="4">
                            <p>Sexo</p>
                            <label>'.$_POST["sexo"].'</label>
                            <hr>
                        </td>
                    </tr>
                    <tr><td colspan="10"><h1>Información de la muestra</h1></td></tr>
                    <tr class="fecha">
                        <td colspan="7"></td>
                        <td colspan="3">
                            <p>Fecha</p>
                            <label>'.$_POST["fecha-obtencion"].'</label>
                            <hr>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <p>Composición de la muestra</p>
                            <label>'.$_POST["compsicion-muestra"].'</label>
                            <hr>
                        </td>
                        <td colspan="5">
                            <p>Origen de la muestra</p>
                            <label>'.$_POST["origen-muestra"].'</label>
                            <hr>
                        </td>
                    </tr>                    
                    <tr>
                        <td colspan="6">
                            <p>Nombre del donante</p>
                            <label>'.$_POST["nombre-donante"].'</label>
                            <hr>
                        </td>
                        <td colspan="2">
                            <p>Sexo</p>
                            <label>'.$_POST["sexo-familiar"].'</label>
                            <hr>
                        </td>
                        <td colspan="2">
                            <p>Edad</p>
                            <label>'.$_POST["edad-familiar"].'</label>
                            <hr>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <p>Código referencia</p>
                            <label>'.$_POST["codigo-referencia"].'</label>
                            <hr>
                        </td>
                        <td colspan="3">
                            <p>1ra Dosis</p>
                            <label>'.$_POST["primera-dosis"].'</label>
                            <hr>
                        </td>
                        <td colspan="3">
                            <p>2da Dosis</p>
                            <label>'.$_POST["segunda-dosis"].'</label>
                            <hr>
                        </td>
                    </tr>                                  
                </tbody>
            </table>
        </body>
        ';

