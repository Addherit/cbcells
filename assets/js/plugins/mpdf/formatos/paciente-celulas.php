<?php

        $plantilla = '
        <body>
            <br>
            <h1 class="title">'.$_POST["title"].'</h1>
            <br><br>
            <table class="table">            
                <tbody>
                    <tr class="fecha">
                        <td colspan="8"></td>
                        <td colspan="2">
                            <p>Fecha</p>
                            <label>'.$_POST["fecha"].'</label>
                            <hr>
                        </td>
                    </tr>
                    <tr><td colspan="10"><h1>Datos del médico</h1></td></tr>
                    <tr>                        
                        <td colspan="8">
                            <p>Nombre del médico</p>
                            <label>'.$_POST["medico"].'</label>
                            <hr>
                        </td>
                        <td colspan="2">
                            <p>Cédula Profesional</p>
                            <label>'.$_POST["cedula-profesional"].'</label>
                            <hr>
                        </td>
                    </tr>
                    <tr><td colspan="10"><h1>Datos del paciente</h1></td></tr>
                    <tr>
                        <td colspan="4">
                            <p>Nombres</p>
                            <label>'.$_POST["nombres"].'</label>
                            <hr>
                        </td>
                        <td colspan="4">
                            <p>Apellidos</p>
                            <label>'.$_POST["apellidos"].'</label>
                            <hr>
                        </td>
                        <td colspan="2">
                            <p>Edad</p>
                            <label>'.$_POST["edad"].'</label>
                            <hr>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <p>Diagnostico</p>
                            <label>'.$_POST["diagnostico"].'</label>
                            <hr>
                        </td>
                        <td colspan="2">
                            <p>Sexo</p>
                            <label>'.$_POST["sexo"].'</label>
                            <hr>
                        </td>
                    </tr>
                    <tr><td colspan="10"><h1>Estudio de la muestra</h1></td></tr>
                    <tr>
                        <td colspan="7"></td>
                        <td colspan="3">
                            <p>Fecha</p>
                            <label>'.$_POST["fecha-obtencion"].'</label>
                            <hr>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="10">
                            <p>Comentarios</p>
                            <label>'.$_POST["comentarios"].'</label>
                            <hr>
                        </td>                        
                    </tr>                                                                  
                </tbody>
            </table>
        </body>
        ';

