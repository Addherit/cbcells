
document.addEventListener('DOMContentLoaded', () => { validateSession() })

function redirectViewEditUser() {
    const ID_USER = localStorage.getItem('user_id');
    window.location = `/crm/usuarios/editar-usuario/index.php?userId=${ID_USER}`;
}

function validateSession() {

    fetch('/crm/check-login.php')
    .then(data => data.json())
    .then(data => {
        if(data.mensaje == 'true') { 
            localStorage.clear();
        }
        let session = new UserSession();
        session.validateStartedSession();    
    })
    
}

function logOut() {
    
    let session = new UserSession();
    session.logOut() 

}