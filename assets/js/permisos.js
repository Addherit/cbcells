document.addEventListener('DOMContentLoaded',() => {
    setPermisosByUser();
    
})

function getPermisosByUser() {
    
    const FORM_DATA = new FormData()
    FORM_DATA.append('tipo_usuario', localStorage.getItem('user_type'))
    return fetch('/crm/assets/php/permisos/select-permisos.php', { method: 'POST', body: FORM_DATA}) 
    .then(data => data.json())
    .then(data => {
        return data;
    })

}

async function setPermisosByUser() {
    
    const PERMISOS = await getPermisosByUser();

    PERMISOS[0].Dasboard == 0 ? document.querySelector('#m-dashboard').hidden = true : document.querySelector('#m-dashboard').hidden = false

    // celulas troncales
    PERMISOS[0].Ventas == 0 ? document.querySelector('#m-ventas').hidden = true : document.querySelector('#m-ventas').hidden = false
    PERMISOS[0].Medicos == 0 ? document.querySelector('#m-medicos').hidden = true : document.querySelector('#m-medicos').hidden = false
    PERMISOS[0].CControlMante == 0 ? document.querySelector('#c-mantenimiento').hidden = true : document.querySelector('#c-mantenimiento').hidden = false
    PERMISOS[0].Citems == 0 ? document.querySelector('#c-inventario').hidden = true : document.querySelector('#c-inventario').hidden = false
    PERMISOS[0].CPacientes == 0 ? document.querySelector('#c-pacientes').hidden = true : document.querySelector('#c-pacientes').hidden = false
    PERMISOS[0].Insumos == 0 ? document.querySelector('#m-Insumos').hidden = true : document.querySelector('#m-Insumos').hidden = false

    //inmunoterapia 
    PERMISOS[0].Inmunologia == 0 ? document.querySelector('#m-inmunoterapia').hidden = true : document.querySelector('#m-inmunoterapia').hidden = false
    PERMISOS[0].IControlMante == 0 ? document.querySelector('#i-mantenimiento').hidden = true : document.querySelector('#i-mantenimiento').hidden = false
    PERMISOS[0].IPacientes == 0 ? document.querySelector('#i-pacientes').hidden = true : document.querySelector('#i-pacientes').hidden = false
    PERMISOS[0].IControlInven == 0 ? document.querySelector('#i-control-inventario').hidden = true : document.querySelector('#i-control-inventario').hidden = false
    
    PERMISOS[0].Usuarios == 0 ? document.querySelector('#m-usuarios').hidden = true : document.querySelector('#m-usuarios').hidden = false

    PERMISOS[0].Historial == 0 ? document.querySelector('#historial').hidden = true : document.querySelector('#historial').hidden = false
} 