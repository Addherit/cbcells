class Alertas {

    getCantidades() {

        //obtener cantidades de todas los tipos de alertas
        const FORM_DATA = new FormData();
        FORM_DATA.append('type', localStorage.getItem('user_type'))
        FORM_DATA.append('userid', localStorage.getItem('user_id'))

        return fetch('/crm/assets/php/sistemas-de-alertas/select-alertas.php', {method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
            return data;
        })
        
    }

    async setCantidades() {

        const DATA = await this.getCantidades();
        this.getCantAllAlerts(DATA);
        DATA.forEach(notify => {

            if (notify.Estado == 'Pendiente') {
                switch (notify.Ntype) {
                    case '1':
                        // Nuevo medico
                        this.validateIfExistNotifys('notify-new-medic', notify.Ids, `/crm/celulas troncales/medico/lista-medicos/index.php?ids=${notify.Ids}&ntype=${notify.Ntype}`);
                        localStorage.setItem(`ntype${notify.Ntype}`, this.getCantByAlert(notify.Ids))
                        break;
                    case '2':
                        // Nueva venta
                        this.validateIfExistNotifys('notify-new-sell', notify.Ids, `/crm/ventas/lista-ventas/index.php?ids=${notify.Ids}&ntype=${notify.Ntype}`);
                        localStorage.setItem(`ntype${notify.Ntype}`, this.getCantByAlert(notify.Ids))
                        break;
                    case '3':
                        // Reasignación de medico
                        this.validateIfExistNotifys('notify-edit-medic', notify.Ids, `/crm/celulas troncales/medico/lista-medicos/index.php?ids=${notify.Ids}&ntype=${notify.Ntype}`);
                        localStorage.setItem(`ntype${notify.Ntype}`, this.getCantByAlert(notify.Ids))
                        break;
                    case '4':
                        // Cambio de estatus en orden
                        this.validateIfExistNotifys('notify-edit-sell', notify.Ids, `/crm/ventas/lista-ventas/index.php?ids=${notify.Ids}&ntype=${notify.Ntype}`);
                        localStorage.setItem(`ntype${notify.Ntype}`, this.getCantByAlert(notify.Ids))
                        break;
                    case '5':
                        // Registro de nueva información de contacto
                        this.validateIfExistNotifys('notify-contact-web', notify.Ids, `#`);
                        localStorage.setItem(`ntype${notify.Ntype}`, this.getCantByAlert(notify.Ids))
                        break;
                }
                
            }
        });

    }

    getCantByAlert(string) {

        let cant_alert = 0;
        const ARRAY_IDS = string.split(',');
        cant_alert = cant_alert + parseInt(ARRAY_IDS.length);
        return cant_alert;

    }

    getCantAllAlerts(data) {

        let cant_alerts = 0;
        const NOTIFY_GENERAL = document.querySelector('#notify-general');
        data.forEach(notify => {
            if (notify.Estado == 'Pendiente') {
                const ARRAY_NOTIFY = notify.Ids.split(',');
                cant_alerts = cant_alerts + parseInt(ARRAY_NOTIFY.length);
            }
        });

        if (cant_alerts > 0) {
            NOTIFY_GENERAL.removeAttribute('hidden');
            NOTIFY_GENERAL.innerHTML = cant_alerts;
        } else {
            NOTIFY_GENERAL.setAttribute('hidden', true);
        }

    }

    validateIfExistNotifys(name_id, string_ids, url) {

        const CANT_NOTIFYS = this.getCantByAlert(string_ids);
        if (CANT_NOTIFYS > 0) {
            document.querySelector(`#${name_id}`).innerHTML = CANT_NOTIFYS;
            document.querySelector(`#${name_id}`).removeAttribute('hidden');
            document.querySelector(`#${name_id}`).parentElement.setAttribute('href', url);
        } else {
            document.querySelector(`#${name_id}`).setAttribute('hidden');
        }
  
    }

    highlightIds(table_id) {

        const PARAMS_URL = new URLSearchParams(window.location.search);
        let string_ids = PARAMS_URL.get('ids');
        let ntype = PARAMS_URL.get('ntype');
        const TRS = document.querySelectorAll(`#${table_id} tbody tr`);

        if (string_ids) {
            string_ids.split(',').forEach(id => {
                TRS.forEach(tr => {
                    tr.children[2].textContent == id ? tr.classList.add('background-td-editable') : tr;
                });
            });
            this.updateStatusNotify(ntype, localStorage.getItem('user_type'), localStorage.getItem('user_id'));
        }
       

    }

    updateStatusNotify(ntype, user_type, user_id) {

        const FORM_DATA = new FormData();
        FORM_DATA.append('ntype', ntype);
        FORM_DATA.append('type', user_type);
        FORM_DATA.append('userid', user_id);
        fetch('/crm/assets/php/sistemas-de-alertas/update-estado-alerta.php', { method: 'POST', body: FORM_DATA })
        .then(data => {
            this.setCantidades();    
        })

    }

    async validateNewNotify() {

        const DATA = await this.getCantidades();
        DATA.forEach(notify => {
            const NOTIFY_LOCAL = localStorage.getItem(`ntype${notify.Ntype}`);
            const NEW_VALUE = this.getCantByAlert(notify.Ids);

            if (NOTIFY_LOCAL) {
                if (NOTIFY_LOCAL < NEW_VALUE) {
                    localStorage.setItem(`ntype${notify.Ntype}`, NEW_VALUE);
                    const MSN = new Message('info', notify.Mensaje);
                    MSN.showMessage();
                    this.setCantidades();
                }
            }
            
        });
    }

}

const alerta = new Alertas();
alerta.setCantidades();

setTimeout(() => {
    setInterval(() => {
        alerta.validateNewNotify();
    }, 5000);
}, 1000);

