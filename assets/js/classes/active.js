class Active {

    constructor(url) {
        this.url = url
    }

    addActiveClass() {

        const item = document.querySelector(`[href='${this.url}']`)
        if (item != null) {
            item.parentNode.classList.add('active');
            if (item.parentNode.parentNode.id.substring(0,3) == 'sub') {
                item.parentNode.parentNode.classList.add('show');
            }
        }

    }

}

const item = new Active(window.location.pathname);
item.addActiveClass();
