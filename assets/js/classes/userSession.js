class UserSession {

    validateStartedSession() {
        
        const url = window.location.pathname;
        const userID = typeof(localStorage.getItem('user_id'));
        if (userID == 'string' && url == '/crm/login/') {
            switch (localStorage.getItem('user_type')) {
                
                case '4':
                    window.location = '/crm/area de inmunoterapia/nuevo-tratamiento/nueva-inmunoterapia/index.php';
                    break;
                case '5':
                    window.location = '/crm/celulas troncales/control-mantenimiento/lista-mantenimientos/index.php';
                    break;
                case '2':
                    window.location = '/crm/ventas/lista-ventas/index.php';
                    break;
                default:
                    window.location = '/crm/dashboard/index.php';
                    break;
            }
        } else {            
            if (userID != 'string' && url != '/crm/login/') {
                window.location = '/crm/';
            }
        }

    }

    logIn() {

        const form = new FormData(document.querySelector("#form-login"));    
        fetch("/crm/login/php/secure_login.php", {method: 'POST', body: form })
        .then(data => data.json())
        .then(data => {
            if (!data.mensaje) {                
                localStorage.clear();              
                this.createLocalVariables(data);
                location.reload();
            } else {
                const msn = new Message(data.type, data.mensaje);
                msn.showMessage(); 
            }
        })

    }

    createLocalVariables(data) {

        // limpias Localstorage para crear las variables con la información del usuario logueado    
        localStorage.setItem('user_id', data.userId);
        localStorage.setItem('user_type', data.Tipo_usuario);
        localStorage.setItem('all_name', `${data.Nombre} ${data.Apellido}`);
        localStorage.setItem('user', data.name);
        localStorage.setItem('email', data.correo);
                
    }

    logOut() {
        localStorage.clear();
        location.reload();
    }
    
}



