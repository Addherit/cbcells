class Message {
    
    constructor(type, message) {
                
        this.message = message;        
        this.type = type; // success/danger/info/warning

    }

    showMessage() {

        const ICON = this.selectIconMessage();
        
        const options = {        
            message: `${ICON} ${this.message}`
        }
        const settings = {
            type: this.type,        
            template: 
                '<div data-notify="container" class="col-11 col-md-4 alert alert-{0} animated fadeInDown" role="alert" data-notify-position="top-right" style="margin: 15px auto; position: fixed; transition: all 0.5s ease-in-out 0s; z-index: 1031; top: 20px; right: 20px; display: flex; align-items: center;">' +
                    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss" style="position: absolute; right: 10px; top: 50%; margin-top: -9px; z-index: 1033;">' +
                        '<i class="material-icons">close</i>' +
                    '</button>' +                    
                    '<span data-notify="message">{2}</span>' +                                       
                '</div>' 
        }

        $.notify(options,settings);

    }

    selectIconMessage() {
        
        switch (this.type) {
            case 'success':
                return '<i class="fa fa-check-circle" aria-hidden="true" style="color: white"></i>';
                break;
            case 'danger':
                return '<i class="fa fa-exclamation-circle" aria-hidden="true" style="color: white"></i>';
                break;        
            default:
                return '<i class="fa fa-exclamation-triangle" aria-hidden="true" style="color: white"></i>';
                break;
        }        
    }

}


