class DeleteFile {
    constructor(url) {

        this.url = url;

    }
    deteleFile() {

        // Se pone un setTimeOut para que encuentr el pdf antes de eliminarlo.
        setTimeout(() => {
            const FORM_DATA = new FormData()
            FORM_DATA.append('url', this.url)
            fetch('/crm/assets/php/deleteFile.php', {method: 'POST', body: FORM_DATA })
        }, 5000);
       

    }

}