select o.OrderId,o.Estado,o.Notas, concat(u.nombre ,' ',u.apellido) as 'Vendedor' , 
(select SUM(Total) from ItemsOrd i where i.OrderId = o.OrderId) as 'Total',
o.DateCreated as 'Fecha', concat(d.Nombre ,' ',d.Apellido) as 'Medico' 
from Ordenes o 
inner join Users u on u.userId = o.VendedorId 
inner join Doctores d on d.DocId = o.MedId 

--query para sacar los usuarios y el total de sus ventas
select concat(u.nombre ,' ',u.apellido) as 'Vendedor', SUM(i.Total) as 'Total'
from Ordenes o
inner join ItemsOrd i on i.OrderId = o.OrderId
inner join Users u on u.userId = o.VendedorId
group by o.VendedorId

-- query para sacar lo vendido por sku
Select inv.Sku,inv.Nombre,count(*) as 'Vendidos', SUM(i.Total) as 'Total' 
from ItemsOrd i 
inner join Inventario inv on inv.Id = i.ItemId 
group by inv.Sku,inv.Nombre

--query ranking ventas por medico
select concat(d.Nombre ,' ',d.Apellido) as 'Medico', SUM(i.Total) as 'Total' 
from Doctores d 
inner join Ordenes o on o.MedId = d.DocId 
LEFT join ItemsOrd i on i.OrderId = o.OrderId group by d.DocId 
--fecha,medico,total
--costo de envio,guia, nombre medico,

--sp de ventas sku
--CALL `SkuV`();


--nombre y codigo de cliente 
--producto a armar y producto a desarmar
--firmas si, y la ultima de tj como supervisor normal.