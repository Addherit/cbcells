<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">    
        <style>
            .title {
                color:white;
                background-color: #64bc44;
                text-align: center;
                font-family: arial;
                margin-left: 10%;
                margin-right: 10%;            
            }
            .logo {
                width: 20%;
                margin-left: 40%;
                margin-top: 10px;
            }
            .paragraph {                        
                text-align: center;
                margin-left: 25%;
                margin-right: 25%;
            }
            .button {
                background-color: red;
                border: none;
                margin-left: 35%;
                margin-right: 35%;
                text-align: center;
                padding: 15px 32px;            
            }
            .button a {
                color: white;
                text-decoration: none;
            }

        </style>
    </head>
    <body>
        <div class="container">
            <h1 class="title" >¡Cambio de estatus<br>en orden!</h1>    
            <br><br>
            <h3 class="paragraph">Se cambió el estatus en la orden. Clic en el botón para poder visualizarla.</h3>
            <h2 class="button"><a href="http://www.cbcells.mx/crm/ventas/editar'venta/index.php">Ver venta</a></h2>
        </div>
    </body>
</html>