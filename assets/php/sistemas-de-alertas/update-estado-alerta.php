<?php 
header('Content-type: application/json');
include_once('../../db/conexion.php');

if($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        //vars from form
        $userid = $_POST['userid'];
        $type = $_POST['type'];
        $ntype = $_POST['ntype'];

        // sql query for INSERT INTO Medicos
        switch ($type) {
            case 1:
                $sql = "update Notificaciones set EstadoAdmin='Visto' where Ntype='$ntype'";
            break;
            case 2:
                $sql = "update Notificaciones set Estado='Visto' where Ntype='$ntype' and UserId ='$userid'";
            break;
            case 3:
                $sql = "update Notificaciones set EstadoSupervisor ='Visto' where Ntype='$ntype'";
            break;
        }

        // Performs the $sql query on the server to insert the values
        if (mysqli_query($conn, $sql)) {
            $result = ['type' => "success", 'msn' => "estado cambiado"];
        } else {
            $result = ['type' => "danger", 'msn' => "problema del query"];
        }
        mysqli_close($conn);
    } catch (PDOException  $e) {
        $result = ['type' => "danger", 'msn' => "problema de la conexion"];
    }

    echo json_encode($result);
}
?>