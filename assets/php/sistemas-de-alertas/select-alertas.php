<?php 
header('Content-type: application/json');
include_once('../../db/conexion.php');

try {
  $userid = $_POST['userid'];
  $type = $_POST['type'];

  $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $con->exec("set names utf8");

  $final_data=[];
  $sql =" ";
  $union =" union all ";
  switch ($type) {
    case 1: //admin
      //nuevo medico,nueva venta,nuevo informe contacto
      $query = "SELECT count(*)Num,`Mensaje`,`EstadoAdmin` as 'Estado',GROUP_CONCAT(IdAccion SEPARATOR ',') Ids,`Ntype` FROM Notificaciones where EstadoAdmin = 'Pendiente' and Ntype= 1 group by `Mensaje`,`EstadoAdmin`,`Ntype` ";
      $sql .=$query;
      $sql .=$union;
      $query = " SELECT count(*)Num,`Mensaje`,`EstadoAdmin` as 'Estado',GROUP_CONCAT(IdAccion SEPARATOR ',') Ids,`Ntype` FROM Notificaciones where EstadoAdmin = 'Pendiente' and Ntype= 2 group by `Mensaje`,`EstadoAdmin`,`Ntype` ";
      $sql .=$query;
      $sql .=$union;
      $query = " SELECT count(*)Num,`Mensaje`,`EstadoAdmin` as 'Estado',GROUP_CONCAT(IdAccion SEPARATOR ',') Ids,`Ntype` FROM Notificaciones where EstadoAdmin = 'Pendiente' and Ntype= 5 group by `Mensaje`,`EstadoAdmin`,`Ntype` ";
      $sql .=$query;
      break;
    case 2: //vendedor
      //nueva venta,reasignacion medico, cambio estatus
      $query =" SELECT count(*)Num,`Mensaje`,`Estado`,GROUP_CONCAT(IdAccion SEPARATOR ',') Ids,`Ntype` FROM Notificaciones where `Estado` = 'Pendiente' and UserId=$userid and Ntype= 2 group by `Mensaje`,`Estado`,`Ntype` ";
      $sql .=$query;
      $sql .=$union;
      $query =" SELECT count(*)Num,`Mensaje`,`Estado`,GROUP_CONCAT(IdAccion SEPARATOR ',') Ids,`Ntype` FROM Notificaciones where `Estado` = 'Pendiente' and UserId=$userid and Ntype= 3 group by `Mensaje`,`Estado`,`Ntype` ";
      $sql .=$query;
      $sql .=$union;
      $query = " SELECT count(*)Num,`Mensaje`,`Estado`,GROUP_CONCAT(IdAccion SEPARATOR ',') Ids,`Ntype` FROM Notificaciones where `Estado` = 'Pendiente' and UserId=$userid  and Ntype= 4 group by `Mensaje`,`Estado`,`Ntype`  ";
      $sql .=$query;
      break;
    case 3://supervisor
      //nueva venta
      $sql = "  SELECT count(*)Num,`Mensaje`,`EstadoSupervisor` as 'Estado',GROUP_CONCAT(IdAccion SEPARATOR ',') Ids,`Ntype` FROM Notificaciones where EstadoSupervisor = 'Pendiente' and Ntype= 2 group by `Mensaje`,`EstadoAdmin`,`Ntype` ";
      break;
    case 4://inmunopterapia
      //sin notificaciones
      $sql = "SELECT * FROM Notificaciones where Estado = 'Pendiente' and UserId = '$userid'";
      break;
    case 5://control celulas
      //sin notificaciones
      $sql = "SELECT * FROM Notificaciones where Estado = 'Pendiente' and UserId = '$userid'";
      break;
  }

  $datos = $con->query($sql)->fetchAll(PDO::FETCH_ASSOC );
  foreach($datos as $row){
    $final_data[] = $row;
  }
   echo json_encode($final_data);
} catch(PDOException $e) {
    echo $result = ["mensaje" => "Error: ".$e->getMessage()];
}
?>