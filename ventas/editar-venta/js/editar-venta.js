
document.addEventListener('DOMContentLoaded', () => { 
    permisosDeUsuario();
    setVenta();
    validateCollapseDatosFiscales();
    
    
    
})

document.querySelector('#check-factura-electronica').addEventListener('change', () => { validateCollapseDatosFiscales() });
document.querySelector('#return-to').addEventListener('click', () => { returnToListVentas() });

document.querySelector('#btn-new-parcialidad').addEventListener('click', (e) => { e.preventDefault(); insertarParcialidad() });
document.querySelector('#btn-autorizar').addEventListener('click', () => { autorizarVenta() });
document.querySelector('#btn-concluir').addEventListener('click', () => { concluirVenta() });
document.querySelector('#btn-finalizar').addEventListener('click', () => { cerrarVenta() });
document.querySelector('#btn-cancelar').addEventListener('click', () => { cancelarVenta() });
document.querySelector('#btn-autorizar-parcialidad').addEventListener('click', () => { autorizarParcialidad() });
document.querySelector('#btn-cancelar-parcialidad').addEventListener('click', () => { cancelarParcialidad() });
document.querySelector('#btn-sumar-costo').addEventListener('click', () => { sumarCostoEnvio() });
document.querySelector('#btn-restar-costo').addEventListener('click', () => { restarCostoEnvio() });

// PARIALIDAD DE PROMOCION
document.querySelector('#btn-new-par-promo').addEventListener('click', () => { createParcialidadPromo() })
document.querySelector('#btn-auto-par-promo').addEventListener('click', () => { autorizeParcialidadPromo() })


function getVenta() {
    
    const PARAMS_URL = new URLSearchParams(window.location.search);
    const FORM_DATA = new FormData();

    FORM_DATA.append('id_venta', PARAMS_URL.get('venta_id') );
    return fetch('php/select-header.php', {method: 'POST', body: FORM_DATA})
    .then(data => data.json())
    .then(data => {
        return data
    })

}

function getDetalle() {

    const PARAMS_URL = new URLSearchParams(window.location.search);
    const FORM_DATA = new FormData();

    FORM_DATA.append('OrderId', PARAMS_URL.get('venta_id') );
    return fetch('php/select-items-venta.php', {method: 'POST', body: FORM_DATA})
    .then(data => data.json())
    .then(data => {
        return data
    })

}

async function setVenta() {

    const DATA = await getVenta();
    const ITEMS = await getDetalle();
    setCabecera(DATA[0]);
    setDetalle(ITEMS, DATA[0].Parcial, DATA[0].Estado);

    if(DATA[0].Parcial == 'si') {        
        setPacialidades();
    }

    if (DATA[0].VendedorId == localStorage.getItem('user_id')) {
        // si el user logueado hizo la venta, se ocultarán los botones de autorizarParcialidad, concluir, finalizar y autorizar parcialidad
        document.querySelector('#btn-concluir').hidden = true;
        document.querySelector('#btn-finaliza').hidden = true;
        document.querySelector('#btn-autorizar').hidden = true;
        document.querySelector('#btn-autorizar-parcialidad').hidden = true;
        
    }

}

function addClassIsFilled() {

    //funcion para hacer el efecto de focus en input de datos
    const DIVS = document.querySelectorAll('.bmd-form-group');
    DIVS.forEach(element => {
        element.classList.add('is-filled');        
    });

}

function setCabecera(data) {

    addClassIsFilled();

    document.querySelector('[name=fecha]').value = data.DateCreated
    document.querySelector('[name=estatus]').value = data.Estado
    document.querySelector('[name=nombre-medico]').value = data.Medico
    document.querySelector('[name=email]').value = data.Correo
    document.querySelector('[name=cedula-profesional]').value = data.Cedula
    document.querySelector('[name=domicilio]').value = data.Domicilio
    document.querySelector('[name=telefono]').value = data.Telefono
    document.querySelector('[name=descuento]').value = data.Descuento
    document.querySelector('[name=guia]').value = data.Guia;
    document.querySelector('[name=vendedor]').value = data.Vendedor
    document.querySelector('[name=paciente]').value = data.Paciente
    document.querySelector('[name=patologia]').value = data.Patologia
    document.querySelector('[name=cuenta-bancaria]').value = data.Cuenta;
    
    document.querySelector('[name=tratamiento]').value = data.Tratamiento
    let pagado = false; 
    data.Pagado == 'true' ? pagado = true : pagado;
    document.querySelector('[name=pagado-bool]').checked = pagado
    document.querySelector('[name=costo-envio]').value = formatter.currencyFormat(data.Costo_env);
    document.querySelector('[name=venta-total]').value = formatter.currencyFormat(data.VentaTotal);

    // //datos fiscales
    let factura = false; 
    data.Factura == 'true' ? factura = true : factura;
    document.querySelector('#check-factura-electronica').checked = factura
    document.querySelector('[name=nombre-fiscal]').value = data.Fiscal_name
    document.querySelector('[name=rfc]').value = data.Rfc
    document.querySelector('[name=regimen-fiscal]').value = data.Regimen_fiscal
    document.querySelector('[name=direccion-fiscal]').value = data.Direccion_completa
    document.querySelector('[name=email-factura]').value = data.Email_factura
    document.querySelector('[name=email-factura2]').value = data.SegundoMail
    
    if (data.Parcial == 'no') {
        document.querySelector('[name=observaciones').value = data.Notas;
        document.querySelector('#col-parcialidades').hidden = true;
    } else {
        // document.querySelector('#col-parcialidades').hidden = false;
    }

    permisosByStatus();
    validateCollapseDatosFiscales();

}

function setDetalle(items, parcial, status) {
   
    let lineas = '';
    let tdEntrega = '';
    let btnDelete = '<td></td>'
    let tr = '<tr> <span>';
    const RANGE_OPTIONS = setOptionsCantEtga();
    if (status == 'Autorizado') {
        btnDelete = `<td><a href="#" class="text-danger btn-tbl-accion" onclick="deleteRow(this)"><span class="material-icons">delete</span>Eliminar</a></td>`
    }

    items.forEach(item => {
        item.Promo == 'si' ? tr = `<tr>` : tr
        parcial == 'si' ? 
            tdEntrega = `<td class="text-center background-td-editable" onchange="getRest(this)"><select>${RANGE_OPTIONS}</select></td>` : 
            tdEntrega = `<td class="text-center" style="cursor: pointer" onclick="getDetallePromo('${item.ItemId}')" title="Ver detalle de la promoción" data-toggle="tooltip" data-placement="bottom">${item.Entregado}</td>`;
        lineas += `            
            ${tr}           
                <td hidden id="td-id">${item.Id}</td>
                <td hidden id="td-item-id">${item.ItemId}</td>
                <td style="cursor: pointer" onclick="getDetallePromo('${item.ItemId}')" title="Ver detalle de la promoción" data-toggle="tooltip" data-placement="bottom">${item.Sku}</td>
                <td style="cursor: pointer" onclick="getDetallePromo('${item.ItemId}')" title="Ver detalle de la promoción" data-toggle="tooltip" data-placement="bottom">${item.Concepto}</td>
                <td class="text-right text-primary" style="cursor: pointer" onclick="getDetallePromo('${item.ItemId}')" title="Ver detalle de la promoción" data-toggle="tooltip" data-placement="bottom">
                    <span class="material-icons" style="vertical-align: text-top; font-size: 18px !important">attach_money</span>
                    <span>${formatter.currencyFormat(item.Precio)}</span>
                </td>
                <td class="text-center" style="cursor: pointer" onclick="getDetallePromo('${item.ItemId}')" title="Ver detalle de la promoción" data-toggle="tooltip" data-placement="bottom">${item.Cantidad_Original}</td>
                <td class="text-center" style="cursor: pointer" onclick="getDetallePromo('${item.ItemId}')" title="Ver detalle de la promoción" data-toggle="tooltip" data-placement="bottom">${item.Descuento}</td>
                ${tdEntrega}
                <td class="text-center" style="cursor: pointer" onclick="getDetallePromo('${item.ItemId}')" title="Ver detalle de la promoción" data-toggle="tooltip" data-placement="bottom">${item.Cantidad_Original - item.Entregado}</td>
                <td class="text-right text-success" style="cursor: pointer" onclick="getDetallePromo('${item.ItemId}')" title="Ver detalle de la promoción" data-toggle="tooltip" data-placement="bottom">
                    <span class="material-icons" style="vertical-align: text-top; font-size: 18px !important">attach_money</span>
                    <span>${formatter.currencyFormat(item.Total)}</span>
                </td>
                ${btnDelete}                
            </tr>`
    });    
    document.querySelector('#tbl-lista-items tbody').innerHTML = lineas;
    $('[data-toggle="tooltip"]').tooltip();

}

async function setPacialidades() {

    const ITEMS_PARCIALES = await getParialidades();
    let lineas = '';
    ITEMS_PARCIALES.forEach(item => {
        lineas += `
            <tr onclick="openModalDetalle(this)" style="cursor: pointer">
                <td>${item.Id}</td>
                <td>${item.Vendedor}</td>
                <td>${item.Datecreated}</td>
                <td>${item.Observacion}</td>
                <td>${item.Estado}</td>
            </tr>
        `;
    });

    document.querySelector('#tbl-lista-parcialidades-all tbody').innerHTML = lineas;
}

async function openModalDetalle(tr) {

    $("#modal-detalle").modal();
    const PARCIALIDADES = await getParcialidadById(tr.children[0].textContent);
    document.querySelector('#id-parcialidad-modal').innerHTML = tr.children[0].textContent;
    let lineas = '';

    PARCIALIDADES.forEach(item => {
        lineas += 
        `
            <tr>
                <td>${item.SKu}</td>
                <td>${item.Concepto}</td>
                <td class="text-right text-primary">
                    <span class="material-icons" style="vertical-align: text-top; font-size: 18px !important">attach_money</span>
                    <span>${formatter.currencyFormat(item.Precio)}</span>
                </td>
                <td class="text-center">${item.Cantidad_Original}</td>
                <td class="text-center">${item.Descuento}</td>
                <td class="text-center">${item.Cantidad}</td>
                <td class="text-center">${item.Cantidad_Original - item.Entregado}</td>
                <td class="text-right text-success">
                    <span class="material-icons" style="vertical-align: text-top; font-size: 18px !important">attach_money</span>
                    <span>${formatter.currencyFormat(item.Total)}</span>
                </td>
            </tr>
        `;
    });

    document.querySelector("#tbl-lista-items-parcial-id tbody").innerHTML = lineas; 

}

function getParcialidadById(id_parcialidad) {

    const FORM_DATA = new FormData()
    FORM_DATA.append('ParcialId', id_parcialidad);
    return fetch('php/select-items-parcial.php', {method: 'POST', body: FORM_DATA})
    .then(data => data.json())
    .then(data => {
        return data
    })

}

function getParialidades() {

    const PARAMS_URL = new URLSearchParams(window.location.search);
    const FORM_DATA = new FormData();

    FORM_DATA.append('OrderId', PARAMS_URL.get('venta_id') );
    return fetch('php/select-parciales.php', {method: 'POST', body: FORM_DATA})
    .then(data => data.json())
    .then(data => {
        return data
    })
    
}

function validateCollapseDatosFiscales() {

    const DIV_DATOS_FISCALES = document.querySelector('#div-datos-fiscales');
    const CHECK_DATOS_FISCALES = document.querySelector('#check-factura-electronica');
    CHECK_DATOS_FISCALES.checked  == true ? DIV_DATOS_FISCALES.classList.add('show') : DIV_DATOS_FISCALES.classList.remove('show');

}


function returnToListVentas() {
    window.location = '/crm/ventas/lista-ventas/index.php';
}

function autorizarVenta() {

    const PARAMS_URL = new URLSearchParams(window.location.search);
    const FORM_DATA = new FormData();

    FORM_DATA.append('OrderId', PARAMS_URL.get('venta_id') );
    fetch('php/update-autorizado.php', {method: 'POST', body: FORM_DATA})
    .then(data => data.json())
    .then(data => {
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
    })

}

function concluirVenta() {

    const PARAMS_URL = new URLSearchParams(window.location.search);
    const FORM_DATA = new FormData();

    FORM_DATA.append('OrderId', PARAMS_URL.get('venta_id') );
    fetch('php/update-concluir.php', {method: 'POST', body: FORM_DATA})
    .then(data => data.json())
    .then(data => {
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
    })

}

function cerrarVenta() {

    const PARAMS_URL = new URLSearchParams(window.location.search);
    const FORM_DATA = new FormData();

    FORM_DATA.append('OrderId', PARAMS_URL.get('venta_id') );
    fetch('php/update-cerrado.php', {method: 'POST', body: FORM_DATA})
    .then(data => data.json())
    .then(data => {
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
    })

}

function insertarParcialidad() {
    
    const PARAMS_URL = new URLSearchParams(window.location.search);
    const ARRAY_ROWS = creatArrayOfRows();
    const DATA ={
        "parcialidad":[
            {
                id: PARAMS_URL.get('venta_id'),
                observaciones: document.querySelector('[name=observaciones]').value
            },
            ARRAY_ROWS
        ]
    };
    fetch('php/insert-new-parcialidad.php', { method: 'POST', body: JSON.stringify(DATA) })
    .then(data => data.json())
    .then(data => {
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
        setPacialidades();
    });

}

function creatArrayOfRows() {

    let rows = document.querySelectorAll('#tbl-lista-items tbody tr');
    let all_arrays = [];
    rows.forEach(row => {
        let array = {
            id: row.children[0].textContent,
            itemId: row.children[1].textContent,
            sku: row.children[2].textContent,
            concepto: row.children[3].textContent,
            precio: formatter.numberFormat(row.children[4].children[1].textContent),
            cant: row.children[5].textContent,
            dcto: row.children[6].textContent,
            etga: row.children[7].children[0].value,
            rest: row.children[8].textContent,
            total: formatter.numberFormat(row.children[9].children[1].textContent),
        }
        all_arrays.push(array);
    });
    return all_arrays;

}

function autorizarParcialidad() {

    const ID_PARCIALIDAD = document.querySelector('#id-parcialidad-modal').innerHTML;
    const PARAMS_URL = new URLSearchParams(window.location.search);
    const FORM_DATA = new FormData();

    FORM_DATA.append('OrderId', PARAMS_URL.get('venta_id') );
    FORM_DATA.append('idParcialidad', ID_PARCIALIDAD );
    fetch('php/update-autorizar-parcialidad.php', {method: 'POST', body: FORM_DATA})
    .then(data => data.json())
    .then(data => {
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
    })

}

function cancelarVenta() {

    const PARAMS_URL = new URLSearchParams(window.location.search);
    const FORM_DATA = new FormData();

    FORM_DATA.append('OrderId', PARAMS_URL.get('venta_id') );
    fetch('php/cancelar-venta.php', {method: 'POST', body: FORM_DATA}) 
    .then(data => data.json())
    .then(data => {
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
    })

}

function cancelarParcialidad() {

    const ID_PARCIALIDAD = document.querySelector('#id-parcialidad-modal').innerHTML;
    const PARAMS_URL = new URLSearchParams(window.location.search);
    const FORM_DATA = new FormData();

    FORM_DATA.append('OrderId', PARAMS_URL.get('venta_id') );
    FORM_DATA.append('idParcialidad', ID_PARCIALIDAD );
    fetch('php/cancelar-parcialidad.php', {method: 'POST', body: FORM_DATA}) 
    .then(data => data.json())
    .then(data => {
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
    })

}

function sumarCostoEnvio() {

    let input_costo_envio = document.querySelector('[name=costo-envio]');
    let input_total = document.querySelector('[name=venta-total]');
    if (input_costo_envio.value != "") {
        let new_total = parseFloat(formatter.numberFormat(input_total.value)) + parseFloat(formatter.numberFormat(input_costo_envio.value));
        input_total.value = formatter.currencyFormat(new_total);
    }
    
}

function permisosDeUsuario() {
    
    switch (localStorage.getItem('user_type')) {
        case '2':
            document.querySelector('#btn-concluir').hidden = true
            document.querySelector('#btn-autorizar').hidden = true
            document.querySelector('#btn-finalizar').hidden = true
            document.querySelector('#btn-cancelar').hidden = true

            // btns parcialidades 
            document.querySelector('#btn-cancelar-parcialidad').hidden = true
            document.querySelector('#btn-autorizar-parcialidad').hidden = true

            break;
    
        default:
            break;
    }


}

function pagarVenta() {


    const PARAMS_URL = new URLSearchParams(window.location.search);
    const FORM_DATA = new FormData();

    FORM_DATA.append('OrderId', PARAMS_URL.get('venta_id'));
    FORM_DATA.append('value', document.querySelector('[name=pagado-bool]').checked);

    fetch('php/update-pagado.php', {method: 'POST', body: FORM_DATA}) 
    .then(data => data.json())
    .then(data => {
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
    })
    
} 

function facturarVenta() {


    const PARAMS_URL = new URLSearchParams(window.location.search);
    const FORM_DATA = new FormData();

    FORM_DATA.append('OrderId', PARAMS_URL.get('venta_id'));
    FORM_DATA.append('value', document.querySelector('#check-factura-electronica').checked);

    fetch('php/update-factura.php', {method: 'POST', body: FORM_DATA}) 
    .then(data => data.json())
    .then(data => {
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
    })
    
} 

function setOptionsCantEtga() {

    let options = ``;
    for (let index = 0; index < 101; index++) {
        options += `<option value="${index}">${index}</option>`
    }
    return options;

}

function sumarCostoEnvio() {

    let input_costo_envio = document.querySelector('[name=costo-envio]');
    let input_total = document.querySelector('[name=venta-total]');
    if (input_costo_envio.value != "") {
        let new_total = parseFloat(formatter.numberFormat(input_total.value)) + parseFloat(input_costo_envio.value);
        input_total.value = formatter.currencyFormat(new_total);
    }

}

function restarCostoEnvio() {

    let input_costo_envio = document.querySelector('[name=costo-envio]');
    let input_total = document.querySelector('[name=venta-total]');
    if (input_costo_envio.value != "") {
        let new_total = parseFloat(formatter.numberFormat(input_total.value)) - parseFloat(input_costo_envio.value);
        input_total.value = formatter.currencyFormat(new_total);
        input_costo_envio.value = '0.00';
    }

}

function permisosByStatus() {
    
    const STATUS = document.querySelector('[name=estatus]').value
    
    switch (STATUS) {
        case 'Cancelada':
            document.querySelector('#btn-cancelar').hidden = true;
            document.querySelector('#btn-concluir').hidden = true;
            document.querySelector('#btn-finalizar').hidden = true;
            document.querySelector('#btn-new-parcialidad').hidden = true;
            document.querySelector('#btn-autorizar').hidden = true;
            // nuevos botones
            document.querySelector('#btn-sumar-costo').hidden = true;
            document.querySelector('#btn-restar-costo').hidden = true;
            break;
        case 'Cerrado':
            document.querySelector('#btn-cancelar').hidden = true;
            document.querySelector('#btn-concluir').hidden = true;
            document.querySelector('#btn-finalizar').hidden = true;
            document.querySelector('#btn-new-parcialidad').hidden = true;
            document.querySelector('#btn-autorizar').hidden = true;
            // nuevos botones
            document.querySelector('#btn-sumar-costo').hidden = true;
            document.querySelector('#btn-restar-costo').hidden = true;
            break;
    
        default:
            break;
    }
}

function deleteRow(element) {

    const PARAMS = new URLSearchParams(window.location.search)
    let row = element.parentElement.parentElement;
    let id = row.children[0].textContent;
    const FORM_DATA = new FormData()
    FORM_DATA.append('id', id);
    FORM_DATA.append('idVenta', PARAMS.get('venta_id'));
    fetch('php/delete-items-venta.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
    })

}

function getDetallePromo(item_id) {
    
    $('#modal-detalle-promo').modal('show');
    getDetalleParcialidadesPromo()
    document.querySelector('#id-promocion-modal').innerHTML = item_id;
    const PARAMS_URL = new URLSearchParams(window.location.search);
    const FORM_DATA = new FormData();
    FORM_DATA.append('IdPromo', item_id)
    FORM_DATA.append('IdVenta', PARAMS_URL.get('venta_id'))
    fetch('php/select-detalle-promo.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        
        const RANGE_OPTIONS = setOptionsCantEtga();
        let linea = '';
        data.forEach(item => {
            linea += `            
                <tr>
                    <td>${item.Id}</td>
                    <td>${item.Nombre}</td>
                    <td>${item.Sku}</td>
                    <td>${item.Cantidad_Original}</td>
                    <td class="text-center background-td-editable"><select  onchange="getRestante(this)">${RANGE_OPTIONS}</select></td>
                    <td>${item.Restante}</td>
                </tr>
            `
        })
        document.querySelector('#tbl-detalle-promo tbody').innerHTML = linea;        
        
    })

}

function getDetalleParcialidadesPromo() {
    
    const PARAMS_URL = new URLSearchParams(window.location.search);
    $('#modal-detalle-promo').modal('show');
    const FORM_DATA = new FormData();
    FORM_DATA.append('OrderId', PARAMS_URL.get('venta_id'))
    fetch('php/select-parciales-promo.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        let linea = '';
        data.forEach(item => {
            linea += `    
                <tr style="cursor: pointer" onclick="getDetalleParcialidadPromoById('${item.Id}')" title="Ver detalle de la prcialidad" data-toggle="tooltip" data-placement="bottom">
                    <td>${item.Id}</td>
                    <td>${item.OrderId}</td>
                    <td>${item.Datecreated}</td>
                    <td>${item.Vendedor}</td>
                    <td>${item.Estado}</td>
                    <td>${item.Observacion}</td>
                </tr>
            `
        })
        document.querySelector('#tbl-detalle-promo-parcialidades tbody').innerHTML = linea;
        $('[data-toggle="tooltip"]').tooltip();
    })

}

function getDetalleParcialidadPromoById(parcialidaId) {

    document.querySelector('#id-parcialidad-promo').innerHTML = parcialidaId;
    const FORM_DATA = new FormData();
    FORM_DATA.append('ParcialidaId', parcialidaId)
    fetch('php/select-items-parcial-promo.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {

        
        let linea = '';
        data.forEach(item => {
            linea += `
                <tr>
                    <td>${item.Id}</td>
                    <td>${item.Sku}</td>
                    <td>${item.Nombre}</td>
                    <td>${item.Cantidad_Original}</td>
                    <td>${item.Entregado}</td>
                </tr>
            `
        })
        document.querySelector('#tbl-detalle-parcialida-promo tbody').innerHTML = linea;
        
    })

    document.querySelector('#btn-auto-par-promo').hidden = false

}


function createParcialidadPromo()  {
    
    const PARAMS_URL = new URLSearchParams(window.location.search);
    let body = []
    document.querySelectorAll('#tbl-detalle-promo tbody tr').forEach(element => {
        body.push({
            "IdItem": element.children[0].textContent,            
            "Original": element.children[3].textContent,
            "Entregado": element.children[4].children[0].value,
            "Restante" : element.children[5].textContent
        })
    })

    let obj = {
        "header":[
            {
                "IdVenta": PARAMS_URL.get('venta_id'),
                "IdPromocion": document.querySelector('#id-promocion-modal').innerHTML,
                "Comment" : document.querySelector('[name=observaciones-parcialidad-promo]').value
            }            
        ],
        "body": body
    }

    fetch('php/insert-items-promo.php', { method: 'POST', body: JSON.stringify(obj) })
    .then(data => data.json())
    .then(data => {
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
        $('#modal-detalle-promo').modal('hide');

    })
    
}

function getRestante(select) {

    const RESTANTE = select.parentElement.parentElement.children[5].textContent
    const ENTREGADO = select.value
    const RESTANTE_NUEVO = parseInt(RESTANTE) - parseInt(ENTREGADO);
    select.parentElement.parentElement.children[5].textContent = RESTANTE_NUEVO

}


function autorizeParcialidadPromo() {
    
    const ID_PARCIALIDAD = document.querySelector('#id-parcialidad-promo').innerHTML;
    const FORM_DATA = new FormData();
    FORM_DATA.append('idParcialidad', ID_PARCIALIDAD)
    fetch('php/update-autorizar-parcialidad-promo.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
        $('#modal-detalle-promo').modal('hide');

    })

}