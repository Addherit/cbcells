<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

try {
    $data = json_decode(file_get_contents('php://input'), true);
    $idventa = $data["header"][0]['IdVenta'];
    $idpromo = $data["header"][0]['IdPromocion'];
    $coment = $data["header"][0]['Comment'];

    $body = $data["body"];

//    ItemsParcialPromo Tabla de los items
        
    $sql="INSERT INTO `ParcialidadPromo`(`OrderId`, `Observacion`) VALUES('$idventa','$coment')"; 
//    echo $sql;
    if ($conn->query($sql) === TRUE) {

        $select = "SELECT MAX(Id) as Id  FROM ParcialidadPromo";
        $datos = $con->query($select)->fetchAll(PDO::FETCH_ASSOC );
//        echo $datos;
        $parcial = $datos[0]['Id'];
 //       echo $parcial;


        //construccion de ingreso 
        $query="INSERT INTO `ItemsParcialPromo`(`ItemId`, `Cantidad`, `Cantidad_Original`,
            `Entregado`, `OrderId`, `PromoId`, `Parcialidad`) VALUES "; 


        $datos_invent=[];
        foreach($body as $row){

            $entregado = $row['Entregado'];
            $iditem= $row['IdItem'];
            $original = $row['Original'];
            $restante = $row['Restante'];

            //insert construccion
            $values="('$iditem','$entregado','$original','$entregado','$idventa','$idpromo','$parcial'),";
            $query .= $values;
        }
        $query = substr($query, 0, -1);

        if ($conn->query($query) === TRUE) {
            $result = ['type' => "success", 'msn' => "Parcialidad  de Promoción creada correctamente",'sql'=>$query];
        }else{
            $result = ['type' => "danger", 'msn' => "Problema del query",'query'=>$query];
        }
    }else{
        $result = ['type' => "danger", 'msn' => "Problema del query"];
    }
} catch (PDOException  $e) {
    $result = ['type' => "danger", 'msn' => "Problema de la conexión"];
}
echo json_encode($result);

