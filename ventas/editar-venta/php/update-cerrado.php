<?php 
session_start();
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

if($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        //vars from form
        $id= $_POST['OrderId'];
        // sql query for INSERT INTO paciente
        $sql="UPDATE `Ordenes` SET `Estado` ='Cerrado' WHERE `OrderId`=$id";
        // Performs the $sql query on the server to insert the values
        if ($conn->query($sql) === TRUE) {
            //get vendedor
            $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $datos = $con->query("SELECT VendedorId FROM Ordenes where OrderId =$id")->fetchAll(PDO::FETCH_ASSOC );
            foreach($datos as $row){
                $final_data[] = $row;
            }
            $vendedorid = $final_data[0]['VendedorId'];
            //notificacion
            $queryn="INSERT INTO `Notificaciones`( `UserId`, `Mensaje`, `IdAccion`, `Ntype`,`Type`, `EstadoAdmin`, `EstadoSupervisor`, `Mostrar`) VALUES ('$vendedorid','Cambio de estatus en orden','$id','4','Cambio de estatus en orden','visto','visto','A/C')";
            $conn->query($queryn);
            //aqui se debde se sumar los stocks quitados por los items de este pedido. 
            $result = ['type' => "success", 'msn' => "Venta Autorizada correctamente",'id'=>$id];
        }
        else {
            $result = ['type' => "danger", 'msn' => "Problema del query",'query'=> $sql];
        }

        $conn->close();
    } catch (PDOException  $e) {
        $result = ['type' => "danger", 'msn' => "Problema de la conexión"];
    }
    echo json_encode($result);
}
?>