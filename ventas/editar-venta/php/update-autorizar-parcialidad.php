<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

if($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        //vars from form
        $id= $_POST['idParcialidad'];
        // sql query for INSERT INTO paciente
        $sql="UPDATE `Parcialidades` SET `Estado` ='Autorizado' WHERE `Id`=$id";
//        $sql="UPDATE `ItemsOrd` SET `Estado` ='Autorizado' WHERE `Id`=$id";
        // Performs the $sql query on the server to insert the values
        if ($conn->query($sql) === TRUE) {
            $result = ['type' => "success", 'msn' => "Parcialidad Autorizada correctamente",'id'=>$id];
        }
        else {
            $result = ['type' => "danger", 'msn' => "Problema del query",'query'=> $sql];
        }

        $conn->close();
    } catch (PDOException  $e) {
        $result = ['type' => "danger", 'msn' => "Problema de la conexión"];
    }
    echo json_encode($result);
}
?>