<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

try {
  $data = $_POST['id_venta'];
  $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $con->exec("set names utf8");
  $final_data=[];

  $query ="SELECT o.`OrderId`,d.`Cedula`,CASE WHEN o.`Pagado` = 0 THEN 'false' ELSE 'true'END as Pagado,CASE WHEN o.`Factura` = 0 THEN 'false' ELSE 'true'END as Factura,o.`Estado`,o.`Parcial`,CONCAT(u.Nombre, ' ', u.Apellido) AS Vendedor,o.VendedorId,o.`Notas`,o.`DateCreated`,CONCAT(d.Nombre, ' ', d.Apellido) AS Medico,o.`Costo_env`,o.`Guia`,o.`Cuenta`,o.`VentaTotal`,o.`Descuento`,o.`Paciente`,o.`Patologia`,o.`Tratamiento`,df.Fiscal_name,df.Rfc,df.Regimen_fiscal,df.Direccion_completa,df.Email_factura,df.SegundoMail,d.Correo,d.Domicilio,d.Telefono FROM Ordenes o inner join Users u on o.VendedorId = u.userId inner join Doctores d on o.MedId = d.DocId left join DatosFiscales df on o.MedId = df.DocId where o.OrderId=$data and o.Estado NOT IN ('Eliminado') ";
  $datos = $con->query($query)->fetchAll(PDO::FETCH_ASSOC );

  foreach($datos as $row){
    $final_data[] = $row;
  }
  echo json_encode($final_data);
} catch(PDOException $e) {
    echo $result = ["mensaje" => "Error: ".$e->getMessage()];
}
?>