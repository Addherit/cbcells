<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

if($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        //vars from form
        $id= $_POST['id'];
        $idventa= $_POST['idVenta'];

        // sql query for INSERT INTO Medicos
        $sql = "update ItemsOrd set Estado ='Eliminado' where OrderId =$idventa and Id =$id";
        
        // Performs the $sql query on the server to insert the values
        if (mysqli_query($conn, $sql)) {
            $result = ['type' => "success", 'msn' => "Item eliminada correctamente"];
        } else {
            $result = ['type' => "danger", 'msn' => "problema del query",'sql'=>$sql];
        }
        mysqli_close($conn);
    } catch (PDOException  $e) {
        $result = ['type' => "danger", 'msn' => "problema de la conexion"];
    }

    echo json_encode($result);
}
?>