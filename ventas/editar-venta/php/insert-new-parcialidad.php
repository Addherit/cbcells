<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

if($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        //vars from form
        $data = json_decode(file_get_contents('php://input'), true);
        $orderid = $data["parcialidad"][0]['id'];
        $observacion = $data["parcialidad"][0]['observaciones'];

        //ingreso parcialidad
        $sql = "INSERT INTO `Parcialidades`( `OrderId`, `Observacion`) VALUES ($orderid,'".$observacion."')";
        $conn->query($sql);

        //regreso del id 
        $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $con->exec("set names utf8");
        $final_data=[];

        $query = "SELECT MAX(Id) as Id FROM `Parcialidades`where OrderId =$orderid";
        $datos = $con->query($query)->fetchAll(PDO::FETCH_ASSOC );
        foreach($datos as $row){
            $final_data[] = $row;
        }
        $parcid = $final_data[0]['Id'];
        //construccion del insert items
        $query = "INSERT INTO `ItemsOrd`( `ItemId`, `Descuento`, `Concepto`,`Total`, `Cantidad`, `Cantidad_Original`, `Entregado`, `OrderId`, `Parcialidad`) VALUES ";
        foreach($data['parcialidad'][1] as $row){
            $itemid = $row['itemId'];
            $idrowanterior = $row['id'];
            $cant = $row['cant'];
            $concepto = $row['concepto'];
            $dcto = $row['dcto'];
            $etga = $row['etga'];
            $precio = $row['precio'];
            $rest = $row['rest'];
            $total = $row['total'];

            $aux = (floatval($rest) - floatval($etga));
            if($aux == 0){
                $entregado = $cant;
            }else{
                $entregado = (floatval($cant) - floatval($aux));
            }

            $values ="($itemid,'$dcto','$concepto','$total','$etga','$cant','$entregado',$orderid,'$parcid'),";
            $query .=$values;

            $sql2="UPDATE `ItemsOrd` SET `Estado`='OParcialidad' WHERE Id=$idrowanterior";
            $conn->query($sql2);
        }
        $query = substr($query, 0, -1);
        $conn->query($query);
        $result = ['type' => "success", 'msn' => "Nueva Parcialidad creada correctamente",
        'query'=>$query,'sql'=>$sql,'sql2'=>$sql2];

    } catch (PDOException  $e) {
        $result = ['type' => "danger", 'msn' => "Problema de la conexión"];
    }
    echo json_encode($result);
}
?>