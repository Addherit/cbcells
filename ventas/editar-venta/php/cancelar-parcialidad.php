<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

if($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        //vars from form
        $id= $_POST['OrderId'];
        $idParcial= $_POST['idParcialidad'];
        // sql query for INSERT INTO paciente
        $sql="UPDATE `Parcialidades` SET `Estado` ='Cancelado' WHERE `Id`=$idParcial";
        // Performs the $sql query on the server to insert the values
        if ($conn->query($sql) === TRUE) {
            //aqui se debde se sumar los stocks quitados por los items de este pedido. 
            $result = ['type' => "success", 'msn' => "PArcialidad Cancelada correctamente",'id'=>$id];
        }
        else {
            $result = ['type' => "danger", 'msn' => "Problema del query",'query'=> $sql];
        }

        $conn->close();
    } catch (PDOException  $e) {
        $result = ['type' => "danger", 'msn' => "Problema de la conexión"];
    }
    echo json_encode($result);
}
?>