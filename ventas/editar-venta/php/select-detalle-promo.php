<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

try {
  $id = $_POST['IdPromo'];
  $idventa = $_POST['IdVenta'];

  $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $con->exec("set names utf8");
  $final_data=[];

  $sql ="SELECT count(*) as 'Cuenta' FROM ParcialidadPromo where OrderId = $idventa and Estado in ('Autorizado','Activo')";

  $datos = $con->query($sql)->fetchAll(PDO::FETCH_ASSOC );

  foreach($datos as $row){
    $final_data[] = $row;
  }
  $cuenta = $datos[0]['Cuenta'];
 // echo $cuenta;

  if($cuenta == 0){
    $query =" SELECT ip.*,ip.Cantidad as 'Cantidad_Original',i.Sku,ip.Cantidad as 'Restante' FROM `ItemsPromo` ip inner join `Inventario` i on i.Id = ip.`IdItem` where `IdPromo` = $id";
    $datos = $con->query($query)->fetchAll(PDO::FETCH_ASSOC );

    foreach($datos as $row){
      $final_datas[] = $row;
    }
    echo json_encode($final_datas);
  }else{
    $sql ="SELECT Max(Id) as 'id' FROM ParcialidadPromo where OrderId = $idventa and Estado in ('Autorizado','Activo')";

    $datos = $con->query($sql)->fetchAll(PDO::FETCH_ASSOC );

    foreach($datos as $row){
      $final_data[] = $row;
    }
    $id2 = $datos[0]['id'];
//    print_r($datos);
 //   echo $id2;

    //query
//    $query =" SELECT p.`Id`,p.`Estado`,p.`Cantidad_Original`,p.`Entregado` as 'Restante',i.Nombre,iv.Sku FROM `ItemsParcialPromo` p inner join ItemsPromo i on i.Id = p.`ItemId` inner join Inventario iv on iv.Id = i.IdItem where p.Parcialidad = $id2";
    $query = "  SELECT p.`ItemId` as Id,p.`Estado`,p.`Cantidad_Original`,(p.`Cantidad_Original` - (select Sum(e.Entregado) from ItemsParcialPromo e where e.ItemId = p.ItemId and e.OrderId=$idventa )) as 'Restante',i.Nombre,iv.Sku FROM `ItemsParcialPromo` p inner join ItemsPromo i on i.Id = p.`ItemId` inner join Inventario iv on iv.Id = i.IdItem where p.Parcialidad = $id2 ";
    $datos = $con->query($query)->fetchAll(PDO::FETCH_ASSOC );

    foreach($datos as $row){
      $final_datas[] = $row;
    }
    echo json_encode($final_datas);
  }
  
} catch(PDOException $e) {
    echo $result = ["mensaje" => "Error: ".$e->getMessage()];
}
?>