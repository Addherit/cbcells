<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

try {
  $data = $_POST['ParcialidaId'];
  $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $con->exec("set names utf8");
  $final_data=[];

  //$query = "SELECT io.*,i.SKu,i.Precio FROM ItemsParcialPromo io inner join ItemsPromo i on io.ItemId = i.Id where Parcialidad=$data";
  $query = "SELECT io.*,i.Sku,ip.Nombre FROM ItemsParcialPromo io inner join ItemsPromo ip on io.ItemId = ip.Id inner join Inventario i on ip.IdItem = i.Id where io.Parcialidad =$data ";
  //$query = "SELECT io.* FROM ItemsParcialPromo io inner join ItemsPromo i on io.ItemId = i.Id where io.PromoId =$data ";
  $datos = $con->query($query)->fetchAll(PDO::FETCH_ASSOC );
  foreach($datos as $row){
    $final_data[] = $row;
  }
//  echo $query;
  echo json_encode($final_data);
} catch(PDOException $e) {
    echo $result = ["mensaje" => "Error: ".$e->getMessage()];
}
?>