<?php

// Import PHPMailer classes into the global namespace
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require '../../../assets/php/phpMailer/vendor/autoload.php';

// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);

try {
    //Server settings
    $mail->SMTPDebug = 0;                               // Enable verbose debug output SMTP::DEBUG_SERVER
    $mail->isSMTP();                                    // Send using SMTP
    $mail->Host       = 'smtp.gmail.com';           // Set the SMTP server to send through
    $mail->SMTPAuth   = true;                           // Enable SMTP authentication
    $mail->Username   = 'info.cbcells@gmail.com';           // SMTP username
    $mail->Password   = 'cbcellss';            // SMTP password
    $mail->SMTPSecure = 'TLS';                          // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
    $mail->Port       = 587;                             // TCP port to connect to
 
    //Recipients  
    $title = 'Se registró un cambio de estatus en orden *No responder correo*';
    $remitente = 'info.cbcells@gmail.com';    
    $receptor = 'info.cbcells@gmail.com';
    $message = file_get_contents('../../../assets/php/formatos_mail/format-new-medic.php'); 
    
    $mail->setFrom($remitente, $title);
    $mail->addAddress($receptor);

    // Content
    $mail->isHTML(true);   
    $mail->MsgHTML($message);
    $mail->Subject = 'Cambio de esttus';
   

    // Activo condificacción utf-8
    $mail->CharSet = 'UTF-8';

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}

?>