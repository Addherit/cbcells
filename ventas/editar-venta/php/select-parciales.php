<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

try {
  $data = $_POST['OrderId'];
  $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $con->exec("set names utf8");
  $final_data=[];

  $query = "SELECT p.`Id`,p.`OrderId`,p.`Observacion`,p.`Datecreated`,CONCAT(u.Nombre, ' ', u.Apellido) as Vendedor, p.`Estado` FROM `Parcialidades` p inner join Ordenes o on p.OrderId = o.OrderId inner join Users u on u.userId = o.VendedorId where p.OrderId = $data";
  $datos = $con->query($query)->fetchAll(PDO::FETCH_ASSOC );

  foreach($datos as $row){
    $final_data[] = $row;
  }
  echo json_encode($final_data);
} catch(PDOException $e) {
    echo $result = ["mensaje" => "Error: ".$e->getMessage()];
}
?>