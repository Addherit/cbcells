<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

try {
  $data = $_POST['ParcialId'];
  $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $con->exec("set names utf8");
  $final_data=[];

  $query = "SELECT io.*,i.SKu,i.Precio FROM ItemsOrd io inner join Inventario i on io.ItemId = i.Id where Parcialidad=$data";
  $datos = $con->query($query)->fetchAll(PDO::FETCH_ASSOC );

  foreach($datos as $row){
    $final_data[] = $row;
  }
  echo json_encode($final_data);
} catch(PDOException $e) {
    echo $result = ["mensaje" => "Error: ".$e->getMessage()];
}
?>