<!DOCTYPE html>
<html lang="en">

<head>
  <?php include '../../header.html' ?>
</head>

<body class="">
  <div class="wrapper ">
    <?php include '../../sidebar/sidebar.html' ?>
    <div class="main-panel">
      <?php include '../../sidebar/navbar.html' ?>
      <div class="content">
        <div class="container-fluid">
          <div class="row">
          <div class="col-md-10 offset-md-1">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Nueva venta</h4>
                  <p class="card-category">Ingresa la información de la venta</p>
                </div>
                <div class="card-body">
                  <form id="form-add-venta">
                    <div class="row">
                      <div class="col-md-4 offset-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Fecha</label>
                          <input type="text" class="form-control" name="fecha" readonly style="background-color: white">
                        </div>
                      </div>                 
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Estatus</label>
                          <input type="text" class="form-control" name="estatus" id="estatus" disabled>
                        </div>
                      </div>
                    </div>    

                    <div class="row">
                      <div class="col-md-5">
                        <div class="form-group select-custom">
                          <label class="bmd-label-floating">Nombre del médico</label>                          
                          <select class="custom-select" name="nombre-medico"></select>
                        </div>
                      </div>                      
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Correo electrónico</label>
                          <input type="email" class="form-control" name="email">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Cédula profesional</label>
                          <input type="text" class="form-control" name="cedula-profesional">
                        </div>
                      </div>
                    </div>


                    <div class="row">
                      <div class="col-md-8">
                        <div class="form-group">
                          <label class="bmd-label-floating">Domicilio de envío/ Calle y Número/ Ciudad/ Estado </label>
                          <input type="text" class="form-control" name="domicilio">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Teléfono</label>
                          <input type="text" class="form-control" name="telefono">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Se aplica descuento</label>
                          <input type="text" class="form-control" name="descuento">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Guía</label>
                          <input type="text" class="form-control" name="guia">
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Vendedor asignado</label>
                          <input type="text" class="form-control" name="vendedor">
                          <input type="hidden" class="form-control" name="id_vendedor">
                        </div>
                      </div>
                    </div>  
                    <div class="row">                      
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Paciente</label>
                          <input type="text" class="form-control" name="paciente">
                        </div>
                      </div>                      
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Patología</label>
                          <input type="text" class="form-control" name="patologia">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group  select-custom">
                          <label class="bmd-label-floating">Cuenta bancaria</label>
                          <!-- <input type="text" class="form-control" name="cuenta-bancaria"> -->
                          <select class="custom-select" name="cuenta-bancaria">
                            <option value="" disabled selected>Selecciona una cuenta</option>
                            <option value="Centa cbcells - banorte 3829">Centa cbcells - banorte 3829</option>
                            <option value="Cuenta carolina - Bancomer 4589">Cuenta carolina - Bancomer 4589</option>
                            <option value="Cuenta fabian - Banorte 3230">Cuenta fabian - Banorte 3230</option>
                            <option value="Otro">Otro</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-8">
                        <div class="form-group">
                          <label>Tratamiento</label>
                          <div class="form-group">
                            <label class="bmd-label-floating"> Ingresa observaciones sobre la venta</label>
                            <textarea class="form-control" rows="3" name="tratamiento"></textarea>
                          </div>
                        </div>
                      </div>
                    </div>
                    <br>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="card">
                          <div class="card-header card-header-primary">
                              <h4 class="card-title ">Lista de Artículos</h4>
                              <p class="card-category">Selecciona el SKU y escribe la cantidad</p>
                          </div>
                          <div class="card-body">
                            <div class="table-responsive" style="height: auto">
                              <table class="table" id="tbl-lista-sku">
                                <thead class=" text-primary">                                
                                  <th>Sku</th>
                                  <th>Concepto</th>
                                  <th class="text-right">Precio</th>
                                  <th style="width: 1px">Cant</th>
                                  <th style="width: 1px">Dcto</th>
                                  <th style="width: 1px">Etga</th>
                                  <th style="width: 1px">Rest</th>
                                  <th class="text-right">Total</th>                                
                                </thead>
                                <tbody>                                      
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>           
                    </div>          


                    <div class="row">                        
                      <div class="col-md-4 offset-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Costo de envío</label>
                          <div class="input-group no-border"> 
                            <span class="material-icons" style="margin-top: 10px; font-size: 18px !important">attach_money</span>
                            <input type="text" class="form-control" name="costo-envio" value="0.00"><br>
                          </div>
                        </div>
                        <button type="button" class="btn btn-success float-left" id="btn-sumar-costo"><span class="material-icons">add</span> Sumar</button>
                        <button type="button" class="btn btn-danger float-right" id="btn-restar-costo"><span class="material-icons">remove</span> Restar</button>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Venta total</label>
                          <div class="input-group no-border">                      
                            <span class="material-icons" style="margin-top: 10px; font-size: 18px !important">attach_money</span>
                            <input type="text" class="form-control" name="venta-total" value="0.00">
                          </div>
                        </div>
                      </div>
                    </div> 

                    <div class="row">
                      <div class="col-12">
                        <div class="form-group">
                          <label>Observaciones</label>
                          <div class="form-group">
                            <label class="bmd-label-floating"> Ingresa observaciones sobre la venta</label>
                            <textarea class="form-control" rows="3" name="observaciones"></textarea>
                          </div>
                        </div>
                      </div>                      
                    </div>


                    <br>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="custom-switch">
                          <input type="checkbox" class="custom-control-input" id="check-factura-electronica">
                          <label class="custom-control-label" for="check-factura-electronica">Factura electrónica</label>
                        </div>
                      </div>
                    </div>
                    <div id="div-datos-fiscales" class="collapse">
                      <div class="row">
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="bmd-label-floating">Nombre fiscal</label>
                            <input type="text" class="form-control" name="nombre-fiscal">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="bmd-label-floating">RFC</label>
                            <input type="text" class="form-control" name="rfc">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="bmd-label-floating">Régimen fiscal</label>
                            <input type="text" class="form-control" name="regimen-fiscal">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="bmd-label-floating">Correo electrónico 1</label>
                            <input type="email" class="form-control" name="email-factura">
                          </div>
                        </div>
                      </div>  
                      <div class="row">
                        <div class="col-md-8">
                          <div class="form-group">
                            <label class="bmd-label-floating">Dirección completa</label>
                            <input type="text" class="form-control" name="direccion-fiscal">
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label class="bmd-label-floating">Correo electrónico 2</label>
                            <input type="email" class="form-control" name="email-factura2">
                          </div>
                        </div>
                      </div> 
                    </div>  
                    <div class="row">
                      <div class="col-12">
                                                                                        
                        <button type="submit" class="btn btn-primary pull-right" id="btn-save"><span class="material-icons">save_alt</span> Guardar datos</button>
                        <button type="button" class="btn btn-primary float-left" id="btn-lista-ventas"><span class="material-icons">arrow_back_ios</span>Lista de ventas</button>   
                      </div>
                    </div> 
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
</body>
<?php include '../../footer.html' ?>
<script src="js/ventas.js"></script>

</html>