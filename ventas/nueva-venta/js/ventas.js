
document.addEventListener('DOMContentLoaded', () => {    

    setTimeout(() => {
        document.querySelector('[name=fecha]').parentElement.classList.add('is-filled');
    }, 400);

    setInterval(() => {  
        document.querySelector('[name=fecha]').value = moment().format('DD-MM-YYYY h:mm:ss a'); 
    }, 1000);

    document.querySelector('[name=vendedor]').value = localStorage.getItem('all_name');
    document.querySelector('[name=id_vendedor]').value = localStorage.getItem('user_id');
    validateCollapseDatosFiscales();
    setMedicosSelectList();
    setTableRows(3);

})
document.querySelector('#form-add-venta').addEventListener('submit', (event) => { event.preventDefault();insertVenta() })
document.querySelector('#check-factura-electronica').addEventListener('change', () => { validateCollapseDatosFiscales() })
document.querySelector('[name=nombre-medico]').addEventListener('change', () => { 
    completeInfoMedico(document.querySelector('[name="nombre-medico"]').value)
})
document.querySelector('#btn-lista-ventas').addEventListener('click', () => { goToListVentas() });
document.querySelector('#btn-sumar-costo').addEventListener('click', () => { sumarCostoEnvio() });
document.querySelector('#btn-restar-costo').addEventListener('click', () => { restarCostoEnvio() });

async function insertVenta() {

    
    document.querySelector('[name=costo-envio]').value = formatter.numberFormat(document.querySelector('[name=costo-envio]').value)
    document.querySelector('[name=venta-total]').value = formatter.numberFormat(document.querySelector('[name=venta-total]').value)
    const ALL_ROWS = document.querySelectorAll('#tbl-lista-sku tbody tr');    
    const ARRAY_ROWS = creatArrayOfRows(ALL_ROWS);
    if (ARRAY_ROWS.length > 0 && document.querySelector('[name=nombre-medico]').value != '') {
        const RESULT = await insertCabecera();
        if( RESULT.type == 'success') {        
            insertDetalle(RESULT.id_venta, ARRAY_ROWS, ALL_ROWS);
            enviarCorreoAlerta(RESULT.id_venta);
        } else {
            const MSN = new Message('danger', 'Error: Al generar venta');
            MSN.showMessage();
        }
    } else {        
        const MSN = new Message('warning', 'Alerta: No se puede generar una orden sin items y/o médico');
        MSN.showMessage();
    }
    
} 

function enviarCorreoAlerta(id) {

    const fd = new FormData()
    fd.append('id', id)
    fetch('php/enviar-correo.php', { method: 'POST', body: fd });
    
}


function insertCabecera() {

    const FORM_DATA = new FormData(document.querySelector('#form-add-venta'))
    FORM_DATA.append('check-factura-electronica', document.querySelector('#check-factura-electronica').checked)
    return fetch('php/insert-order.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        document.querySelector('[name=costo-envio]').value = formatter.currencyFormat(document.querySelector('[name=costo-envio]').value)
        document.querySelector('[name=venta-total]').value = formatter.currencyFormat(document.querySelector('[name=venta-total]').value) 
        return data;
        
    })

}

function insertDetalle(id_venta, array_rows, all_rows) {
    
    const PARCIAL = knowIfPartial(all_rows);    
    const DATA ={
        "detalle":[        
            {id: id_venta, parcial: PARCIAL},        
            array_rows
        ]
    }
    
    fetch('php/insert-items-order.php', { method: 'POST', body: JSON.stringify(DATA) })
    .then(data => data.json())
    .then(data => {
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
    })
}

function knowIfPartial(rows) {

    let restante = 0;
    let parcial = 'no';
    rows.forEach(row => {
        restante = restante + parseInt(row.children[6].textContent);
    });
    if (restante > 0) parcial = 'si';
    return parcial;

}

function creatArrayOfRows(rows) {
   
    let all_arrays = [];
    rows.forEach(row => {
        if (row.children[0].children[0].value) {
            let sku = row.children[0].children[0].value.split('-')
            let array = {
                sku: sku[0],
                concepto: row.children[1].textContent,
                precio: formatter.numberFormat(row.children[2].children[1].textContent),
                cant: row.children[3].children[0].value,
                dcto: row.children[4].textContent,
                etga: row.children[5].children[0].value,
                rest: row.children[6].textContent,
                total: formatter.numberFormat(row.children[7].children[1].textContent),
                promo: row.children[9].textContent,
            }
            all_arrays.push(array);
        }               
    });
    return all_arrays;

}

function validateCollapseDatosFiscales() {

    const DIV_DATOS_FISCALES = document.querySelector('#div-datos-fiscales');
    const CHECK_DATOS_FISCALES = document.querySelector('#check-factura-electronica');
    CHECK_DATOS_FISCALES.checked  == true ? DIV_DATOS_FISCALES.classList.add('show') : DIV_DATOS_FISCALES.classList.remove('show');

}

function getMedicos() {

    const FORM_DATA = new FormData()
    FORM_DATA.append('userId', localStorage.getItem('user_id'))
    return fetch('php/select-doctors.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        return data;
    });

}

async function setMedicosSelectList() {

    const MEDICOS = await getMedicos();
    const OPTIONS = getOptiosMedicos(MEDICOS);
    document.querySelector('[name=nombre-medico]').innerHTML = OPTIONS;

}

function getOptiosMedicos(medicos) {

    let options = `<option selected value="">Selecciona un médico</option>`;
    medicos.forEach(medico => {
        options += `<option value="${medico.DocId}">${medico.Nombre} ${medico.Apellido}</option>`;        
    });
    return options;

}

async function setTableRows(num_rows) {

    const ROWS = await getTableRows(num_rows);    
    if (num_rows == 3) {
        document.querySelector('#tbl-lista-sku tbody').innerHTML = ROWS;
    } else {
        $('#tbl-lista-sku tbody').append(ROWS);
    }

}
 
async function getTableRows(num_filas) {
    
    const SKU_OPTIONS = await getSkuOptions(); 
    const RANGE_OPTIONS = setOptionsCantEtga();
    let rows = '';
    for (let index = 0; index < num_filas; index++) {
        rows += 
        `<tr>
            <td>
                <select class="custom-select" style="border: none" onchange="selectSku(this)">${SKU_OPTIONS}</select>
            </td>
            <td></td>   
            <td class="text-right text-primary">
                <span class="material-icons" style="vertical-align: text-top; font-size: 18px !important">attach_money</span> 
                <span>0.00</span>
            </td>
            <td class="text-center background-td-editable" onchange="getRest(this)"><select>${RANGE_OPTIONS}</select></td>
            <td contentEditable class="text-center background-td-editable" onkeyup="getDescuento(this)">0</td>
            <td class="text-center background-td-editable" onchange="getRest(this)"><select>${RANGE_OPTIONS}</select></td>
            <td class="text-center">0</td>
            <td class="text-right text-success">
                <span class="material-icons" style="vertical-align: text-top; font-size: 18px !important">attach_money</span> 
                <span>0.00</span>
            </td>
            <td><a href="#" class="text-danger btn-tbl-accion" onclick="deleteRow(this)"><span class="material-icons">delete</span>Eliminar</a></td>
            <td hidden></td>
        </tr>`
    }
    return rows;    

}

function getRest(td) {

        const TR = td.parentElement;
        let cant = parseInt(td.parentElement.children[3].children[0].value);
        let etga = parseInt(td.parentElement.children[5].children[0].value);        
        const REST = cant - etga;        
        TR.children[6].textContent = REST;
        getDescuento(td);

}

function getTotal(tr) {

    let precio = formatter.numberFormat(tr.children[2].children[1].textContent);
    let cantidad = tr.children[3].children[0].value;
    let descuento = tr.children[4].textContent;
    let total = (parseFloat(precio) * parseInt(cantidad)) - descuento;            
    tr.children[7].innerHTML = `
        <span class="material-icons" style="vertical-align: text-top; font-size: 18px !important">attach_money</span> 
        <span>${formatter.currencyFormat(total)}</span>
    `;      

}

function getDescuento(td) {

    const TR = td.parentElement;
    if (TR.children[2].children[1] == undefined) {
        const MSN = new Message('warning', 'Debes agregar un artículo antes de poner la cantidad');
        MSN.showMessage();
    } else {
        let precio = formatter.numberFormat(TR.children[2].children[1].textContent);
        let cantidad = TR.children[3].children[0].value;
        let subtotal = (parseFloat(precio) * parseInt(cantidad));
        let descuento = (parseInt(TR.children[4].textContent) / 100) * subtotal;
        let total = subtotal - descuento;
        if (td.textContent != '') {
            td.parentElement.children[7].innerHTML = `
                <span class="material-icons" style="vertical-align: text-top; font-size: 18px !important">attach_money</span>
                <span>${formatter.currencyFormat(total)}</span>
            `;     
            getTotalVenta();   
        } else {
            const MSN = new Message('warning', 'No se permiten letras o espacio en blanco');
            MSN.showMessage();
        } 
    }
       

}

function validateValueEmpty(td) {

    if (!td.textContent) { td.textContent = 0 }

}

function getSkus(id, sku) {

    const FORM_DATA = new FormData();
    FORM_DATA.append('id', id);
    FORM_DATA.append('sku', sku);
    return fetch('php/select-skus.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        return data;
    })

}

async function getSkuOptions() {
    
    const SKUS = await getSkus('', '');
    let options = `<option value="" selected disabled>Selecciona uno</option>`;
    SKUS.forEach(sku => {
        options += `<option value="${sku.Id}-${sku.Sku}">${sku.Nombre}</option>`
    });    
    return options;
    
}

async function completeInfoMedico(id_medico) {
    console.log(id_medico);
    const MEDICOS = await getMedicos();
    MEDICOS.forEach(medico => {
        if (medico.DocId == id_medico ) { console.log(medico); setInfoInInputs(medico) }
    });

}

function setInfoInInputs(medico) {

    const INPUTS_TOBE_FILLED = ['email','domicilio','telefono','nombre-fiscal','rfc', 'regimen-fiscal','direccion-fiscal'];
    INPUTS_TOBE_FILLED.forEach(name => {
        document.querySelector(`[name=${name}]`).parentElement.classList.add('is-filled');        
    });

    document.querySelector('[name=email]').value = medico.Correo;
    document.querySelector('[name=domicilio').value = medico.Domicilio;
    document.querySelector('[name=telefono').value = medico.Telefono;

    // Datos fiscales
    document.querySelector('[name=nombre-fiscal]').value = medico.Fiscal_name;
    document.querySelector('[name=rfc]').value = medico.Rfc;
    document.querySelector('[name=regimen-fiscal').value = medico.Regimen_fiscal;
    document.querySelector('[name=direccion-fiscal').value = medico.Direccion_completa;

}



async function selectSku(select_sku) {

    const ROW = select_sku.parentElement.parentElement;
    const ARRAY_ID_SKU = select_sku.value.split('-');
    const SKU = await getSkus(ARRAY_ID_SKU[0], ARRAY_ID_SKU[1]);

console.log(SKU);
    ROW.children[1].textContent = SKU[0].Nombre;
    ROW.children[2].innerHTML = `
        <span class="material-icons" style="vertical-align: text-top; font-size: 18px !important">attach_money</span>
        <span>${formatter.currencyFormat(SKU[0].Precio)}</span>
    `;
    console.log(SKU[0].promo);
    // esta td está hidden, pero tendrá el valor de si es promo o no 
    ROW.children[9].textContent = SKU[0].promo;

    getTotal(ROW); 
    validarUltimaRow(ROW)   

}

function getTotalVenta() {

    const ROWS = document.querySelectorAll('#tbl-lista-sku tbody tr');
    let total_venta = 0;    
    ROWS.forEach(row => {
        
        const TOTAL = parseFloat(formatter.numberFormat(row.children[7].children[1].textContent));
        total_venta = total_venta + TOTAL;

    });
    document.querySelector('[name=venta-total]').parentElement.classList.add('is-filled');
    document.querySelector('[name=venta-total]').value = `${formatter.currencyFormat(total_venta)}`;

}

function validarUltimaRow(row) {

    let num_rows = document.querySelectorAll('#tbl-lista-sku tbody tr').length;
    if(row.rowIndex == num_rows) setTableRows(1); 

}

function deleteRow(element) {

    let row = element.parentElement.parentElement;
    row.children[0].children[0].value = "";
    row.hidden = true;

}

function goToListVentas() {
    window.location = `/crm/ventas/lista-ventas/index.php`;
}

function setOptionsCantEtga() {

    let options = ``;
    for (let index = 0; index < 101; index++) {
        options += `<option value="${index}">${index}</option>`
    }
    return options;

}

function sumarCostoEnvio() {

    let input_costo_envio = document.querySelector('[name=costo-envio]');
    let input_total = document.querySelector('[name=venta-total]');
    if (input_costo_envio.value != "") {
        let new_total = parseFloat(formatter.numberFormat(input_total.value)) + parseFloat(input_costo_envio.value);
        input_total.value = formatter.currencyFormat(new_total);
    }

}

function restarCostoEnvio() {

    let input_costo_envio = document.querySelector('[name=costo-envio]');
    let input_total = document.querySelector('[name=venta-total]');
    if (input_costo_envio.value != "") {
        let new_total = parseFloat(formatter.numberFormat(input_total.value)) - parseFloat(input_costo_envio.value);
        input_total.value = formatter.currencyFormat(new_total);
        input_costo_envio.value = '0.00';
    }

}
