<?php

// Import PHPMailer classes into the global namespace
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require '../../../assets/php/phpMailer/vendor/autoload.php';

// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);
$id = $_POST['id'];

try {
    //Server settings
    $mail->SMTPDebug = 0;                               // Enable verbose debug output SMTP::DEBUG_SERVER
    $mail->isSMTP();                                    // Send using SMTP
    $mail->Host       = 'smtp.gmail.com';           // Set the SMTP server to send through
    $mail->SMTPAuth   = true;                           // Enable SMTP authentication
    $mail->Username   = 'info.cbcells@gmail.com';           // SMTP username
    $mail->Password   = 'cbcellss';            // SMTP password
    $mail->SMTPSecure = 'TLS';                          // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
    $mail->Port       = 587;                             // TCP port to connect to
 
    //Recipients  
    $title = 'Se registró una nueva venta *No responder correo*';
    $remitente = 'info.cbcells@gmail.com';    
    $receptor = 'info.cbcells@gmail.com';
    $message = '
        <!DOCTYPE html>
        <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="ie=edge">    
                <style>
                    .title {
                        color:white;
                        background-color: #64bc44;
                        text-align: center;
                        font-family: arial;
                        margin-left: 10%;
                        margin-right: 10%;            
                    }
                    .logo {
                        width: 20%;
                        margin-left: 40%;
                        margin-top: 10px;
                    }
                    .paragraph {                        
                        text-align: center;
                        margin-left: 25%;
                        margin-right: 25%;
                    }
                    .button {
                        background-color: red;
                        border: none;
                        margin-left: 35%;
                        margin-right: 35%;
                        text-align: center;
                        padding: 15px 32px;            
                    }
                    .button a {
                        color: white;
                        text-decoration: none;
                    }

                </style>
            </head>
            <body>
                <div class="container">     
                    <h1 class="title" >¡Nueva venta<br>Creada!</h1>
                    <br><br>
                    <h3 class="paragraph">Se creó un nueva venta. Para poder visualizarla ir a lista de ventas.</h3>
                    <h2 class="button"><a href="https://cbcells.mx/crm/ventas/editar-venta/index.php?venta_id='.$id.'">Ver venta</a></h2>
                </div>
            </body>
        </html>
    ';
    
    $mail->setFrom($remitente, $title);
    $mail->addAddress($receptor);

    // Content
    $mail->isHTML(true);   
    $mail->MsgHTML($message);
    $mail->Subject = 'Nueva venta registrada ID: '.$id;
   

    // Activo condificacción utf-8
    $mail->CharSet = 'UTF-8';

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}

?>