<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

try {
    $data = json_decode(file_get_contents('php://input'), true);
    $id = $data["detalle"][0]['id'];
    $parcial = $data["detalle"][0]['parcial'];

    $sql = "UPDATE `Ordenes` SET `Parcial`='$parcial' WHERE OrderId = $id";
    if ($conn->query($sql) === TRUE) {

        if($parcial=="si") {
            $sql = "INSERT INTO Parcialidades(`OrderId`,`Observacion`) VALUES('$id','Parcialidad Inicial')";
            $conn->query($sql);
            //get the parcial id
            $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $con->exec("set names utf8");
            $final_data=[];

            $query = "SELECT Id FROM `Parcialidades`where OrderId =$id";
            $datos = $con->query($query)->fetchAll(PDO::FETCH_ASSOC );

            foreach($datos as $row){
                $final_data[] = $row;
            }
            $parci = $final_data[0]['Id'];

            $query="INSERT INTO `ItemsOrd`(`ItemId`, `Descuento`,`Estado`, `Concepto`, `Valor`, `Total`, `Cantidad`, `Cantidad_Original`, `Entregado`, `OrderId`,`Parcialidad`,`Promo`) VALUES "; 
        }else{
            $query="INSERT INTO `ItemsOrd`(`ItemId`, `Descuento`,`Estado`, `Concepto`, `Valor`, `Total`, `Cantidad`, `Cantidad_Original`, `Entregado`, `OrderId`, `Promo`) VALUES "; 
        }


        $datos_invent=[];
        foreach($data['detalle'][1] as $row){

            $sku = $row['sku'];
            $dcto = $row['dcto'];
            $concepto = $row['concepto'];
            $precio = $row['precio'];
            $etga = $row['etga'];
            $total = $row['total'];
            $cant = $row['cant'];
            $promo = $row['promo'];

            //guardar datos para inventario            
            $datos_invent[] = array("id"=>$sku,"canti" =>$cant);

            //insert construccion
            if($parcial=="si") {
                $values="('$sku','$dcto','activo','$concepto','$precio','$total','$etga','$cant','$etga','$id','$parci','$promo'),";
            }else{
                $values="('$sku','$dcto','activo','$concepto','$precio','$total','$etga','$cant','$etga','$id','$promo'),";
            }
            $query .= $values;
        }
        $query = substr($query, 0, -1);
        if ($conn->query($query) === TRUE) {
            //inventario($datos_invent);
            $sql ="";
            $sql1 ="";
            foreach($datos_invent as $row){
                $sql1.="/UPDATE `Inventario` SET `Stock`= (`Stock` - ".$row['canti'].") WHERE Id =".$row['id']."";
                $sql = "UPDATE `Inventario` SET `Stock`= (`Stock` - ".$row['canti'].") WHERE Id =".$row['id']."";
                $conn->query($sql);
            }

            $result = ['type' => "success", 'msn' => "Venta creada correctamente",'datos'=>json_encode($datos_invent),'sql'=>$sql1];
        }else{
            $result = ['type' => "danger", 'msn' => "Problema del query",'query'=>$query];
        }
    }
    else {
       $result = ['type' => "danger", 'msn' => "Problema del query"];
    }
} catch (PDOException  $e) {
    $result = ['type' => "danger", 'msn' => "Problema de la conexión"];
}
echo json_encode($result);


function inventario($datos) {

    foreach($datos as $row){
        $id = $row['id'];
        $canti = $row['canti'];
        $sql="UPDATE `Inventario` SET `Stock`= (`Stock` - $canti) WHERE Id =$id";
        $conn->query($sql);
    }
}
?>