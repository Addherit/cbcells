<?php 
session_start();
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

try {
  $vendedor = $_POST['id_vendedor'];
  $medico = $_POST['nombre-medico'];
  $notas = $_POST['observaciones'];
  $envio = $_POST['costo-envio'];
  $guia = $_POST['guia'];
  $cuenta = $_POST['cuenta-bancaria'];
  $total = $_POST['venta-total'];
  $descuento = $_POST['descuento'];
  $paciente = $_POST['paciente'];
  $patologia = $_POST['patologia'];
  $tratamiento = $_POST['tratamiento'];
  //nuevos
  $cfdi = $_POST['cfdi'];
  $factura = $_POST['check-factura-electronica'];
  $cedula = $_POST['cedula-profesional'];


  $query=  "INSERT INTO `Ordenes`( `Estado`, `Parcial`, `VendedorId`, `Notas`,`MedId`, `Costo_env`, `Guia`,`Cuenta`,`VentaTotal`,`Descuento`, `Paciente`, `Patologia`,`Tratamiento`, `Cedula-profesional`,`Cfdi`,`Factura`) VALUES ('Abierto','no','$vendedor','$notas','$medico','$envio','$guia','$cuenta','$total','$descuento','$paciente','$patologia','$tratamiento','$cedula','$cfdi',$factura)";
  $final_data["query"]=$query;
  // Performs the $sql query on the server to insert the values
  if ($conn->query($query) === TRUE) {
    $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $datos = $con->query("SELECT MAX(OrderId) as Id FROM Ordenes")->fetchAll(PDO::FETCH_ASSOC );
    foreach($datos as $row){
      $final_data[] = $row;
    }
    //session
    $aux =$_SESSION["res"];
    $aux1 = $aux['userId'];
    $aux2 = $final_data[0]['Id'];
    //notificacion
    $queryn="INSERT INTO `Notificaciones`( `UserId`, `Mensaje`, `IdAccion`, `Ntype`, `Type`, `Mostrar`) VALUES ('$aux1','Registro de Nueva venta','$aux2','2','Registro de Nueva venta','A/C')";
    $conn->query($queryn);

    //resultado
    $result = ['type' => "success", 'msn' => "Orden creada correctamente",
    'id_venta'=>$final_data[0]['Id']];
  }
  else {
    $result = ['type' => "danger", 'msn' => "Problema del query",'query'=>$query];
  }

  $conn->close();
} catch (PDOException  $e) {
    $result = ['type' => "danger", 'msn' => "Problema de la conexión"];
}
echo json_encode($result);
?>