document.addEventListener('DOMContentLoaded', () => { setRowsVentas() })
document.querySelector('#form-search').addEventListener('submit', (event) => { event.preventDefault(); setRowsVentas() })
document.querySelector('#btn-new-venta').addEventListener('click', () => { goToNewVenta() })
document.querySelector('[name=periodo-filtro]').addEventListener('change', () => { setRowsVentas() })

function getVentas() {
    
    const FORM_DATA = new FormData(document.querySelector('#form-search'));
    FORM_DATA.append('filtro', document.querySelector('[name=periodo-filtro]').value)
    return fetch('php/select-ventas.php',{method: 'POST', body: FORM_DATA})
    .then(data => data.json())
    .then(data => {
        return data;
    })
    
}

function getItemsVenta(id_venta) {
    
    const FORM_DATA = new FormData();
    FORM_DATA.append('OrderId', id_venta)
    return fetch('php/select-items-venta.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        return data;
    })

}


async function setRowsVentas() {

    let ventas = await getVentas();
    if(ventas) {
        const ROWS = getRows(ventas);     
        document.querySelector('#tbl-lista-ventas tbody').innerHTML = ROWS;
          // utilizas la clase Alertas para remarcar los nuevas ventas en la lista mostrada en caso de que exista la var ids en la url
          alerta.highlightIds('tbl-lista-ventas');
    } else {
        const MSN = new Message('warning', 'No hay ventas para mostrar');
        MSN.showMessage();
    }    
    
}

function getRows(data) {   

    let rows = '';
    let text_color = ``;
    let factura = ``;


    data.forEach(venta => {
        text_color = getTextColor(venta.Estado)
        venta.Factura == 'true' ? factura = 'Si' : factura = 'No';
        rows += `
            <tr>
                <td><a href="#" class="text-danger btn-tbl-accion" onclick="deleteVenta(${venta.OrderId})"><span class="material-icons">delete</span> Eliminar</a></td>
                <td><a href="#" class="text-info btn-tbl-accion" onclick="editVenta(${venta.OrderId})"><span class="material-icons">visibility</span> Ver</a></td>
                <td>${venta.OrderId}</td>
                <td>${venta.Fecha}</td>
                <td>${venta.Vendedor}</td>
                <td>${venta.Medico}</td>
                <td class="text-center">${venta.TotalItems}</td>
                <td></td>
                <td class="text-center">${parseInt(venta.TotalItems) - parseInt(venta.EntregadosItems)}</td>
                <td>${venta.Domicilio}</td>
                <td class="text-center">${factura}</td>
                <td class="text-primary">
                    <span class="material-icons" style="vertical-align: text-top; font-size: 18px !important">attach_money</span>
                    <span>${formatter.currencyFormat(venta.Total)}</span>            
                </td>
                <td>${venta.Notas}</td>
                <td class="${text_color}">${venta.Estado}</td>        
            </tr>
        `;
    });

    return rows;

}

function getTextColor(estado) {

    let text_color = ``;
    
    switch (estado) {
        case 'Cancelada':
            text_color = 'text-danger';
            break;
        case 'Abierto':
            text_color = 'text-success';
            break;
        default:
            text_color = 'text-primary';
            break;
    }

    return text_color;

}

function deleteVenta(id_venta) {

    var r = confirm("Seguro que desea eliminarlo?");
    if (r == true) {
        const FORM_DATA = new FormData()
        FORM_DATA.append('id_venta', id_venta)
        fetch('php/delete-ventas.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json()) 
        .then(data => {
            const MSN = new Message(data.type, data.msn);
            MSN.showMessage();
            setRowsVentas();
        })
    }

}

function editVenta(id_venta) {
    window.location = `/crm/ventas/editar-venta/index.php?venta_id=${id_venta}`;
}

function goToNewVenta() {
    window.location = `/crm/ventas/nueva-venta/index.php`;
}

async function getDataDetalleVenta(OrderId) {
    
    let array = [];
    let total_productos = 0;
    let tipo_promocion = 'N/A';
    let pendiente_entrega = 0;
    const DATA = await getItemsVenta(OrderId);
    DATA.forEach(element => {
        total_productos = parseInt(total_productos) + parseInt(element.Cantidad_Original);
        pendiente_entrega = parseInt(pendiente_entrega) + (parseInt(element.Cantidad_Original) - parseInt(element.Entregado));
    });

    array.push({
        total_productos: total_productos,
        tipo_promocion: tipo_promocion,
        pendiente_entrega: pendiente_entrega
    })
    return array;
    
}