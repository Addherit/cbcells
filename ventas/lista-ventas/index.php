<!DOCTYPE html>
<html lang="en">

<head>
  <?php include '../../header.html' ?>
</head>

<body class="">
  <div class="wrapper ">
    <?php include '../../sidebar/sidebar.html' ?>
    <div class="main-panel">
      <?php include '../../sidebar/navbar.html' ?>
      <div class="content">
        <div class="container-fluid">  
            <div class="row" style="width: 100%">
                <div class="col-md-7">
                    <div class="form-group select-custom">
                        <label class="bmd-label-floating">Periodo</label>                          
                        <select class="custom-select" name="periodo-filtro" style="background-color: transparent"> 
                            <option value="" selected>Sin filtro</option>
                            <option value="dia">Día</option>                   
                            <option value="semana">Semana</option>
                            <option value="mes">Mes</option>
                            <option value="año">Año</option>
                        </select>
                    </div>                        
                </div>
                <div class="col-md-5">
                    <form class="navbar-form" id="form-search">
                        <div class="input-group no-border">
                            <input type="text" name="search-field" class="form-control" placeholder="Buscar venta">
                            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                            <i class="material-icons">search</i>
                            <div class="ripple-container"></div>
                            </button>
                        </div>
                    </form>
                </div>
            </div>       
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Lista de Ventas</h4>
                            <p class="card-category">Para poder editar o eliminar una venta, utiliza los botones de acción</p>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive" style="min-height: 100%">
                                <table class="table table-hover" id="tbl-lista-ventas">
                                    <thead class=" text-primary">
                                        <th colspan="2"></th>
                                        <th>ID</th>
                                        <th>Fecha y hora</th>
                                        <th>Vendedor</th>
                                        <th>Medico</th>
                                        <th>Total de productos</th>
                                        <th>Promoción</th>
                                        <th>Producto pendiente entrega</th>
                                        <th>Dirección de envío</th>
                                        <th>Factura</th>
                                        <th>Total</th>
                                        <th>Notas</th>
                                        <th>Estado</th>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button class="btn btn-primary float-right" id="btn-new-venta"><span class="material-icons">add</span>Nueva venta</button>                                                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>           
            </div>
            </div>
        </div>
      </div> 
</body>
<?php include '../../footer.html' ?>
<script src="js/lista-ventas.js"></script>


</html>