<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

try {
  $data = $_POST['OrderId'];
  $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $con->exec("set names utf8");
  $final_data=[];

//  $query = "SELECT io.*,i.SKu,i.Precio FROM ItemsOrd io inner join Inventario i on io.ItemId = i.Id where io.OrderId=$data and io.Estado='Activo'";
  //$query= "SELECT io.*,CASE WHEN io.Promo = 'si' THEN t.Sku ELSE i.Sku END AS Sku,CASE WHEN io.Promo = 'si' THEN t.Precio ELSE i.Precio END AS Precio FROM ItemsOrd io inner join Inventario i on io.ItemId = i.Id  left join TipoDescuento t on io.ItemId = t.id where io.OrderId=$data and io.Estado='Activo'";
  $query ="SELECT io.Id,io.ItemId,io.Datecreated,io.Descuento,io.Estado,io.Valor,io.Total,io.Cantidad,io.Cantidad_Original,io.Entregado,io.OrderId,io.Parcialidad,io.Promo,CASE WHEN io.Promo = 'si' THEN t.Sku ELSE i.Sku END AS Sku,CASE WHEN io.Promo = 'si' THEN t.Precio ELSE i.Precio END AS Precio, CASE WHEN io.Promo = 'si' THEN t.Concepto ELSE i.Nombre END AS Concepto FROM ItemsOrd io inner join Inventario i on io.ItemId = i.Id left join TipoDescuento t on io.ItemId = t.id where io.OrderId=$data and io.Estado='Activo' ";
  $datos = $con->query($query)->fetchAll(PDO::FETCH_ASSOC );

  foreach($datos as $row){
    $final_data[] = $row;
  }
  echo json_encode($final_data);
} catch(PDOException $e) {
    echo $result = ["mensaje" => "Error: ".$e->getMessage()];
}
?>