"use strict";
document.addEventListener('DOMContentLoaded', () => { 
    setTblInventario();
    dateRangePickeer();
    setTableRowsModal(3); 
})
document.querySelector('#btn-add').addEventListener('click', () => { addStock() })
document.querySelector('#btn-remove').addEventListener('click', () => { removeStock() })

document.querySelector('#btn-add-promo').addEventListener('click', () => { addPromocion() })
document.querySelector('#btn-add-item').addEventListener('click', () => { addItem() })
document.querySelector('#btn-save-changes-articulo').addEventListener('click', () => { editItemPrice() })

function getInventario() {
    
    return fetch('php/select-inventario.php')
    .then(data => data.json())
    .then(data => {
        return data;
    })

}

function getOfertas() {
    
    return fetch('php/select-descuentos.php')
    .then(data => data.json())
    .then(data => {
        return data;
    })

}

async function setTblInventario() {

    let ofertas = await getOfertas();
    let data = await getInventario();
    const ROWS = getRows(data, ofertas);    
    document.querySelector('#tbl-inventario tbody').innerHTML = ROWS;
    $('[data-toggle="tooltip"]').tooltip();

}

function getRows(data, ofertas) {
    
    //obtienes las filas con el formato de la tabla
    let filas = '';
    data.forEach(sku => {
        let precio = formatter.numberFormat(sku.Precio)
        filas += `
            <tr title="Editar item" data-toggle="tooltip" data-placement="bottom">
                <td style="cursor: pointer" onclick="openModalEditItem(this, '${sku.Id}')"><a href="#" class="text-danger btn-tbl-accion" onclick="deleteItem(${sku.Id})"><span class="material-icons">delete</span>Eliminar</a></td>
                <td style="cursor: pointer" onclick="openModalEditItem(this, '${sku.Id}')">${sku.Sku}</td>
                <td style="cursor: pointer" onclick="openModalEditItem(this, '${sku.Id}')">${sku.Nombre}</td>
                <td style="cursor: pointer" onclick="openModalEditItem(this, '${sku.Id}')" class="text-primary">$ ${formatter.currencyFormat(precio)}</td>
                <td style="cursor: pointer" onclick="openModalEditItem(this, '${sku.Id}')">${sku.Stock}</td>
                <td class="text-center background-td-editable" contenteditable>0</td>
            </tr>
        `
    });
    ofertas.forEach(oferta => {
        let precio = formatter.numberFormat(oferta.Precio)
        filas += `
            <tr  title="Editar item" data-toggle="tooltip" data-placement="bottom">
                <td style="cursor: pointer" onclick="openModalEditItem(this, '${oferta.id}')"><a href="#" class="text-danger btn-tbl-accion" onclick="deletePromociones(${oferta.id})"><span class="material-icons">delete</span>Eliminar</a></td>
                <td style="cursor: pointer" onclick="openModalEditItem(this, '${oferta.id}')">${oferta.Sku}</td>
                <td style="cursor: pointer" onclick="openModalEditItem(this, '${oferta.id}')">${oferta.Concepto}</td>
                <td style="cursor: pointer" onclick="openModalEditItem(this, '${oferta.id}')" class="text-primary">$ ${formatter.currencyFormat(precio)}</td>
                <td style="cursor: pointer" onclick="openModalEditItem(this, '${oferta.id}')"></td>
                <td></td>
            </tr>
        `
    });
    return filas;

}

function addStock() {

    const ALL_SKUS = getJsonNewAdd();
    const FORM_DATA = new FormData();
    FORM_DATA.append('skus', JSON.stringify(ALL_SKUS))
    fetch('php/update-stock.php',{ method: 'POST', body: FORM_DATA })
    .then (data => data.json())
    .then(data => {        
        //mensaaje de confirmacion
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
        setTblInventario();
    })

}

function removeStock() {

    const ALL_SKUS = getJsonNewAdd();
    const FORM_DATA = new FormData();
    FORM_DATA.append('skus', JSON.stringify(ALL_SKUS))
    fetch('php/restar-stock.php',{ method: 'POST', body: FORM_DATA })
    .then (data => data.json())
    .then(data => {        
        //mensaaje de confirmacion
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
        setTblInventario();
    })

}

function getJsonNewAdd() {

    //función para obteer todas las cantidades a agregar de casa sku y retornar la info en formato json
    let allSkus = {"Skus":[]};
    document.querySelectorAll('#tbl-inventario tbody tr').forEach((tr, index) => {
        allSkus["Skus"].push({"Sku": tr.children[1].textContent, "toRemove": tr.children[5].textContent});
    });    
    return allSkus;

}

function deleteItem(id) {

    var r = confirm("Seguro que desea eliminarlo?");
    if (r == true) {
        const FORM_DATA = new FormData();
        FORM_DATA.append('id', id )
        fetch('php/delete-inventario.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
            const MSN = new Message(data.type, data.msn);
            MSN.showMessage();
            setTblInventario();
        })
    }

}

function deletePromociones(id) {

    var r = confirm("Seguro que desea eliminarlo?");
    if (r == true) {
        const FORM_DATA = new FormData();
        FORM_DATA.append('id', id )
        fetch('php/delete-promocion.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {

            const MSN = new Message(data.type, data.msn);
            MSN.showMessage();
            setTblInventario();
        })
    }

}

function addItem() {

    const FORM_DATA = new FormData(document.querySelector('#form-add-item'))

    fetch('php/insert-inventario.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        if (data.type == 'success') $('#modal-nuevo-item').modal('hide');
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
        setTblInventario();
    })
    
}

function addPromocion() {
    
    let array_detalle = getDetalleItemsNewPromo();
   
    const DATA_MODAL = {
        "header":[        
            {
                sku : document.querySelector('[name=sku-modal-promo]').value,
                concepto : document.querySelector('[name=concepto]').value,
                precio : document.querySelector('[name=precio-modal-promo]').value,
                rangoinicio : document.querySelector('[name=promo-startDate]').value,
                rangofin : document.querySelector('[name=promo-endDate]').value,
            }
        ],
        "detalle":[        
            array_detalle
        ]
    }
    console.log(DATA_MODAL);


    fetch('php/insert-promo.php', { method: 'POST', body: JSON.stringify(DATA_MODAL) })
    .then(data => data.json())
    .then(data => {

        if (data.type == 'success') $('#modal-nueva-promocion').modal('hide');
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
        setTblInventario();
    })

}

function getDetalleItemsNewPromo() {

    const TRS = document.querySelectorAll('#tbl-lista-sku-promo tbody tr');
    let response = [];
    TRS.forEach(tr => {
        if (tr.children[0].children[0].value) {
            let array = {
                sku: tr.children[0].children[0].value,
                concepto: tr.children[1].textContent,
                cantidad: tr.children[2].textContent
            }
            response.push(array);
        }
    });
    return response;
}

function dateRangePickeer() {

    // Inicializa todos los dateRnagePicker con clase dateFilter
    $('.datefilter').daterangepicker({
      autoUpdateInput: false,
      locale: {
          cancelLabel: 'Clear'
      }
    });
    // Se inicializa las funcionalidades del dateRangePicker()
    applyDateRangePicker();
  
  }
  function applyDateRangePicker() {

    // Esta funcion se ejecuta cuando haces click en el botón de apply de dateRangePicker
    // esto es de ranking promocion-----------------------------------------------------------------------
    $('input[name="date-range-promo"]').on('apply.daterangepicker', (ev, picker) => {
      let start_date = picker.startDate.format('YYYY-MM-DD');
      let end_date = picker.endDate.format('YYYY-MM-DD');
      document.querySelector('[name=date-range-promo]').parentElement.classList.add('is-filled');
      $('input[name="date-range-promo"]').val(start_date + ' - ' + end_date);  
      document.querySelector('[name=promo-startDate]').value = start_date;
      document.querySelector('[name=promo-endDate]').value = end_date;    
    });
    
    $('input[name="date-range-promo"]').on('cancel.daterangepicker', (ev, picker) => {
      $('input[name="date-range-promo"]').val('');    
    });
  
  }
  
async function getOptiosSkus() {

    const SKUS = await getInventario()
    let options = `<option value="" selected disabled>Selecciona uno</option>`;
    SKUS.forEach(sku => {
        options += `<option value="${sku.Id}">${sku.Nombre}</option>`
    });    
    return options;
}

async function getTableRows(num_filas) {
    
    const SKU_OPTIONS = await getOptiosSkus();    
    let rows = '';
    for (let index = 0; index < num_filas; index++) {
        rows += 
        `<tr>
            <td>
                <select class="custom-select" style="border: none" onchange="selectSkuModal(this)">${SKU_OPTIONS}</select>
            </td>
            <td></td>   
            <td contentEditable style="background-color: #ffff4926" class="text-center">0</td>
            
        </tr>`
    }
    return rows;    

}

async function setTableRowsModal(num_rows) {

    const ROWS = await getTableRows(num_rows);    
    if (num_rows == 3) {
        document.querySelector('#tbl-lista-sku-promo tbody').innerHTML = ROWS;
    } else {
        $('#tbl-lista-sku-promo tbody').append(ROWS);
    }

}

function validarUltimaRow(row) {

    let num_rows = document.querySelectorAll('#tbl-lista-sku-promo tbody tr').length;
    if(row.rowIndex == num_rows) setTableRowsModal(1); 

}

async function selectSkuModal(select_sku) {

    const ROW = select_sku.parentElement.parentElement;    
    const SKU = await getSkus(select_sku.value);
    ROW.children[1].textContent = SKU[0].Nombre;
    validarUltimaRow(ROW)

}

function getSkus(sku) {

    const FORM_DATA = new FormData();
    FORM_DATA.append('sku', sku);
    return fetch('php/select-skus.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        return data;
    })

}

function openModalEditItem(obj, id) {

    addClassIsFilled();
    document.querySelector('[name=modal-det-sku]').value = obj.parentElement.children[1].textContent;
    document.querySelector('[name=modal-det-concepto]').value = obj.parentElement.children[2].textContent;
    document.querySelector('[name=modal-det-precio]').value = obj.parentElement.children[3].textContent.slice(1);



    //Detalle de la promoción
    const FORM_DATA = new FormData()
    FORM_DATA.append('IdPromo', id)
    fetch('php/select-items-promo.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        let linea = '';

        data.forEach(item => {
            linea += `<tr>
                    <td>${item.Id}</td>
                    <td>${item.Nombre}</td>
                    <td>${item.Sku}</td>
                    <td>${item.Cantidad_Original}</td>                   
                </tr>`
        });
        document.querySelector('#tbl-detalle-promo tbody').innerHTML = linea

    })




    

    $('#modal-edicion-articulo').modal('show');

}

function editItemPrice() {
    
    const SKU = document.querySelector('[name=modal-det-sku]').value
    const PRECIO = document.querySelector('[name=modal-det-precio]').value
    const NOMBRE = document.querySelector('[name=modal-det-concepto]').value
    const FORM_DATA = new FormData()
    FORM_DATA.append('sku', SKU)
    FORM_DATA.append('precio', PRECIO)
    FORM_DATA.append('nombre', NOMBRE)
    fetch('php/update-item.php', { method: 'POST', body: FORM_DATA})
    .then(data => data.json())
    .then(data => {
        //mensaaje de confirmacion
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
        $('#modal-edicion-articulo').modal('hide');
        setTblInventario();
    })

}

function addClassIsFilled() {

    //funcion para hacer el efecto de focus en input de datos
    const DIVS = document.querySelectorAll('.bmd-form-group');
    DIVS.forEach(element => {
        element.classList.add('is-filled');        
    });

}