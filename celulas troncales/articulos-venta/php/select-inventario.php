<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

try {
  $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $final_data=[];
  $con->exec("set names utf8");
  $datos = $con->query("Select * from Inventario WHERE Estado = 'Activo'")->fetchAll(PDO::FETCH_ASSOC );

  foreach($datos as $row){
    $final_data[] = $row;
  }
    echo json_encode($final_data);
} catch(PDOException $e) {
    echo $result = ["mensaje" => "Error: ".$e->getMessage()];
}
?>