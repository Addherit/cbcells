<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

try {
  $skus = $_POST['skus'];
  $aux = json_decode($skus);

  foreach($aux->Skus as $row){
    if($row->toAdd > 0){
      $sku =$row->Sku;
      $stock=$row->toAdd;
      $sql = "UPDATE `Inventario` SET `Stock` = (`Stock` + $stock) WHERE `Sku` = '$sku'";

      if ($conn->query($sql) === TRUE) {
        $result = ["type"=> "success","msn" => "Se actualizaron bien los stocks"];
      }
      else {
        $result = ["type"=> "danger","msn" => "Sku no encontrado"];
      }

    }
  }
  $conn->close();
} catch(PDOException $e) {
   $result = ["type"=>"danger","msn" => "Error: ".$e->getMessage()];
}
  echo json_encode($result);
?>