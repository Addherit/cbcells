<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

if($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        //vars from form
        $nombre= $_POST['nombre'];
        $sku= $_POST['sku'];
        $precio= $_POST['precio'];
        $stock= $_POST['stock'];

        // sql query for INSERT INTO inventario
        $sql = "INSERT INTO `Inventario`( `Nombre`, `Sku`, `Precio`, `Stock`) VALUES ('$nombre','$sku','$precio','$stock')";

        if ($conn->query($sql) === TRUE) {
            $result = ['type' => "success", 'msn' => "Inventario creado correctamente"];
        }
        else {
            $result = ['type' => "danger", 'msn' => "Problema del query"];
        }

        $conn->close();
    } catch (PDOException  $e) {
        $result = ['type' => "danger", 'msn' => "Problema de la conexión"];
    }
    $result["query"] = $sql;

    echo json_encode($result);
}
?>