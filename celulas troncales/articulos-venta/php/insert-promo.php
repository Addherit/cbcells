<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

try {
    $data = json_decode(file_get_contents('php://input'), true);
    $concepto = $data["header"][0]['concepto'];
    $precio = $data["header"][0]['precio'];
    $rangofin = $data["header"][0]['rangofin'];
    $rangoinicio = $data["header"][0]['rangoinicio'];
    $sku = $data["header"][0]['sku'];

    // sql query for INSERT INTO inventario
    $sql = "INSERT INTO `TipoDescuento`(`Concepto`, `Precio`, `RangoInicio`, `RangoFin`, `Sku`) VALUES ('$concepto','$precio','$rangoinicio','$rangofin','$sku')";
    $conn->set_charset("utf8");

    if ($conn->query($sql) === TRUE) {
        //obetener el id de la promo
        $sqlid="SELECT MAX(Id)as Id FROM `TipoDescuento`";
        $did = $con->query($sqlid)->fetchAll(PDO::FETCH_ASSOC );
        $idpromo = $did[0]['Id'];
        //contruccion items 
        $sqlitems="INSERT INTO `ItemsPromo`( `Nombre`, `cantidad`, `IdItem`, `IdPromo`) Values ";
        foreach ($data["detalle"][0] as $row) {
            $nombre = $row['concepto'];
            $cantidad = $row['cantidad'];
            $iditem = $row['sku'];

            $aux="('$nombre','$cantidad',$iditem,$idpromo),";
            $sqlitems .= $aux;
        }
        //borrar la ultima coma
        $sqlitems = substr($sqlitems, 0, -1);
        //insert
    //j    print_r($sqlitems);
        $conn->query($sqlitems);

        $result = ['type' => "success", 'msn' => "Descuento creado correctamente",'Sql'=>$sql];

    }
    else {
        $result = ['type' => "danger", 'msn' => "Problema del query"];
    }

    $conn->close();
   // $result = ['type' => "success", 'msn' => "Sin problemas",'sql'=>$sql];
} catch (PDOException  $e) {
    $result = ['type' => "danger", 'msn' => "Problema de la conexión"];
}
echo json_encode($result);
?>