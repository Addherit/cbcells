<!DOCTYPE html>
<html lang="en">

<head>
  <?php include '../../header.html' ?>
</head>

<body class="">
  <div class="wrapper ">
    <?php include '../../sidebar/sidebar.html' ?>
    <div class="main-panel">
      <?php include '../../sidebar/navbar.html' ?>
      <div class="content">
        <div class="container-fluid">     
          <?php include 'modal.html'?>
          <div class="row">
            <div class="col-md-12">
                <div class="card">
                  <div class="card-header card-header-primary">
                      <h4 class="card-title ">Articulos de venta</h4>
                      <p class="card-category">1.En la columna azul escribe la cantidad a sumar</p>
                      <p class="card-category">2.Clic en el botón Agregar para actualizar el stock de cada SKU</p>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive" style="min-height: 100%">
                        <table class="table table-hover" id="tbl-inventario">
                          <thead class=" text-primary">      
                            <th></th>                          
                            <th>SKU</th>
                            <th>Concepto</th>
                            <th>Precio</th>
                            <th>Stock</th>                            
                            <th style="width: 10px">Agregar</th>                            
                          </thead>
                          <tbody>                                      
                          </tbody>
                        </table>
                    </div>
                    <div class="row">
                      <div class="col-12">
                        <button class="btn btn-primary" id="btn-new-promocion" data-toggle="modal" data-target="#modal-nueva-promocion"><span class="material-icons">card_giftcard</span> Nueva Promoción</button>
                        <button class="btn btn-primary float-right" id="btn-add"><span class="material-icons">add</span> Sumar Stock</button>
                        <button class="btn btn-primary float-right" id="btn-remove"><span class="material-icons">remove</span> Restar Stock</button>
                        <button class="btn btn-primary float-right" id="btn-new-item" data-toggle="modal" data-target="#modal-nuevo-item"><span class="material-icons">input</span> Nuevo item</button>
                       
                      </div>
                    </div>
                  </div>
                </div>
            </div>           
          </div>      
        </div>
      </div> 
</body>
<?php include '../../footer.html' ?>
<script src="js/inventario.js"></script>

</html>