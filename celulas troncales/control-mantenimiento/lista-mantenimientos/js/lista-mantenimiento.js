document.addEventListener('DOMContentLoaded', () => {
    setMantenimientos()

})
document.querySelector('#btn-new-mantenimiento').addEventListener('click', () => { goToNewMantenimiento() })

function getMantenimientos() {
    
    return fetch('php/select-mante.php')
    .then(data => data.json())
    .then(data => {
        return data
    })

}

async function setMantenimientos() {
    
    const MANTENIMIENTOS = await getMantenimientos();
    const ROWS = getStructuredRows(MANTENIMIENTOS);
    document.querySelector('#tbl-mantenimiento tbody').innerHTML = ROWS;    

}

function getStructuredRows(data) {
    
    let linea = '';
    data.forEach(mantenimiento => {
        linea += `
            <tr>
                <td><a href="#" class="text-danger btn-tbl-accion" onclick="deleteMantenimiento(${mantenimiento.Id})"><span class="material-icons">delete</span> Eliminar</a></td>
                <td><a href="#" class="text-info btn-tbl-accion" onclick="editMantenimiento(${mantenimiento.Id})"><span class="material-icons">visibility</span> Ver</a></td>
                <td>${mantenimiento.Descripcion}</td>
                <td>${mantenimiento.Marca}</td>
                <td>${mantenimiento.Modelo}</td>
                <td>${mantenimiento.NoSerie}</td>
                <td>${mantenimiento.Entregado}</td>
                <td>${mantenimiento.Mantenimiento}</td>
                <td>${mantenimiento.Provedor}</td>
                <td>${mantenimiento.Contacto}</td>
                <td>${mantenimiento.Comentarios}</td>
                <td>${mantenimiento.Datecreated}</td>
                <td class="text-primary">${mantenimiento.Estado}</td>
            </tr>`
    });

    return linea;               
}      

function goToNewMantenimiento() {
    window.location = `/crm/celulas troncales/control-mantenimiento/nuevo-mantenimiento/index.php`;
}

function deleteMantenimiento(id) {

    var r = confirm("Seguro que desea eliminarlo?");
    if (r == true) {
        const FORM_DATA = new FormData()
        FORM_DATA.append('Id', id);
        fetch('php/delete-mante.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
            const MSN = new Message(data.type, data.msn);
            MSN.showMessage();
            setMantenimientos();
        })
    }

}

function editMantenimiento(id) {
    window.location = `/crm/celulas troncales/control-mantenimiento/editar-mantenimiento/index.php?id=${id}`;
}