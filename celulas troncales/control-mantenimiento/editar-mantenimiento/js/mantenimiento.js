

document.addEventListener('DOMContentLoaded', () => {
    SetMantenimiento();
})

document.querySelector('#btn-edit-mantenimiento').addEventListener('click', () => { editInmunoterapia() })
document.querySelector('#btn-lista-mantenimiento').addEventListener('click', () => { goToListMantenimiento() })

function getRows(data) {

    let rows = '';
    data.forEach(element => {
        rows += 
        `<tr>
            <td contentEditable class="background-td-editable">${element.Descripcion}</td>
            <td contentEditable class="background-td-editable">${element.Marca}</td>
            <td contentEditable class="background-td-editable">${element.Modelo}</td>
            <td contentEditable class="background-td-editable">${element.NoSerie}</td>
            <td contentEditable class="background-td-editable"><input type="date" value="${element.Entregado}"></td>
            <td contentEditable class="background-td-editable"><input type="date" value="${element.Mantenimiento}"></td>
            <td contentEditable class="background-td-editable">${element.Provedor}</td>
            <td contentEditable class="background-td-editable">${element.Contacto}</td>
            <td contentEditable class="background-td-editable">${element.Comentarios}</td>
        </tr>`
    });
    return rows;    

}


async function SetMantenimiento() {

    const DATA = await getMantenimiento()
    const ROWS = getRows(DATA);
    document.querySelector('#tbl-mantenimiento tbody').innerHTML = ROWS;

}

function getMantenimiento() {

    const PARAMS_URL = new URLSearchParams(window.location.search);
    const FORM_DATA = new FormData()
    FORM_DATA.append('id', PARAMS_URL.get('id'))
    return fetch('php/select-mante.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        return data;
    })

}

function editInmunoterapia() {

    const PARAMS_URL = new URLSearchParams(window.location.search);
    const TRS = document.querySelectorAll('#tbl-mantenimiento tbody tr');
    TRS.forEach(row => {
        
        if (row.children[0].textContent != '') {
            let form_data = new FormData()
            form_data.append('id', PARAMS_URL.get('id'));
            form_data.append('descripcion', row.children[0].textContent);
            form_data.append('marca', row.children[1].textContent);
            form_data.append('modelo', row.children[2].textContent);
            form_data.append('serie', row.children[3].textContent);
            form_data.append('entregado', row.children[4].children[0].value);
            form_data.append('mantenimiento', row.children[5].children[0].value);
            form_data.append('proveedor', row.children[6].textContent);
            form_data.append('contacto', row.children[7].textContent);
            form_data.append('comentarios', row.children[8].textContent);

            fetch('php/edit-mante.php', { method: 'POST', body: form_data })
            .then(data => data.json())
            .then(data => {
                const MSN = new Message(data.type, data.msn);
                MSN.showMessage();
            })
        }
    });

}



function goToListMantenimiento() {
    window.location = `/crm/celulas troncales/control-mantenimiento/lista-mantenimientos/index.php`;
}