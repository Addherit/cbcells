<?php 
session_start();
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

if($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        $skus = $_POST['skus'];
        $aux = json_decode($skus);

        foreach($aux->Skus as $row){
        if($row->cantidad > 0){
            $sku =$row->Sku;
            $stock=$row->cantidad;
            $sql = "UPDATE `Insumos` SET `Stock` = (`Stock` + $stock) WHERE `Id` = $sku";
            if ($conn->query($sql) === TRUE) {
                $result = ["type"=> "success","msn" => "Se actualizaron bien los stocks"];
            }
            else {
                $result = ["type"=> "danger","msn" => "Sku no encontrado"];
            }
        }
        }
            $aux =$_SESSION["res"];
            $aux1 = $aux['userId'];

            $query = "INSERT INTO `Historial`(`Mensaje`, `Usuario`) VALUES ('Se actualizaron los stocks de materia prima','$aux1')";
            mysqli_query($conn, $query);

        $conn->close();
    } catch (PDOException  $e) {
        $result = ['type' => "danger", 'msn' => "Problema de la conexión"];
    }
    $result["query"] = $sql;

    echo json_encode($result);
}
?>