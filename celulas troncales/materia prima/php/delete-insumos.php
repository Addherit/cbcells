<?php 
session_start();
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

if($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        //vars from form
        $sku= $_POST['id'];

        // sql query for INSERT INTO Medicos
        $sql = "update Insumos set Estado ='desactivo' where Id =$sku";
        
        // Performs the $sql query on the server to insert the values
        if (mysqli_query($conn, $sql)) {
            $result = ['type' => "success", 'msn' => "Insumo eliminado correctamente"];

            $aux =$_SESSION["res"];
            $aux1 = $aux['userId'];

            $query = "INSERT INTO `Historial`(`Mensaje`, `Usuario`) VALUES ('Insumo eliminado correctamente','$aux1')";
            mysqli_query($conn, $query);
        } else {
            $result = ['type' => "danger", 'msn' => "problema del query",'query'=>$sql];
        }
        mysqli_close($conn);
    } catch (PDOException  $e) {
        $result = ['type' => "danger", 'msn' => "problema de la conexion"];
    }

    echo json_encode($result);
}
?>