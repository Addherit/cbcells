<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

try {
  $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $final_data=[];

    $query = "SELECT * FROM `Insumos` where `Estado` = 'Activo' ";
  
  $datos = $con->query($query)->fetchAll(PDO::FETCH_ASSOC );

  foreach($datos as $row){
    $final_data[] = $row;
  }
   echo json_encode($final_data);
} catch(PDOException $e) {
    echo $result = ["mensaje" => "Error: ".$e->getMessage()];
}
?>