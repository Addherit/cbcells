<?php 
session_start();
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

if($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        //vars from form
        $concepto= $_POST['nombre'];
        $sku= $_POST['sku'];
        $stock= $_POST['stock'];
        $prove= $_POST['proveedor'];
        $stock_min= $_POST['stock-minimo'];

        // sql query for INSERT INTO inventario
        $sql = "INSERT INTO `Insumos`( `Sku`, `Concepto`, `Provedor`, `Stock`, `Stock_min`) VALUES ('$sku','$concepto','$prove','$stock','$stock_min')";

        if ($conn->query($sql) === TRUE) {
            $result = ['type' => "success", 'msn' => "Insumo creado correctamente"];

            $aux =$_SESSION["res"];
            $aux1 = $aux['userId'];

            $query = "INSERT INTO `Historial`(`Mensaje`, `Usuario`) VALUES ('Insumo creado correctamente','$aux1')";
            mysqli_query($conn, $query);

        }
        else {
            $result = ['type' => "danger", 'msn' => "Problema del query"];
        }

        $conn->close();
    } catch (PDOException  $e) {
        $result = ['type' => "danger", 'msn' => "Problema de la conexión"];
    }
    $result["query"] = $sql;

    echo json_encode($result);
}
?>