"use strict";

document.addEventListener('DOMContentLoaded', () => { 
    setInsumosList();
})
document.querySelector('#btn-add-insumo').addEventListener('click', () => { addInsumo() });
document.querySelector('#btn-add-stock').addEventListener('click', () => { editStock('suma') });
document.querySelector('#btn-rest-stock').addEventListener('click', () => { editStock('resta') });

function getInsumoslist() {

    return fetch('php/select-insumos.php')
    .then(data => data.json())
    .then(data => {
        return data;
    })
}

async function setInsumosList() {

    const INSUMOS = await getInsumoslist();
    let lineas = '';
    INSUMOS.forEach(insumo => {
        lineas += `
            <tr>
                <td id="${insumo.Id}"><a href="#" class="text-danger btn-tbl-accion" onclick="deleteInsumo('${insumo.Id}')"><span class="material-icons">delete</span>Eliminar</a></td>
                <td>${insumo.Sku}</td>
                <td>${insumo.Concepto}</td>
                <td>${insumo.Provedor}</td>
                <td>${insumo.Stock}</td>
                <td>${insumo.Stock_min}</td>
                <td contenteditable class="text-center background-td-editable">0</td>
            </tr>
        `
    });
    document.querySelector('#tbl-insumos tbody').innerHTML = lineas;

}

function deleteInsumo(Id) {

    var r = confirm("Seguro que desea eliminarlo?");
    if (r == true) {
        const FORM_DATA = new FormData()
        FORM_DATA.append('id', Id);
        fetch('php/delete-insumos.php', { method: 'POST', body: FORM_DATA})
        .then(data => data.json())
        .then(data => {
            
            //mensaaje de confirmacion
            const MSN = new Message(data.type, data.msn);
            MSN.showMessage();
            setInsumosList();

        })
    }

}

function addInsumo() {

    const FORM_DATA = new FormData(document.querySelector('#form-add-insumo'))

    fetch('php/insert-insumos.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {

        if(data.type == 'success') $('#modal-nuevo-insumo').modal('hide');
        //mensaaje de confirmacion
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
        setInsumosList();
    })

}

function editStock(operacion) {

    const ALL_SKUS = getJsonNewAdd();
    const FORM_DATA = new FormData();
    FORM_DATA.append('operacion', operacion)
    FORM_DATA.append('skus', JSON.stringify(ALL_SKUS))
    fetch('php/update-insumos.php',{ method: 'POST', body: FORM_DATA })
    .then (data => data.json())
    .then(data => {     

        //mensaaje de confirmacion
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
        setInsumosList();
    })

}
    
function getJsonNewAdd() {

    //función para obteer todas las cantidades a agregar de cada sku y retornar la info en formato json
    let allSkus = {"Skus":[]};
    document.querySelectorAll('#tbl-insumos tbody tr').forEach(tr => {
        allSkus["Skus"].push({"Sku": tr.children[0].id, "cantidad": tr.children[6].textContent});
    });    
    return allSkus;

}