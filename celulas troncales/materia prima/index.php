<!DOCTYPE html>
<html lang="en">

<head>
  <?php include '../../header.html' ?>
</head>

<body class="">
  <div class="wrapper ">
    <?php include '../../sidebar/sidebar.html' ?>
    <div class="main-panel">
      <?php include '../../sidebar/navbar.html' ?>
      <div class="content">
        <div class="container-fluid">     
          <?php include 'modal.html'?>
          <div class="row">
            <div class="col-md-12">
                <div class="card">
                  <div class="card-header card-header-primary">
                      <h4 class="card-title ">Materia prima</h4>
                      <p class="card-category">1.En la columna azul escribe la cantidad a sumar</p>
                      <p class="card-category">2.Clic en el botón Agregar/Restar para actualizar el insumo</p>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive" style="min-height: 100%">
                        <table class="table table-hover" id="tbl-insumos">
                          <thead class=" text-primary">      
                            <th></th>                          
                            <th>SKU</th>
                            <th>Concepto</th>
                            <th>Proveedor</th>
                            <th>Stock</th>
                            <th>Stock minimo</th>                            
                            <th style="width: 10px">Agregar</th>                            
                          </thead>
                          <tbody>                                      
                          </tbody>
                        </table>
                    </div>
                    <div class="row">
                      <div class="col-12">
                        <button class="btn btn-primary float-right" id="btn-add-stock"><span class="material-icons">add</span> Agregar</button>
                        <button class="btn btn-primary float-right" id="btn-rest-stock"><span class="material-icons">remove</span> Restar</button>
                        <button class="btn btn-primary float-right" id="btn-new-insumo" data-toggle="modal" data-target="#modal-nuevo-insumo"><span class="material-icons">input</span> Nuevo Insumo</button>                       
                      </div>
                    </div>
                  </div>
                </div>
            </div>           
          </div>      
        </div>
      </div> 
</body>
<?php include '../../footer.html' ?>
<script src="js/insumos.js"></script>

</html>