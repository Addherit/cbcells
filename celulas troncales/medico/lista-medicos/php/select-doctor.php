<?php 
session_start();
header('Content-type: application/json');
include_once('../../../../assets/db/conexion.php');

$final_data = Array();
try {
    // session
    $aux =$_SESSION["res"];
    $userid = $aux['userId'];
    $type = $aux['Tipo_usuario'];
//   print_r($aux) ;
    //construccion
    $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $con->exec("SET NAMES 'utf8'");
    $data = $_POST['search-field'];
    $filtro = $_POST['filtro'];
    $token = $_POST['token'];//nombre-email-telefono
    if($type == 2){
        switch ($token) {
            case 'nombre':
                $query = "SELECT  d.`DocId`, d.`Nombre`, d.`Apellido`, d.`Cedula`, d.`Especialidad`, d.`CreatedDate`, d.`Correo`, d.`Telefono`, d.`Domicilio`, d.`Tipo_descuento`, d.`Ingreso`, CONCAT(u.Nombre ,' ',u.Apellido) as 'Vendedor', d.`estado` FROM Doctores d inner join Users u on u.userId = d.Vendedor where ((d.estado ='activo' and d.Nombre LIKE '%$data%') OR (d.Apellido LIKE '%$data%'and d.estado ='activo' )) and d.`Vendedor`=$userid order by d.`DocId` desc ";
                break;
            case 'email':
                $query = "SELECT  d.`DocId`, d.`Nombre`, d.`Apellido`, d.`Cedula`, d.`Especialidad`, d.`CreatedDate`, d.`Correo`, d.`Telefono`, d.`Domicilio`, d.`Tipo_descuento`, d.`Ingreso`, CONCAT(u.Nombre ,' ',u.Apellido) as 'Vendedor', d.`estado` FROM Doctores d inner join Users u on u.userId = d.Vendedor where (d.estado ='activo' and d.Correo LIKE '%$data%')  and d.`Vendedor`=$userid order by d.`DocId` desc ";
                break;
            case 'telefono':
                $query = "SELECT  d.`DocId`, d.`Nombre`, d.`Apellido`, d.`Cedula`, d.`Especialidad`, d.`CreatedDate`, d.`Correo`, d.`Telefono`, d.`Domicilio`, d.`Tipo_descuento`, d.`Ingreso`, CONCAT(u.Nombre ,' ',u.Apellido) as 'Vendedor', d.`estado` FROM Doctores d inner join Users u on u.userId = d.Vendedor where (d.estado ='activo' and d.Telefono LIKE '%$data%') and d.`Vendedor`=$userid order by d.`DocId` desc ";
                break;
            default:
                $query = "SELECT  d.`DocId`, d.`Nombre`, d.`Apellido`, d.`Cedula`, d.`Especialidad`, d.`CreatedDate`, d.`Correo`, d.`Telefono`, d.`Domicilio`, d.`Tipo_descuento`, d.`Ingreso`, CONCAT(u.Nombre ,' ',u.Apellido) as 'Vendedor', d.`estado` FROM Doctores d inner join Users u on u.userId = d.Vendedor where ((d.estado ='activo' and d.Nombre LIKE '%$data%') OR (d.Apellido LIKE '%$data%'and d.estado ='activo' )) and d.`Vendedor`=$userid order by d.`DocId` desc ";
                break;
        }
    }else{
        if($filtro == ""){
            switch ($token) {
                case 'nombre':
                    $query = "SELECT  d.`DocId`, d.`Nombre`, d.`Apellido`, d.`Cedula`, d.`Especialidad`, d.`CreatedDate`, d.`Correo`, d.`Telefono`, d.`Domicilio`, d.`Tipo_descuento`, d.`Ingreso`, CONCAT(u.Nombre ,' ',u.Apellido) as 'Vendedor', d.`estado` FROM Doctores d inner join Users u on u.userId = d.Vendedor where ((d.estado ='activo' and d.Nombre LIKE '%$data%') OR (d.Apellido LIKE '%$data%'and d.estado ='activo' )) order by d.`DocId` desc ";
                    break;
                case 'email':
                    $query = "SELECT  d.`DocId`, d.`Nombre`, d.`Apellido`, d.`Cedula`, d.`Especialidad`, d.`CreatedDate`, d.`Correo`, d.`Telefono`, d.`Domicilio`, d.`Tipo_descuento`, d.`Ingreso`, CONCAT(u.Nombre ,' ',u.Apellido) as 'Vendedor', d.`estado` FROM Doctores d inner join Users u on u.userId = d.Vendedor where (d.estado ='activo' and d.Correo LIKE '%$data%')   order by d.`DocId` desc ";
                    break;
                case 'telefono':
                    $query = "SELECT  d.`DocId`, d.`Nombre`, d.`Apellido`, d.`Cedula`, d.`Especialidad`, d.`CreatedDate`, d.`Correo`, d.`Telefono`, d.`Domicilio`, d.`Tipo_descuento`, d.`Ingreso`, CONCAT(u.Nombre ,' ',u.Apellido) as 'Vendedor', d.`estado` FROM Doctores d inner join Users u on u.userId = d.Vendedor where (d.estado ='activo' and d.Telefono LIKE '%$data%') order by d.`DocId` desc ";
                    break;
                default:
                    $query = "SELECT  d.`DocId`, d.`Nombre`, d.`Apellido`, d.`Cedula`, d.`Especialidad`, d.`CreatedDate`, d.`Correo`, d.`Telefono`, d.`Domicilio`, d.`Tipo_descuento`, d.`Ingreso`, CONCAT(u.Nombre ,' ',u.Apellido) as 'Vendedor', d.`estado` FROM Doctores d inner join Users u on u.userId = d.Vendedor where ((d.estado ='activo' and d.Nombre LIKE '%$data%') OR (d.Apellido LIKE '%$data%'and d.estado ='activo' )) order by d.`DocId` desc ";
                    break;
            }
        }else{
            switch ($token) {
                case 'nombre':
                    $query = "SELECT  d.`DocId`, d.`Nombre`, d.`Apellido`, d.`Cedula`, d.`Especialidad`, d.`CreatedDate`, d.`Correo`, d.`Telefono`, d.`Domicilio`, d.`Tipo_descuento`, d.`Ingreso`, CONCAT(u.Nombre ,' ',u.Apellido) as 'Vendedor', d.`estado` FROM Doctores d inner join Users u on u.userId = d.Vendedor where ((d.estado ='activo' and d.Nombre LIKE '%$data%') OR (d.Apellido LIKE '%$data%'and d.estado ='activo' )) and d.`Vendedor`=$filtro  order by d.`DocId` desc ";
                    break;
                case 'email':
                    $query = "SELECT  d.`DocId`, d.`Nombre`, d.`Apellido`, d.`Cedula`, d.`Especialidad`, d.`CreatedDate`, d.`Correo`, d.`Telefono`, d.`Domicilio`, d.`Tipo_descuento`, d.`Ingreso`, CONCAT(u.Nombre ,' ',u.Apellido) as 'Vendedor', d.`estado` FROM Doctores d inner join Users u on u.userId = d.Vendedor where (d.estado ='activo' and d.Correo LIKE '%$data%') and d.`Vendedor`=$filtro  order by d.`DocId` desc ";
                    break;
                case 'telefono':
                    $query = "SELECT  d.`DocId`, d.`Nombre`, d.`Apellido`, d.`Cedula`, d.`Especialidad`, d.`CreatedDate`, d.`Correo`, d.`Telefono`, d.`Domicilio`, d.`Tipo_descuento`, d.`Ingreso`, CONCAT(u.Nombre ,' ',u.Apellido) as 'Vendedor', d.`estado` FROM Doctores d inner join Users u on u.userId = d.Vendedor where (d.estado ='activo' and d.Telefono LIKE '%$data%') and d.`Vendedor`=$filtro order by d.`DocId` desc ";
                    break;
                default:
                    $query = "SELECT  d.`DocId`, d.`Nombre`, d.`Apellido`, d.`Cedula`, d.`Especialidad`, d.`CreatedDate`, d.`Correo`, d.`Telefono`, d.`Domicilio`, d.`Tipo_descuento`, d.`Ingreso`, CONCAT(u.Nombre ,' ',u.Apellido) as 'Vendedor', d.`estado` FROM Doctores d inner join Users u on u.userId = d.Vendedor where ((d.estado ='activo' and d.Nombre LIKE '%$data%') OR (d.Apellido LIKE '%$data%'and d.estado ='activo' )) and d.`Vendedor`=$userid order by d.`DocId` desc ";
                    break;
            }
        }
    }
    
  $datos = $con->query($query)->fetchAll(PDO::FETCH_ASSOC );

  foreach($datos as $row){
    $final_data[] = $row;
  }
    echo json_encode($final_data,JSON_UNESCAPED_UNICODE);
} catch (PDOException  $e) {
    $result = ["mensaje" => "Error: ".$e];
}

?>