<?php 
header('Content-type: application/json');
include_once('../../../../assets/db/conexion.php');

try {
  $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $final_data=[];

  $query = "SELECT d.`DocId`, d.`Nombre`, d.`Apellido`, d.`Cedula`, d.`Especialidad`, d.`CreatedDate`, d.`Correo`, d.`Telefono`, d.`Domicilio`, d.`Tipo_descuento`, d.`Ingreso`, CONCAT(u.Nombre ,' ',u.Apellido) as 'Vendedor', d.`estado` FROM Doctores d inner join Users u on u.userId = d.Vendedor where estado ='activo'";
  $datos = $con->query($query)->fetchAll(PDO::FETCH_ASSOC );

  foreach($datos as $row){
    $final_data[] = $row;
  }
    echo json_encode($final_data);
} catch(PDOException $e) {
    echo $result = ["mensaje" => "Error: ".$e->getMessage()];
}
?>