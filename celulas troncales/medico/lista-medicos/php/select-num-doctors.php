<?php 
session_start();
header('Content-type: application/json');
include_once('../../../../assets/db/conexion.php');

$final_data = Array();
try {
    //construccion
    $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $con->exec("SET NAMES 'utf8'");

    $query = "select count(*) num from Doctores d where d.estado = 'activo' and d.Vendedor <>  '' ";
    
  $datos = $con->query($query)->fetchAll(PDO::FETCH_ASSOC );

  foreach($datos as $row){
    $final_data[] = $row;
  }
    echo json_encode($final_data,JSON_UNESCAPED_UNICODE);
} catch (PDOException  $e) {
    $result = ["mensaje" => "Error: ".$e];
}

?>