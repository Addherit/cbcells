document.addEventListener('DOMContentLoaded', () => { setMedicosListInTable(); setVendedoresFiltro(); getTotalMedicos(); })
document.querySelector('#form-search').addEventListener('submit', (event) => { event.preventDefault(); setMedicosListInTable() });
document.querySelector('#btn-new-medico').addEventListener('click', () => { goToNewMedico() });
document.querySelector('[name=id-vendedor-filtro]').addEventListener('change', () => { setMedicosListInTable() })

async function setMedicosListInTable() {

    const MEDICOS = await searchMedico();
    if(MEDICOS.length > 0) {
        const FILAS = createRowsTable(MEDICOS);     
        document.querySelector('#tbl-lista-medicos tbody').innerHTML = FILAS;
        // utilizas la clase Alertas para remarcar los nuevos medicos en la lista mostrada en caso de que exista la var ids en la url
        alerta.highlightIds('tbl-lista-medicos');
    } else {
        const MSN = new Message('warning', 'No hay medicos con ese nombre');
        MSN.showMessage();
    }

 

}

function createRowsTable(medicos) {

    let filas = '';
    medicos.forEach(medico => {
        filas += `
            <tr>
                <td><a href="#" class="text-danger btn-tbl-accion" onclick="deleteMedico(${medico.DocId})"><span class="material-icons">delete</span>Eliminar</a></td>
                <td><a href="#" class="text-info btn-tbl-accion" onclick="editMedico(${medico.DocId})"><span class="material-icons">create</span>Editar</a></td>
                <td class="text-primary">${medico.DocId}</td>
                <td>${medico.Nombre} ${medico.Apellido}</td>
                <td>${medico.Correo}</td>
                <td>${medico.Telefono}</td>
                <td>${medico.Especialidad}</td>
                <td>${medico.Vendedor}</td>                
                <td class="text-success">${medico.estado}</td>
            </tr>
        `
    });
    return filas;

}

function editMedico(docId) {
    window.location = `/crm/celulas troncales/medico/editar-medico/index.php?DocId=${docId}`;
}

function deleteMedico(docId) {

    var r = confirm("Seguro que desea eliminarlo?");
    if (r == true) {
        const FORM_DATA = new FormData();
        FORM_DATA.append('docId', docId);
        fetch('php/delete-doctor.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
            const MSN = new Message(data.type, data.msn);
            MSN.showMessage();        
            setMedicosListInTable();
        });
    }

}

function searchMedico() {

     const FORM_DATA = new FormData(document.querySelector('#form-search'))
     FORM_DATA.append('filtro', document.querySelector('[name=id-vendedor-filtro]').value)
     FORM_DATA.append('token', document.querySelector('[name=token]').value)
     return fetch('php/select-doctor.php', { method: 'POST', body: FORM_DATA })
     .then(data => data.json())
     .then(data => {
         return data
     })

}

function goToNewMedico() {
    window.location = `/crm/celulas troncales/medico/nuevo-medico/index.php`;
}

function setVendedoresFiltro() {
    
    fetch('php/select-vendedores.php')
    .then(data => data.json())
    .then(data => {
        let option = `<option value="" selected>Selecciona un vendedor</option>`
        data.forEach(vendedor => {
            option += `
                <option value="${vendedor.userId}">${vendedor.Nombre} ${vendedor.Apellido}</option>
            `
        });
        document.querySelector('[name=id-vendedor-filtro]').innerHTML = option;
    })

}

function getTotalMedicos() {
    
    fetch('php/select-num-doctors.php')
    .then(data => data.json())
    .then(data => {
        document.querySelector('#cant-medicos').innerHTML = data[0].num;
    })
}

