<!DOCTYPE html>
<html lang="en">

<head>
  <?php include '../../../header.html' ?>
</head>

<body class="">
  <div class="wrapper ">
    <?php include '../../../sidebar/sidebar.html' ?>
    <div class="main-panel">
      <?php include '../../../sidebar/navbar.html' ?>
      <div class="content">
        <div class="container-fluid">  
            <div class="row" style="width: 100%">
                <div class="col-md-4">
                    <div class="form-group select-custom">
                        <label class="bmd-label-floating">Vendedor</label>                          
                        <select class="custom-select" name="id-vendedor-filtro" style="background-color: transparent"></select>
                    </div>                        
                </div>
                <div class="col-md-4">
                    <div class="form-group select-custom">
                        <label class="bmd-label-floating">Tipo filtro</label>                          
                        <select class="custom-select" name="token" style="background-color: transparent">  
                            <option value=""selected>Sin filtro</option>
                            <option value="nombre">Nombre</option>
                            <option value="email">E-mail</option>
                            <option value="telefono">Teléfono</option>
                        </select>
                    </div>                        
                </div>
                <div class="col-md-4">
                    <form class="navbar-form" id="form-search">
                        <div class="input-group no-border">
                            <input type="text" name="search-field" class="form-control" placeholder="Buscar médico">
                            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                            <i class="material-icons">search</i>
                            <div class="ripple-container"></div>
                            </button>
                        </div>
                    </form>
                </div>
            </div>       
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="row">
                                <div class="col-8">
                                    <h4 class="card-title ">Lista de Médicos</h4>
                                    <p class="card-category">Para poder editar o eliminar un médico, utiliza los botones de acción</p>
                                </div>
                                <div class="col-4">
                                    <h4>Medicos: <span id="cant-medicos"></span></h4>
                                </div>
                            </div>
                            
                        </div>
                        <div class="card-body">
                            <div class="table-responsive" style="min-height: 100%">
                                <table class="table table-hover" id="tbl-lista-medicos">
                                    <thead class=" text-primary">
                                        <th colspan="2"></th>
                                        <th>ID</th>
                                        <th>Nombre completo</th>
                                        <th>Email</th>
                                        <th>Teléfono</th>
                                        <th>Especialidad</th>
                                        <th>Vendedor Asignado</th>
                                        <th>Estatus</th>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button class="btn btn-primary float-right" id="btn-new-medico"><span class="material-icons">add</span>Nuevo médico</button>                                                                   
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>           
            </div>
            </div>
        </div>
      </div> 
</body>
<?php include '../../../footer.html' ?>
<script src="js/lista-medicos.js"></script>


</html>