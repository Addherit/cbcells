<?php 
session_start();
header('Content-type: application/json');
include_once('../../../../assets/db/conexion.php');

if($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        //vars from form
        $nombre= $_POST['nombres'];
        $apellido= $_POST['apellidos'];
        $cedula= $_POST['cedula-profesional'];
        $espe= $_POST['especialidad'];
        $email= $_POST['email'];
        $domicilio= $_POST['domicilio'];
        $tel= $_POST['telefono'];
        $descuento= $_POST['descuento'];
        $ingreso= $_POST['ingreso'];
        $otros= $_POST['otros'];
        $vendedor= $_POST['vendedor'];
        $sup= $_POST['sup-especialidad'];
        $domi_consul= $_POST['domicilio-consultorio'];
        // campos fiscales
        $nam_fis= $_POST['nombre-fiscal'];
        $rfc= $_POST['rfc'];
        $reg_fis= $_POST['regimen-fiscal'];
        $dir_fis= $_POST['direccion-fiscal'];
        $email_fac = $_POST['email-factura'];
        $email_fac2 = $_POST['email-factura2'];


        // sql query for INSERT INTO Medicos
        $sql = "INSERT INTO Doctores (`Nombre`, `Apellido`, `Cedula`, `Especialidad`, `Correo`,`Telefono`, `Domicilio`, `Tipo_descuento`, `Ingreso`, `Vendedor`,`Sup_Especialidad`,`Domicilio_consultorio`,`Otros`) VALUES ('$nombre','$apellido','$cedula','$espe','$email','$tel','$domicilio','$descuento','$ingreso','$vendedor','$sup','$domi_consul','$otros')"; 
        //$email;
        //$tel;
        $tok ="none";
        if(!empty($email)){
            if(!empty($tel)){
            $sqlcheck = "select count(*) as 'Cuenta' from Doctores where Correo ='$email' or Telefono ='$tel'"; 

            $datos = $con->query($sqlcheck)->fetchAll(PDO::FETCH_ASSOC );
            foreach($datos as $row){
                $final_data[] = $row;
            }
            $cuenta = $final_data[0]['Cuenta'];

            }
        }
        if(empty($nombre)){
            $cuenta ++;
            $tok = 'name';
        }
        if(empty($apellido)){
            $cuenta ++;
            $tok = 'ap';
        }
        if(empty($email)){
            $cuenta ++;
            $tok = 'email';
        }
        if(empty($tel)){
            $cuenta ++;
            $tok = 'tel';
        }
        if($cuenta != 0){
            if($tok != 'none'){
                $result = ['type' => "danger", 'msn' => "El Telefono, Email o el Nombre Esta Ausente",'id'=>$id];
            }else{
                $result = ['type' => "danger", 'msn' => "El Telefono o el Email ya estan registrados",'id'=>$id];
            }
        }else{
            //$checksql="select * from Doctores where Correo ='$email' or Telefono = '$tel'";
            // Performs the $sql query on the server to insert the values
            if ($conn->query($sql) === TRUE) {
                $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $datos = $con->query("SELECT MAX(DocId) as Id FROM Doctores")->fetchAll(PDO::FETCH_ASSOC );
                foreach($datos as $row){
                    $final_data[] = $row;
                }
                $id = $final_data[0]['Id'];
                $sql2 = "insert into DatosFiscales(`fiscal_name`, `rfc`, `regimen_fiscal`, `direccion_completa`, `docid`,`Email_factura`,`SegundoMail`) values('$nam_fis','$rfc','$reg_fis','$dir_fis','$id','$email_fac','$email_fac2')";
                $conn->query($sql2);

                $result = ['type' => "success", 'msn' => "Médico creado correctamente",'id'=>$id];

                $aux =$_SESSION["res"];
                $aux1 = $aux['userId'];
                //HISTORIAL
                $query = "INSERT INTO `Historial`(`Mensaje`, `Usuario`) VALUES ('Medico creado correctamente','$aux1')";
                $conn->query($query);
                //notificacion
                $queryn="INSERT INTO `Notificaciones`( `UserId`, `Mensaje`, `IdAccion`, `Estado`, `Ntype`, `Type`, `EstadoSupervisor`, `Mostrar`) VALUES ('$aux1','Registro de Nuevo medico','$id','visto','1','Registro de Nuevo medico','visto','A')";
                $conn->query($queryn);
            }
            else {
                $result = ['type' => "danger", 'msn' => "Problema del query"];
            }

            $conn->close();
        }
    } catch (PDOException  $e) {
        $result = ['type' => "danger", 'msn' => "Problema de la conexión"];
    }
    $result["query"] = $sql;
    $result["cuenta"] = $cuenta;

    echo json_encode($result);
}
?>