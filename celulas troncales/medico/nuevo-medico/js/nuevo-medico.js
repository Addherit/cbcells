document.addEventListener('DOMContentLoaded', () => {
    
    getDateNow();
    validateCollapseDatosFiscales();
    setVendedoresOptions();

})

document.querySelector('#form-add-medico').addEventListener('submit', (event) => {
    event.preventDefault();
    insertMedico();
})

document.querySelector('#check-datos-fiscales').addEventListener('change', () => { validateCollapseDatosFiscales() })
document.querySelector('#btn-lista-medicos').addEventListener('click', () => { goToListMedicos() })

function getDateNow() {

    setTimeout(() => {
        document.querySelector('[name=fecha]').parentElement.classList.add('is-filled');
    }, 400);
    setInterval(() => { 
        document.querySelector('[name=fecha]').value = moment().format('DD-MM-YYYY h:mm:ss a');
    }, 1000);

}

function insertMedico() {
    

    if (document.querySelector('[name=vendedor]').value == '') {
        const MSN = new Message('warning', 'No puedes registrar un médico sin vendedor');
        MSN.showMessage();
    } else {            
        const FORM_DATA = new FormData(document.querySelector('#form-add-medico'))
        fetch('php/insert-new-doctor.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
            const MSN = new Message(data.type, data.msn);
            MSN.showMessage();
            if(data.type == 'success') { 
                document.querySelector("#form-add-medico").reset()
                const fd = new FormData()
                fd.append('id', data.id)            
                fetch('php/enviar-correo.php', { method: 'POST', body: fd });
            }
        })
    }
    
}

function validateCollapseDatosFiscales() {

    const DIV_DATOS_FISCALES = document.querySelector('#div-datos-fiscales');
    const CHECK_DATOS_FISCALES = document.querySelector('#check-datos-fiscales');
    CHECK_DATOS_FISCALES.checked  == true ? DIV_DATOS_FISCALES.classList.add('show') : DIV_DATOS_FISCALES.classList.remove('show');

}

function getVendedoresOptios() {
    
    return fetch('php/select-vendedores.php')
    .then(data => data.json())
    .then(data => {
        return data;
    })
}

async function setVendedoresOptions() {

    const DATA_VENDEDORES = await getVendedoresOptios();
    const OPTIONS = createVendedoresOptions(DATA_VENDEDORES);
    document.querySelector('[name=vendedor]').innerHTML = OPTIONS;
}

function createVendedoresOptions(vendedores) {

    let option = '';
    switch (localStorage.getItem('user_type')) {
        case '4':
            vendedores.forEach(vendedor => {
                if (vendedor.userId == 20)
                    option += `<option value="${vendedor.userId}" selected> ${vendedor.Nombre} ${vendedor.Apellido}</option>`;
            });
            break;
        case '2':
            vendedores.forEach(vendedor => {
                if (vendedor.userId == localStorage.getItem('user_id'))
                    option += `<option value="${vendedor.userId}" selected> ${vendedor.Nombre} ${vendedor.Apellido}</option>`;
            });
            break;
    
        default:
            option += '<option selected value="">Selecciona un vendedor</option>';
            vendedores.forEach(vendedor => {            
                option += `<option value="${vendedor.userId}"> ${vendedor.Nombre} ${vendedor.Apellido}</option>`        
            });
            break;
    }
    return option;
    
}

function goToListMedicos() {
    window.location = `/crm/celulas troncales/medico/lista-medicos/index.php`;
}