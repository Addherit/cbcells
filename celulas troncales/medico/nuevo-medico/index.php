<!DOCTYPE html>
<html lang="en">

<head>
  <?php include '../../../header.html' ?>
</head>

<body class="">
  <div class="wrapper ">
    <?php include '../../../sidebar/sidebar.html' ?>
    <div class="main-panel">
      <?php include '../../../sidebar/navbar.html' ?>
      <div class="content">
        <div class="container-fluid">
          <div class="row">
          <div class="col-md-10 offset-md-1">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Datos del médico</h4>
                  <p class="card-category">Ingresa la información del médico</p>
                </div>
                <div class="card-body">
                  <form id="form-add-medico">
                    <div class="row">
                      <div class="col-md-5 offset-md-7">
                        <div class="form-group">
                          <label class="bmd-label-floating">Fecha</label>
                          <input type="text" class="form-control" name="fecha" readonly style="background-color: white">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Nombres</label>
                          <input type="text" class="form-control" name="nombres">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Apellidos</label>
                          <input type="text" class="form-control" name="apellidos">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Cédula Profesional</label>
                          <input type="text" class="form-control" name="cedula-profesional">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Especialidad</label>
                          <input type="text" class="form-control" name="especialidad">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Sup/Especialidad</label>
                          <input type="text" class="form-control" name="sup-especialidad">
                        </div>
                      </div>                      
                    </div>
                    <div class="row">
                      <div class="col-md-8">
                        <div class="form-group">
                          <label class="bmd-label-floating">Domicilio / Calle y Número/ Ciudad/ Estado </label>
                          <input type="text" class="form-control" name="domicilio">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Teléfono</label>
                          <input type="text" class="form-control" name="telefono">
                        </div>
                      </div>
                    </div>                  
                    <div class="row">
                      <div class="col-md-8">
                        <div class="form-group">
                          <label class="bmd-label-floating">Domicilio del consultorio </label>
                          <input type="text" class="form-control" name="domicilio-consultorio">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Correo electrónico</label>
                          <input type="email" class="form-control" name="email">
                        </div>
                      </div>
                    </div>                    
                    <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Tipo de descuento</label>
                          <input type="text" class="form-control" name="descuento">
                        </div>
                      </div>                      
                      <div class="col-md-7">
                        <div class="form-group select-custom">
                          <label class="bmd-label-floating">Vendedor asignado</label>                          
                          <select class="custom-select" name="vendedor">                     
                          </select>
                        </div>                        
                      </div>
                    </div>  
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group select-custom">
                          <label class="bmd-label-floating">Fuente de ingreso</label>                  
                          <select class="custom-select" name ="ingreso">
                            <option selected disabled>Selecciona fuente</option>
                            <option value="Web formato cbcells">Web formato cbcells</option>
                            <option value="Red social">Red social</option>
                            <option value="Refererido por medico">Refererido por medico</option>
                            <option value="Congreso">Congreso</option>
                            <option value="Webinar">Webinar</option>
                            <option value="Autogestión">Autogestión</option>
                            <option value="Call Center Cbcells">Call Center Cbcells</option>
                            <option value="Email">Email Cbcells</option>
                            <option value="Otros">Otros</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-8">
                        <div class="form-group">
                          <label class="bmd-label-floating">Otros</label>
                          <input type="text" class="form-control" name="otros">
                        </div>   
                      </div>
                    </div>     
                    <br>   
                    <div class="row">
                      <div class="col-md-12">
                        <div class="custom-switch">
                          <input type="checkbox" class="custom-control-input" id="check-datos-fiscales">
                          <label class="custom-control-label" for="check-datos-fiscales">Datos fiscales</label>
                        </div>
                      </div>
                    </div> 
                    <div id="div-datos-fiscales" class="collapse">
                      <div class="row">
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="bmd-label-floating">Nombre fiscal</label>
                            <input type="text" class="form-control" name="nombre-fiscal">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="bmd-label-floating">RFC</label>
                            <input type="text" class="form-control" name="rfc">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="bmd-label-floating">Régimen fiscal</label>
                            <input type="text" class="form-control" name="regimen-fiscal">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="bmd-label-floating">Correo electrónico 1</label>
                            <input type="email" class="form-control" name="email-factura">
                          </div>
                        </div>
                      </div>  
                      <div class="row">
                        <div class="col-md-8">
                          <div class="form-group">
                            <label class="bmd-label-floating">Dirección completa</label>
                            <input type="text" class="form-control" name="direccion-fiscal">
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label class="bmd-label-floating">Correo electrónico 2</label>
                            <input type="email" class="form-control" name="email-factura2">
                          </div>
                        </div>
                      </div> 
                    </div>
                    <div class="row">
                      <div class="col-12">
                        <button type="submit" class="btn btn-primary pull-right" id="btn-save"><span class="material-icons">save_alt</span> Guardar datos</button>
                        <button type="button" class="btn btn-primary float-left" id="btn-lista-medicos"><span class="material-icons">arrow_back_ios</span>Lista médicos</button>
                      </div>
                    </div>    
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
</body>
<?php include '../../../footer.html' ?>
<script src="js/nuevo-medico.js"></script>

</html>