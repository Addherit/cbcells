<?php 
session_start();
header('Content-type: application/json');
include_once('../../../../assets/db/conexion.php');

if($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        //vars from form
        $id= $_POST['docId'];
        $nombre= $_POST['nombres'];
        $apellido= $_POST['apellidos'];
        $cedula= $_POST['cedula-profesional'];
        $especialidad= $_POST['especialidad'];
        $correo= $_POST['email'];
        $telefono= $_POST['telefono'];
        $domicilio= $_POST['domicilio'];
        $tipo_descuento= $_POST['descuento'];
        $sup= $_POST['sup-especialidad'];
        $ingreso= $_POST['ingreso'];
        $otros= $_POST['otros'];
        $vendedor= $_POST['vendedor'];
        $domiconsul= $_POST['domicilio-consultorio'];
        // campos fiscales
        $nam_fis= $_POST['nombre-fiscal'];
        $rfc= $_POST['rfc'];
        $reg_fis= $_POST['regimen-fiscal'];
        $dir_fis= $_POST['direccion-fiscal'];
        $email_fac = $_POST['email-factura'];
        $email_fac2 = $_POST['email-factura2'];


        // sql query for INSERT INTO paciente
        $sql="UPDATE `Doctores` SET `Nombre`='$nombre',`Apellido`='$apellido',`Cedula`='$cedula',`Especialidad`='$especialidad',`Correo`='$correo',`Telefono`='$telefono',`Domicilio`='$domicilio',`Tipo_descuento`='$tipo_descuento',`Ingreso`='$ingreso',`Vendedor`='$vendedor',`Sup_Especialidad`='$sup',`Domicilio_consultorio`='$domiconsul',`Otros`='$otros' WHERE DocId = $id";
        // Performs the $sql query on the server to insert the values
        if ($conn->query($sql) === TRUE) {

            $sql2="UPDATE `DatosFiscales` SET `Fiscal_name`='$nam_fis',`Rfc`='$rfc',`Regimen_fiscal`='$reg_fis',`Direccion_completa`='$dir_fis',`Email_factura`='$email_fac',`SegundoMail`='$email_fac2' WHERE DocId =$id";
            $conn->query($sql2);

            $result = ['type' => "success", 'msn' => "Medico editado correctamente",'email-fac'=>$email_fac];

            $aux =$_SESSION["res"];
            $aux1 = $aux['userId'];

            $query = "INSERT INTO `Historial`(`Mensaje`, `Usuario`) VALUES ('Medico editado correctamente','$aux1')";
            $conn->query($query);
            //notificacion
            $queryn="INSERT INTO `Notificaciones`( `UserId`, `Mensaje`, `IdAccion`, `Ntype`, `Type`, `EstadoAdmin`, `EstadoSupervisor`, `Mostrar`) VALUES ('$vendedor','Reasignación de Medico','$id','3','Reasignación de Medico','visto','visto','A')";
            $conn->query($queryn);
        }
        else {
            $result = ['type' => "danger", 'msn' => "Problema del query"];
        }

        $conn->close();
    } catch (PDOException  $e) {
        $result = ['type' => "danger", 'msn' => "Problema de la conexión"];
    }
    $result["query"] = $sql;

    echo json_encode($result);
}
?>