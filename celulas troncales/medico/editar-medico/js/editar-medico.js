document.addEventListener('DOMContentLoaded', () => {  
    document.querySelector('#check-datos-fiscales').checked = true;
    validateCollapseDatosFiscales();
    setVendedoresOptions();
    setTimeout(() => { searchMedico() }, 200);
     
})

document.querySelector('#form-edit-medico').addEventListener('submit', (event) => { event.preventDefault(); editMedico() })
document.querySelector('#check-datos-fiscales').addEventListener('change', () => { validateCollapseDatosFiscales() })
document.querySelector('#return-to').addEventListener('click', () => { returnToListMedics() })
document.querySelector('#btn-pdf').addEventListener('click', () => { getPDF() })

function editMedico() {
    
    const PARAMS_URL = new URLSearchParams(window.location.search);
    const FORM_DATA = new FormData(document.querySelector('#form-edit-medico'))
    FORM_DATA.append('docId', PARAMS_URL.get('DocId'));
    fetch('php/update-doctor.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();        
    })
    
}
 
function validateCollapseDatosFiscales() {

    const DIV_DATOS_FISCALES = document.querySelector('#div-datos-fiscales');
    const CHECK_DATOS_FISCALES = document.querySelector('#check-datos-fiscales');
    CHECK_DATOS_FISCALES.checked  == true ? DIV_DATOS_FISCALES.classList.add('show') : DIV_DATOS_FISCALES.classList.remove('show');

} 

function searchMedico() {

    const PARAMS_URL = new URLSearchParams(window.location.search);    
    const FORM_DATA = new FormData();
    FORM_DATA.append('docId', PARAMS_URL.get('DocId')  )
    fetch('php/select-doctor.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        putDataInInputs(data[0]);
    })

}

function putDataInInputs(data) {
    
    addClassIsFilled();    

    document.querySelector('[name=fecha]').value = data.CreatedDate
    document.querySelector('[name=nombres]').value = data.Nombre
    document.querySelector('[name=apellidos]').value = data.Apellido
    document.querySelector('[name=cedula-profesional]').value = data.Cedula
    document.querySelector('[name=especialidad]').value = data.Especialidad
    document.querySelector('[name=sup-especialidad]').value = data.Sup_Especialidad
    document.querySelector('[name=email]').value = data.Correo
    document.querySelector('[name=domicilio]').value = data.Domicilio
    document.querySelector('[name=telefono]').value = data.Telefono
    document.querySelector('[name=domicilio-consultorio]').value = data.Domicilio_consultorio
    document.querySelector('[name=descuento]').value = data.Tipo_descuento    
    document.querySelector('[name=vendedor]').value = data.Vendedor
    document.querySelector('[name=ingreso]').value = data.Ingreso
    document.querySelector('[name=otros]').value = data.Otros

    document.querySelector('[name=nombre-fiscal]').value = data.Fiscal_name
    document.querySelector('[name=rfc]').value = data.Rfc
    document.querySelector('[name=regimen-fiscal]').value = data.Regimen_fiscal
    document.querySelector('[name=direccion-fiscal]').value = data.Direccion_completa
    document.querySelector('[name=email-factura]').value = data.Email_factura
    document.querySelector('[name=email-factura2]').value = data.SegundoMail

    applyPermitionsUser();
    

}

function addClassIsFilled() {

    //funcion para hacer el efecto de focus en input de datos
    const DIVS = document.querySelectorAll('.bmd-form-group');
    DIVS.forEach(element => {
        element.classList.add('is-filled');        
    });

}

function getVendedoresOptios() {
    
    return fetch('php/select-vendedores.php')
    .then(data => data.json())
    .then(data => {
        return data;
    })
}

async function setVendedoresOptions() {

    const DATA_VENDEDORES = await getVendedoresOptios();
    const OPTIONS = createVendedoresOptions(DATA_VENDEDORES);
    document.querySelector('[name=vendedor]').innerHTML = OPTIONS;
}

function createVendedoresOptions(vendedores) {
 
    let option = '<option selected disabled>Selecciona un vendedor</option>';
    vendedores.forEach(vendedor => {
        option += `<option value="${vendedor.userId}"> ${vendedor.Nombre} ${vendedor.Apellido}</option>`        
    });
    return option;
    
}

function returnToListMedics() {
    window.location = '/crm/celulas troncales/medico/lista-medicos/index.php';
}

function getPDF() {

    const FORM_DATA = new FormData(document.querySelector('#form-edit-medico'))
    FORM_DATA.append('title', 'DATOS DEL MÉDICO');
    FORM_DATA.append('plantilla_type', 'medico');
    FORM_DATA.append('vendedorAsig', document.querySelector('[name=vendedor]').selectedOptions[0].innerHTML )
    fetch('/crm/assets/js/plugins/mpdf/print.php', { method: 'POST', body: FORM_DATA })
    .then( data => {
        if(data.status == 200) {
            window.open('/crm/assets/js/plugins/mpdf/formato.pdf','_blanck'); 
            // class DeleteFile needs URL parameter
            const FILE = new DeleteFile('../js/plugins/mpdf/formato.pdf');
            FILE.deteleFile();
        }
    })
    
}

function applyPermitionsUser() {
    
    if (localStorage.getItem('user_type') == '2' || localStorage.getItem('user_type') == '4' ) {
        document.querySelector('[name=vendedor').disabled = true
    }

}
