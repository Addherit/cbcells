<!DOCTYPE html>
<html lang="en">

<head>
  <?php include '../../../header.html' ?>
</head>

<body class="">
  <div class="wrapper ">
    <?php include '../../../sidebar/sidebar.html' ?>
    <div class="main-panel">
      <?php include '../../../sidebar/navbar.html' ?>
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-10 offset-md-1">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Solicitud de células</h4>
                  <p class="card-category">Ingresa la información del paciente</p>
                </div>
                <div class="card-body">
                  <form id="form-add-paciente">

                    <div class="row">
                      <div class="col-md-5 offset-md-7">
                        <div class="form-group">
                          <label class="bmd-label-floating">Fecha</label>
                          <input type="text" class="form-control" name="fecha" readonly style="background-color: white">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <h3>Datos del médico</h3>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-10">
                        <div class="form-group select-custom">
                          <label class="bmd-label-floating">Nombre del médico</label>                          
                          <select class="custom-select" name="medico"></select>
                        </div>                      
                      </div>                     
                      <div class="col-md-2">
                        <div class="form-group select-custom">
                          <label class="bmd-label-floating">Cedula profesional</label>                          
                          <input type="text" class="form-control" name="cedula-profesional">
                        </div>                      
                      </div>                     
                    </div>  
                    <div class="row">
                      <div class="col-md-12">
                        <h3>Datos del paciente</h3>
                      </div>
                    </div>                                                          
                    <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Nombres</label>
                          <input type="text" class="form-control" name="nombres">
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Apellidos</label>
                          <input type="text" class="form-control" name="apellidos">
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label class="bmd-label-floating">Edad</label>
                          <input type="text" class="form-control" name="edad">
                        </div>
                      </div>  
                      <div class="col-md-9">
                        <div class="form-group">
                          <label class="bmd-label-floating">Diagnostico</label>
                          <input type="text" class="form-control" name="diagnostico">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group select-custom">
                          <label class="bmd-label-floating">Sexo</label>                          
                          <select class="custom-select" name="sexo">
                            <option value="" selected disabled></option>
                            <option value="Hombre">Hombre</option>
                            <option value="Mujer">Mujer</option>
                          </select>
                        </div>                      
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <h3>Estudio de la muestra</h3>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Fecha de obtención</label>
                          <input type="date" class="form-control" name="fecha-obtencion">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">                          
                          <label class="bmd-label-floating">Comentarios</label>
                          <textarea class="form-control" rows="3" name="comentarios"></textarea>
                        </div>
                      </div>             
                    </div>
                    <br> 
                    <div class="row">
                      <div class="col-12">
                        <button type="submit" class="btn btn-primary pull-right" id="btn-save"><span class="material-icons">save_alt</span> Guardar datos</button>
                        <button type="button" class="btn btn-primary float-left" id="btn-lista-pacientes"><span class="material-icons">arrow_back_ios</span>Lista pacientes</button>
                      </div>
                    </div>                     
                    
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
</body>
<?php include '../../../footer.html' ?>
<script src="js/nuevo-paciente.js"></script>

</html>