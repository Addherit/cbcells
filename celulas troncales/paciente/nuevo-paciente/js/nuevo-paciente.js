document.addEventListener('DOMContentLoaded', () => {
    setMedicos();
    getDateNow();

})

document.querySelector('#form-add-paciente').addEventListener('submit', (event) => {
    event.preventDefault();
    insertPaciente();
})
document.querySelector('#btn-lista-pacientes').addEventListener('click', () => { goToListPacientes() })

function getDateNow() {

    setTimeout(() => {
        document.querySelector('[name=fecha-obtencion]').parentElement.classList.add('is-filled');
        document.querySelector('[name=fecha]').parentElement.classList.add('is-filled');
    }, 400);
    setInterval(() => { 
        document.querySelector('[name=fecha]').value = moment().format('DD-MM-YYYY h:mm:ss a');
    }, 1000);

}

function getMedicos() {

    return fetch('php/select-doctors.php')
    .then(data => data.json())
    .then(data => {
        return data;
    })

}

async function setMedicos() {

    const MEDICOS = await getMedicos();
    let options = '<option>Selecciona uno</option> <option value="na">NA</option>';
    MEDICOS.forEach(medico => {        
        options += `<option value="${medico.DocId}">${medico.Nombre} ${medico.Apellido}</option>`;        
    });
    document.querySelector('[name=medico]').innerHTML = options;
}

function insertPaciente() {
    
    const FORM_DATA = new FormData(document.querySelector('#form-add-paciente'))
    fetch('php/insert-new-paciente.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
        if (data.type == 'success') { document.querySelector('#form-add-paciente').reset() }
    })
    
}

function goToListPacientes() {
    window.location = `/crm/celulas troncales/paciente/lista-pacientes/index.php`;
}