<?php 
session_start();
header('Content-type: application/json');
include_once('../../../../assets/db/conexion.php');

if($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        //vars from form
        $medico= $_POST['medico'];
        $cedula_profesional= $_POST['cedula-profesional'];
        $nombre= $_POST['nombres'];
        $apellido= $_POST['apellidos'];
        $edad= $_POST['edad'];
        $diagnostico= $_POST['diagnostico'];
        $sexo= $_POST['sexo'];
        $fecha_obtencion= $_POST['fecha-obtencion'];
        $comentarios= $_POST['comentarios'];
        
        // sql query for INSERT INTO Paciente
        $sql ="INSERT INTO `Pacientes`( `Nombre`, `Apellido`, `Diagnostico`, `Edad`, `Medico`, `Comentarios`, `Fecha-obtencion`, `Cedula-profesional`, `Sexo`) VALUES ('$nombre','$apellido','$diagnostico','$edad','$medico','$comentarios','$fecha_obtencion','$cedula_profesional','$sexo')";
        

        // Performs the $sql query on the server to insert the values
        if ($conn->query($sql) === TRUE) {

            $result = ['type' => "success", 'msn' => "Paciente creado correctamente"];

            $aux =$_SESSION["res"];
            $aux1 = $aux['userId'];

            $query = "INSERT INTO `Historial`(`Mensaje`, `Usuario`) VALUES ('Paciente creado correctamente','$aux1')";
            $conn->query($query);
        }
        else {
            $result = ['type' => "danger", 'msn' => "Problema del query"];
        }

        $conn->close();
    } catch (PDOException  $e) {
        $result = ['type' => "danger", 'msn' => "Problema de la conexión"];
    }
    $result["query"] = $sql;

    echo json_encode($result);
}
?>