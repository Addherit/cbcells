<?php 
session_start();
header('Content-type: application/json');
include_once('../../../../assets/db/conexion.php');

if($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        //vars from form
        $id= $_POST['pacId'];

        // sql query for INSERT INTO Medicos
        $sql = "update Pacientes set Estado ='desactivo' where PacId =$id";

        // Performs the $sql query on the server to insert the values
        if (mysqli_query($conn, $sql)) {
            $result = ['type' => "success", 'msn' => "Paciente eliminado correctamente"];

            $aux =$_SESSION["res"];
            $aux1 = $aux['userId'];

            $query = "INSERT INTO `Historial`(`Mensaje`, `Usuario`) VALUES ('Paciente eliminado correctamente','$aux1')";
            mysqli_query($conn, $query);
        } else {
            $result = ['type' => "danger", 'msn' => "problema del query"];
        }
        mysqli_close($conn);
    } catch (PDOException  $e) {
        $result = ['type' => "danger", 'msn' => "problema de la conexion"];
    }

    echo json_encode($result);
}
?>