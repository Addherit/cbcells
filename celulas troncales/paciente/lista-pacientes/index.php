<!DOCTYPE html>
<html lang="en">

<head>
  <?php include '../../../header.html' ?>
</head>

<body class="">
  <div class="wrapper ">
    <?php include '../../../sidebar/sidebar.html' ?>
    <div class="main-panel">
      <?php include '../../../sidebar/navbar.html' ?>
      <div class="content">
        <div class="container-fluid">  
            <div class="row" style="width: 100%">
                <div class="col-md-5 offset-md-7">
                    <form class="navbar-form" id="form-search">
                        <div class="input-group no-border">
                            <input type="text" name="search-field" class="form-control" placeholder="Buscar paciente">
                            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                            <i class="material-icons">search</i>
                            <div class="ripple-container"></div>
                            </button>
                        </div>
                    </form>
                </div>
            </div>       
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Lista de Pacientes</h4>
                            <p class="card-category">Para poder editar o eliminar un paciente, utiliza los botones de acción</p>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive" style="min-height: 100%">
                                <table class="table table-hover" id="tbl-lista-pacientes">
                                    <thead class=" text-primary">
                                        <th colspan="2"></th>
                                        <th>ID</th>
                                        <th>Nombre del paciente</th>
                                        <th>Edad</th>
                                        <th>Diagnostico</th>
                                        <th>Sexo</th>
                                        <th>Estatus</th>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button class="btn btn-primary float-right" id="btn-new-paciente"><span class="material-icons">add</span>Nuevo paciente</button>                                                                   
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>           
            </div>
            </div>
        </div>
      </div> 
</body>
<?php include '../../../footer.html' ?>
<script src="js/lista-paciente.js"></script>


</html>