<?php 
session_start();
header('Content-type: application/json');
include_once('../../../../assets/db/conexion.php');

if($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        //vars from form
        $id= $_POST['PacId'];
        $nombre= $_POST['nombres'];
        $apellido= $_POST['apellidos'];
        $diagnostico= $_POST['diagnostico'];
        $edad= $_POST['edad'];
        $medico= $_POST['medico'];
        $sexo= $_POST['sexo'];
        $comentarios= $_POST['comentarios'];
        $fecha_obtencion= $_POST['fecha-obtencion'];
        $cedula_profesional= $_POST['cedula-profesional'];

        
        // sql query for INSERT INTO paciente
        $sql="UPDATE `Pacientes` SET `Nombre`='$nombre',`Apellido`='$apellido',`Diagnostico`='$diagnostico',`Edad`='$edad',`Medico`='$medico',`Comentarios`='$comentarios',`Fecha-obtencion`='$fecha_obtencion',`Cedula-profesional`='$cedula_profesional',`Sexo`='$sexo' WHERE PacId =$id";
        // Performs the $sql query on the server to insert the values
        if ($conn->query($sql) === TRUE) {
            $result = ['type' => "success", 'msn' => "Paciente editado correctamente"];

            $aux =$_SESSION["res"];
            $aux1 = $aux['userId'];

            $query = "INSERT INTO `Historial`(`Mensaje`, `Usuario`) VALUES ('Paciente editado correctamente','$aux1')";
            mysqli_query($conn, $query);
        }
        else {
            $result = ['type' => "danger", 'msn' => "Problema del query"];
        }

        $conn->close();
    } catch (PDOException  $e) {
        $result = ['type' => "danger", 'msn' => "Problema de la conexión"];
    }
   // $result["query"] = $sql;

    echo json_encode($result);
}
?>