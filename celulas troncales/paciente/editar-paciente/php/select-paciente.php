<?php 
header('Content-type: application/json');
include_once('../../../../assets/db/conexion.php');

try {
  $data = $_POST['PacId'];
  $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $con->exec("set names utf8");
  $final_data=[];

  $query = "SELECT `PacId`,`Nombre`,`Apellido`,`CreatedDate`,`Estado`,`Diagnostico`,`Edad`,`Medico`,`Comentarios`,`Fecha-obtencion` as 'FechaObtencion',`Cedula-profesional` as 'CedulaProfesional',`Sexo` FROM Pacientes where PacId =$data and Estado = 'activo'";
  $con->exec("set names utf8");
  $datos = $con->query($query)->fetchAll(PDO::FETCH_ASSOC );

  foreach($datos as $row){
    $final_data[] = $row;
  }
  echo json_encode($final_data);
} catch(PDOException $e) {
    echo $result = ["mensaje" => "Error: ".$e->getMessage()];
}
?>