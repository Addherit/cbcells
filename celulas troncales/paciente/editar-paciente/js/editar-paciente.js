document.addEventListener('DOMContentLoaded', () => {
    setMedicos();
    setTimeout(() => { searchPaciente() }, 500);
})

document.querySelector('#form-edit-paciente').addEventListener('submit', (event) => { event.preventDefault(); editPaciente() })
document.querySelector('#return-to').addEventListener('click', () => { returnToListPacientes() })
document.querySelector('#btn-pdf').addEventListener('click', () => { getPDF() })

function editPaciente() {
    
    const PARAMS_URL = new URLSearchParams(window.location.search);
    const FORM_DATA = new FormData(document.querySelector('#form-edit-paciente'))
    FORM_DATA.append('PacId', PARAMS_URL.get('DocId')  )
    fetch('php/edit-paciente.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
    })

}

function searchPaciente() {

    const PARAMS_URL = new URLSearchParams(window.location.search);    
    const FORM_DATA = new FormData();
    FORM_DATA.append('PacId', PARAMS_URL.get('DocId')  )
    fetch('php/select-paciente.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        putDataInInputs(data[0]);
    })

}

function putDataInInputs(data) {
    
    addClassIsFilled();
    
    document.querySelector('[name=fecha]').value = data.CreatedDate
    document.querySelector('[name=medico]').value = data.Medico
    document.querySelector('[name=cedula-profesional]').value = data.CedulaProfesional

    document.querySelector('[name=nombres]').value = data.Nombre
    document.querySelector('[name=apellidos]').value = data.Apellido
    document.querySelector('[name=edad]').value = data.Edad
    document.querySelector('[name=diagnostico]').value = data.Diagnostico
    document.querySelector('[name=sexo]').value = data.Sexo

    document.querySelector('[name=fecha-obtencion]').value = data.FechaObtencion
    document.querySelector('[name=comentarios]').value = data.Comentarios

}

function addClassIsFilled() {

    //funcion para hacer el efecto de focus en input de datos
    const DIVS = document.querySelectorAll('.bmd-form-group');
    DIVS.forEach(element => {
        element.classList.add('is-filled');        
    });

}

function returnToListPacientes() {
    window.location = '/crm/celulas troncales/paciente/lista-pacientes/index.php';
}

function getPDF() {

    const FORM_DATA = new FormData(document.querySelector('#form-edit-paciente'))
    FORM_DATA.append('title', 'SOLICITUD DE CÉLULAS MESENQUIMALES');
    FORM_DATA.append('plantilla_type', 'paciente-celulas');
    fetch('/crm/assets/js/plugins/mpdf/print.php', { method: 'POST', body: FORM_DATA })
    .then( data => {
        if(data.status == 200) {
            window.open('/crm/assets/js/plugins/mpdf/formato.pdf','_blanck'); 
            // class DeleteFile needs URL parameter
            const FILE = new DeleteFile('../js/plugins/mpdf/formato.pdf');
            FILE.deteleFile();
        }
    })
    
}

function getVendedoresOptios() {
    
    return fetch('php/select-vendedores.php')
    .then(data => data.json())
    .then(data => {
        return data;
    })
}


function createVendedoresOptions(vendedores) {
 
    let option = '<option>Selecciona uno</option>';
    vendedores.forEach(vendedor => {
        option += `<option value="${vendedor.userId}"> ${vendedor.Nombre} ${vendedor.Apellido}</option>`        
    });
    return option;
    
}

function getMedicos() {

    return fetch('php/select-doctors.php')
    .then(data => data.json())
    .then(data => {
        return data;
    })

}

async function setMedicos() {

    const MEDICOS = await getMedicos();
    let options = '<option>Selecciona uno</option> <option value="na">NA</option>';
    MEDICOS.forEach(medico => {        
        options += `<option value="${medico.DocId}">${medico.Nombre} ${medico.Apellido}</option>`;        
    });
    document.querySelector('[name=medico]').innerHTML = options;
        
}

