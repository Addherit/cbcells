<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

if($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        //vars from form
        $username= $_POST['user-name'];
        $pass= $_POST['password'];
        $correo= $_POST['correo'];
        $tipouser= $_POST['tipo-user'];
        $nombre= $_POST['nombres'];
        $apellido= $_POST['apellidos'];

        // sql query for INSERT INTO Medicos
        $sql = "INSERT INTO `Users`(`name`, `pass`, `status`, `correo`, `Tipo_usuario`, `Nombre`, `Apellido`) 
        VALUES ('$username','$pass','activo','$correo',$tipouser,'$nombre','$apellido')";

        // Performs the $sql query on the server to insert the values
        if ($conn->query($sql) === TRUE) {
            $result = ['type' => "success", 'msn' => "Usuario creado correctamente"];
        }
        else {
            $result = ['type' => "danger", 'msn' => "Problema del query"];
        }

        $conn->close();
    } catch (PDOException  $e) {
        $result = ['type' => "danger", 'msn' => "Problema de la conexión"];
    }
    $result["query"] = $sql;

    echo json_encode($result);
}
?>