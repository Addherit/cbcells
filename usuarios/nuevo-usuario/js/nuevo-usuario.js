'use strict';

document.addEventListener('DOMContentLoaded', () => { getDateNow(); setTipoUsuarios() })
document.querySelector('#form-add-user').addEventListener('submit', (event) => { insertUser(); event.preventDefault() })

function getDateNow() {

    setInterval(() => { 
        document.querySelector('[name=fecha]').parentElement.classList.add('is-filled');
        document.querySelector('[name=fecha]').value = moment().format('DD-MM-YYYY h:mm:ss a');
    }, 1000);

}

function getTipoUsuarios() {
    
    return fetch('php/select-tipo-user.php')
    .then(data => data.json())
    .then(data => {
        return data;
    })

}

async function setTipoUsuarios() {

    const TIPO_USERS = await getTipoUsuarios();
    const OPTIONS = getTipoUserOptions(TIPO_USERS);
    document.querySelector('[name=tipo-user]').innerHTML = OPTIONS;

}

function  getTipoUserOptions(TIPO_USERS) {

    let options = '<option value="" selected disabled>Selecciona uno</option>'

    if (localStorage.getItem('user_type') == '3') {
        TIPO_USERS.forEach(tipo_user => {
            if(tipo_user.id == '2')
                options +=`<option value="${tipo_user.id}">${tipo_user.Tipo}</option>`            
        });
    } else {
        TIPO_USERS.forEach(tipo_user => {        
            options +=`<option value="${tipo_user.id}">${tipo_user.Tipo}</option>`            
        });
    }
    
    return options;

}

function insertUser() {

    const FORM_DATA = new FormData(document.querySelector('#form-add-user'))
    fetch('php/insert-user.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
        if(data.type == 'success') { document.querySelector("#form-add-user").reset() }
    })

}
