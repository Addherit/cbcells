<!DOCTYPE html>
<html lang="en">

<head>
  <?php include '../../header.html' ?>
</head>

<body class="">
  <div class="wrapper ">
    <?php include '../../sidebar/sidebar.html' ?>
    <div class="main-panel">
      <?php include '../../sidebar/navbar.html' ?>
      <div class="content">
        <div class="container-fluid">       
          <div class="row">
            <div class="col-md-10 offset-md-1"> 
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Datos del Usuario</h4>
                  <p class="card-category">Ingresa la información del usuario</p>
                </div>
                <div class="card-body">
                  <form id="form-edit-user">
                    <div class="row">
                      <div class="col-md-5 offset-md-7">
                        <div class="form-group">
                          <label class="bmd-label-floating">Fecha</label>
                          <input type="text" class="form-control" name="fecha" readonly style="background-color: white">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Nombres</label>
                          <input type="text" class="form-control" name="nombres">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Apellidos</label>
                          <input type="text" class="form-control" name="apellidos">
                        </div>
                      </div>
                    </div>                                                            
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Correo Eletrónico</label>
                          <input type="text" class="form-control" name="correo">
                        </div>
                      </div>                     
                    </div>                    
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Password</label>
                          <input type="text" class="form-control" name="password">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">User name</label>
                          <input type="text" class="form-control" name="user-name">
                        </div>
                      </div>
                     
                      <div class="col-md-4">
                        <div class="form-group select-custom">
                          <label class="bmd-label-floating">Tipo de usuario</label>                          
                          <select class="custom-select" name="tipo-user">                     
                          </select>
                        </div>                        
                      </div>
                    </div>       
                    <br>
                    <div class="row" id="footer-btns">
                      <div class="col-12">                        
                        <button type="button" class="btn btn-primary pull-right float-left" id="return-to"> <i class="material-icons">arrow_back_ios</i> Listado de Usuarios</button>
                        <button type="submit" class="btn btn-primary pull-right" id="btn-save"><span class="material-icons">save_alt</span> Actualizar datos</button>                        
                      </div>
                    </div>                                        
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
</body>
<?php include '../../footer.html' ?>
<script src="js/editar-usuario.js"></script>

</html>