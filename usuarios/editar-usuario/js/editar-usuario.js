document.addEventListener('DOMContentLoaded', () => { 
    
    getDateNow(); 
    setTipoUsuarios();
    setTimeout(() => { searchUser() }, 200);

})
document.querySelector('#form-edit-user').addEventListener('submit', (event) => { event.preventDefault(); updatetUser() })
document.querySelector('#return-to').addEventListener('click', () => { returnToListUsers() })

function getDateNow() {

    setInterval(() => { 
        document.querySelector('[name=fecha]').parentElement.classList.add('is-filled');
        document.querySelector('[name=fecha]').value = moment().format('DD-MM-YYYY h:mm:ss a');
    }, 1000);

}

function getTipoUsuarios() {
    
    return fetch('php/select-tipo-user.php')
    .then(data => data.json())
    .then(data => {
        return data;
    })

}

async function setTipoUsuarios() {

    const TIPO_USERS = await getTipoUsuarios();
    const OPTIONS = getTipoUserOptions(TIPO_USERS);
    document.querySelector('[name=tipo-user]').innerHTML = OPTIONS;

}

function  getTipoUserOptions(TIPO_USERS) {

    let options = '<option value="" selected disabled>Selecciona uno</option>'

    if (localStorage.getItem('user_type') == '3') {
        TIPO_USERS.forEach(tipo_user => {
            if(tipo_user.id == '2')
                options +=`<option value="${tipo_user.id}">${tipo_user.Tipo}</option>`            
        });
    } else {
        TIPO_USERS.forEach(tipo_user => {        
            options +=`<option value="${tipo_user.id}">${tipo_user.Tipo}</option>`            
        });
    }
    
    return options;

}

function updatetUser() {

    const PARAMS_URL = new URLSearchParams(window.location.search);    
    const FORM_DATA = new FormData(document.querySelector('#form-edit-user'));
    FORM_DATA.append('userId', PARAMS_URL.get('userId')  )
    fetch('php/edit-user.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
    })

}

function searchUser() {

    const PARAMS_URL = new URLSearchParams(window.location.search);    
    const FORM_DATA = new FormData();
    FORM_DATA.append('userId', PARAMS_URL.get('userId')  )
    fetch('php/select-user.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        putDataInInputs(data[0]);
    })

}

function putDataInInputs(data) {

    addClassIsFilled();    
    // document.querySelector('[name=fecha]').value = data.CreatedDate
    document.querySelector('[name=nombres]').value = data.Nombre
    document.querySelector('[name=apellidos]').value = data.Apellido
    document.querySelector('[name=correo]').value = data.correo
    document.querySelector('[name=password]').value = data.pass
    document.querySelector('[name=user-name]').value = data.name
    document.querySelector('[name=tipo-user]').value = data.Tipo_usuario

}

function addClassIsFilled() {

    //funcion para hacer el efecto de focus en input de datos
    const DIVS = document.querySelectorAll('.bmd-form-group');
    DIVS.forEach(element => {
        element.classList.add('is-filled');        
    });

}

function returnToListUsers() {
    window.location = '/crm/usuarios/lista-usuarios/index.php';
}