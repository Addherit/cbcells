<!DOCTYPE html>
<html lang="en">

<head>
  <?php include '../../header.html' ?>
</head>

<body class="">
  <div class="wrapper ">
    <?php include '../../sidebar/sidebar.html' ?>
    <div class="main-panel">
      <?php include '../../sidebar/navbar.html' ?>
      <div class="content">
        <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary">
                  <h4 class="card-title ">Usuarios</h4>
                  <p class="card-category">Puedes editar o borrar cualquier usuario</p>
              </div>
              <div class="card-body">
                <div class="table-responsive" style="height: auto">
                    <table class="table table-hover" id="tbl-lista-users">
                      <thead class=" text-primary">
                        <th></th>
                        <th></th>
                        <th>ID</th>
                        <th>Nombre completo</th>
                        <th>Correo Electrónico</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Tipo de usuiario</th>
                        <th>Estatus</th>
                      </thead>
                      <tbody>                                      
                      </tbody>
                    </table>
                </div>
              </div>              
            </div>           
          </div>    
        </div>
      </div> 
</body>
<?php include '../../footer.html' ?>
<script src="js/lista-usuarios.js"></script>

</html>