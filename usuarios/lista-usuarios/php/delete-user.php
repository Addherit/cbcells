<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

if($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        //vars from form
        $id= $_POST['userid'];

        // sql query for INSERT INTO Medicos
        $sql = "update Users set status ='desactivo' where userId =$id";
        
        // Performs the $sql query on the server to insert the values
        if (mysqli_query($conn, $sql)) {
            $result = ['type' => "success", 'msn' => "Usuario eliminado correctamente"];
        } else {
            $result = ['type' => "danger", 'msn' => "problema del query"];
        }
        mysqli_close($conn);
    } catch (PDOException  $e) {
        $result = ['type' => "danger", 'msn' => "problema de la conexion"];
    }

    echo json_encode($result);
}
?>