document.addEventListener('DOMContentLoaded', () => { getUsers() })

function getUsers() {

    fetch('php/select-users.php')
    .then(data => data.json())
    .then(data => {
        setTblUsers(data);
    })

}

function setTblUsers(data) {

    const ROWS = getRows(data);
    document.querySelector('#tbl-lista-users tbody').innerHTML = ROWS;
    
}

function getRows(data) {
    
    let filas = '';
    data.forEach(user => {
        filas += `
            <tr>
            <td><a href="#" class="text-danger btn-tbl-accion" onclick="deleteUser(${user.userId})"><span class="material-icons">delete</span></a></td>
            <td><a href="#" class="text-info btn-tbl-accion" onclick="editUser(${user.userId})"><span class="material-icons">create</span></a></td>
                <td>${user.userId}</td>
                <td>${user.Nombre} ${user.Apellido}</td>
                <td>${user.correo}</td>
                <td>${user.name}</td>
                <td>${user.pass}</td>
                <td>${user.Tipo_usuario}</td>
                <td class="text-success">${user.status}</td>
            </tr>
        `
    });
    return filas;

}

function deleteUser(user_id) {
    
    const FORM_DATA = new FormData();
    FORM_DATA.append('userid', user_id);
    fetch('php/delete-user.php',{ method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        //mensaaje de confirmacion
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
        getUsers()
    })

}

function editUser(user_id) {
    window.location = `/crm/usuarios/editar-usuario/index.php?userId=${user_id}`;
}