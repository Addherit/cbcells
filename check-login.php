<?php 
session_start();
header('Content-type: application/json');

try {
    if (isset($_SESSION['res'])) {
        //print_r($_SESSION['res']);
        $result = ["mensaje" => "false"];
    }else{
        $result = ["mensaje" => "true"];
    }
} catch(PDOException $e) {
    $result = ["mensaje" => "Error: ".$e->getMessage()];
}
echo json_encode($result);
?>