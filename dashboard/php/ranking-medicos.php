<?php 
header('Content-type: application/json');
include_once('../../assets/db/conexion.php');

try {
  $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $final_data=[];

  $sdate = $_POST['medico-startDate'];
  $edate = $_POST['medico-endDate'];
  $estado = $_POST['estatus-venta'];
  $idven = $_POST['nombre-vendedor'];
  $idmed = $_POST['nombre-medico'];

  $query = "select concat(d.Nombre ,' ',d.Apellido) as 'Medico', SUM(i.Total) as 'Total' from Doctores d inner join Ordenes o on o.MedId = d.DocId LEFT join ItemsOrd i on i.OrderId = o.OrderId where o.Estado NOT IN ('Eliminado') and i.Estado = 'activo' ";
  $group = " group by d.DocId ORDER BY SUM(i.Total) DESC";

  if($sdate != null){
    $dates = " and  o.DateCreated BETWEEN '$sdate' AND '$edate' ";
    $query .= $dates;
  }
  if($estado != null){
    $estatus = " and o.Estado = '$estado' ";
    $query .= $estatus;
  }
  if($idven != null){
    $vende = " and o.VendedorId = '$idven' ";
    $query .= $vende;
  }
  if($idmed != null){
    $vende = " and d.DocId = '$idven' ";
    $query .= $vende;
  }

  $query .= $group;
  $datos = $con->query($query)->fetchAll(PDO::FETCH_ASSOC );

  foreach($datos as $row){
    $final_data[] = $row;
  }
    echo json_encode($final_data);
} catch(PDOException $e) {
    echo $result = ["mensaje" => "Error: ".$e->getMessage()];
}
?>