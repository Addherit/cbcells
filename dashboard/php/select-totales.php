<?php 
header('Content-type: application/json');
include_once('../../assets/db/conexion.php');

try {
  $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $con->exec("set names utf8");
  $final_data=[];

  $query = "select SUM(i.Total) as 'Total','Total' as Estado from Ordenes o inner join ItemsOrd i on i.OrderId = o.OrderId inner join Users u on u.userId = o.VendedorId where i.Estado = 'activo' and o.`Estado` <> 'Eliminado' UNION ALL select SUM(i.Total) as 'Total','Abierto' as Estado from Ordenes o inner join ItemsOrd i on i.OrderId = o.OrderId inner join Users u on u.userId = o.VendedorId where i.Estado = 'activo' and o.`Estado` = 'Abierto' UNION ALL select SUM(i.Total) as 'Total','Cerrado' as Estado from Ordenes o inner join ItemsOrd i on i.OrderId = o.OrderId inner join Users u on u.userId = o.VendedorId where i.Estado = 'activo' and o.`Estado` = 'Cerrado' ";
  $datos = $con->query($query)->fetchAll(PDO::FETCH_ASSOC );

  foreach($datos as $row){
    $final_data[] = $row;
  }
    echo json_encode($final_data);
} catch(PDOException $e) {
    echo $result = ["mensaje" => "Error: ".$e->getMessage()];
}
?>