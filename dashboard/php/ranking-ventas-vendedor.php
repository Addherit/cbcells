<?php 
header('Content-type: application/json');
include_once('../../assets/db/conexion.php');

try {
  $final_data=[];

  $sdate = $_POST['vendedor-startDate'];
  $edate = $_POST['vendedor-endDate'];
  $estado = $_POST['estatus-venta'];
  $idven = $_POST['nombre-vendedor'];

  $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $query = "select concat(u.nombre ,' ',u.apellido) as 'Vendedor', SUM(i.Total) as 'Total' from Ordenes o inner join ItemsOrd i on i.OrderId = o.OrderId inner join Users u on u.userId = o.VendedorId  where o.Estado NOT IN ('Eliminado') and i.Estado = 'activo' ";
  $group= "group by o.VendedorId ORDER BY SUM(i.Total) DESC";

  if($sdate != null){
    $dates = " and o.DateCreated BETWEEN '$sdate' AND '$edate' ";
    $query .= $dates;
  }
  if($estado != null){
    $estatus = " and o.Estado = '$estado' ";
    $query .= $estatus;
  }
  if($idven != null){
    $vende = " and o.VendedorId = '$idven' ";
    $query .= $vende;
  }
  
  $query .= $group;

 // echo $query;

  $datos = $con->query($query)->fetchAll(PDO::FETCH_ASSOC );

  foreach($datos as $row){
    $final_data[] = $row;
  }
    echo json_encode($final_data);
} catch(PDOException $e) {
    echo $result = ["mensaje" => "Error: ".$e->getMessage()];
}
?>