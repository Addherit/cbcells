<?php 
header('Content-type: application/json');
include_once('../../assets/db/conexion.php');

try {
  $sdate = $_POST['sku-startDate'];
  $edate = $_POST['sku-endDate'];
  $estado = $_POST['estatus-venta'];
  $idven = $_POST['nombre-vendedor'];

  $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $final_data=[];

  $query ="Select inv.Sku,inv.Nombre as 'Concepto',Sum(i.Cantidad_Original) as 'CantidadU', SUM(i.Total) as 'Total', IFNULL((Select  SUM(it.Total) from ItemsOrd it inner join Ordenes od on it.OrderId = od.OrderId where  it.Estado = 'activo' AND od.Parcial ='si' and od.Estado not in('Eliminado','Cerrado')  and it.ItemId = i.ItemId),0)as 'VtaPar', IFNULL((Select  SUM(it.Total) from ItemsOrd it inner join Ordenes od on it.OrderId = od.OrderId where  it.Estado = 'activo' AND od.Parcial ='si' and od.Estado ='Cerrado' and it.ItemId = i.ItemId),0) as 'VtaCer' from ItemsOrd i inner join Inventario inv on inv.Id = i.ItemId inner join Ordenes o on i.OrderId = o.OrderId where o.Estado NOT IN ('Eliminado') and i.Estado = 'activo' ";

  $group =" group by inv.Sku,inv.Nombre,VtaPar,VtaCer order by SUM(i.Total) DESC LIMIT 10";
//  $query ="Select inv.Sku,inv.Nombre as 'Concepto',count(*) as 'CantidadU', SUM(i.Total) as 'Total', '0' as 'VtaPar' from ItemsOrd i inner join Inventario inv on inv.Id = i.ItemId inner join Ordenes o on i.OrderId = o.OrderId where o.Estado NOT IN ('Eliminado') and i.Estado = 'activo' ";
 // $group =" group by inv.Sku,inv.Nombre order by SUM(i.Total) DESC LIMIT 10";

  if($sdate != null){
    $dates = " and  i.DateCreated BETWEEN '$sdate' AND '$edate' ";
    $query .= $dates;
  }
  if($estado != null){
    $estatus = " and o.Estado = '$estado' ";
    $query .= $estatus;
  }
  if($idven != null){
    $vende = " and o.VendedorId = '$idven' ";
    $query .= $vende;
  }

  $query .= $group;
  $datos = $con->query($query)->fetchAll(PDO::FETCH_ASSOC );

  foreach($datos as $row){
    $final_data[] = $row;
  }
//  $final_data['query']= $query;
    echo json_encode($final_data);
} catch(PDOException $e) {
    echo $result = ["mensaje" => "Error: ".$e->getMessage()];
}
?>