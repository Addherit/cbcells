<?php 
header('Content-type: application/json');
include_once('../../assets/db/conexion.php');

try {
  $vendedor = $_POST['vendedor'];
  $fechaini = $_POST['fecha_ini'];
  $fechafin = $_POST['fecha_fin'];
 
  //atributo utf8
  $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $con->exec("set names utf8");
  $final_data=[];



  $query = "SELECT o.`OrderId`,o.`DateCreated`,CONCAT(u.Nombre,' ',u.Apellido) as Vendedor,CONCAT(d.Nombre,' ',d.Apellido) as Medico,i.Cantidad_Original,i.Concepto FROM `Ordenes` o LEFT JOIN ItemsOrd i on i.OrderId = o.`OrderId` LEFT JOIN Users u on u.userId = o.`VendedorId` LEFT JOIN Doctores d on d.DocId = o.`MedId` where o.`Estado` <> 'Eliminado' and i.Estado='Activo' ";

  $w = "";
  if(is_numeric($vendedor)){
	$w =" AND o.`VendedorId` = $vendedor ";
  }
  if ($fechaini != ' ') {
	$w = " AND o.`DateCreated` BETWEEN '$fechaini' and '$fechafin' ";
  }
  $query .= $w;

  $datos = $con->query($query)->fetchAll(PDO::FETCH_ASSOC );

  foreach($datos as $row){
    $final_data[] = $row;
  }

  echo json_encode($final_data);
} catch(PDOException $e) {
    echo $result = ["mensaje" => "Error: ".$e->getMessage()];
}
?>