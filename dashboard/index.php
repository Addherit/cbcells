<!DOCTYPE html>
<html lang="en">

<head>
  <?php include '../header.html' ?>
  <style>
    body {
      background-image: url('../assets/img/bg-cells-dashboard.jpg')
    }
  </style>
</head>

<body>
  <div class="wrapper ">
    <?php include '../sidebar/sidebar.html' ?>
    <div class="main-panel">
      <?php include '../sidebar/navbar.html' ?>
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-8 col-lg-9">
              <div class="card card-chart">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Ranking Ventas Vendedor</h4>                  
                </div>
                <div class="card-body">
                  <form id="form-ranking-ventas-vendedor">
                    <div class="row">
                      <div class="col-6 col-sm-4 col-lg-6 col-xl-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Rango de fechas</label>
                          <input type="text" class="form-control datefilter" name="date-range-vendedor">
                          <input type="hidden" name="vendedor-startDate">
                          <input type="hidden" name="vendedor-endDate">
                        </div>
                      </div>                   
                      <div class="col-6 col-xl-3">
                        <div class="form-group select-custom">
                          <label class="bmd-label-floating">Vendedor</label>
                          <select class="custom-select slt-vendedor" name="nombre-vendedor"></select>
                        </div>
                      </div>
                      <!-- <div class="col-5 col-xl-3">
                        <div class="form-group select-custom">
                          <label class="bmd-label-floating">Médico</label>                          
                          <select class="custom-select slt-medico" name="nombre-medico"></select>
                        </div>
                      </div> -->
                      <div class="col-4 col-xl-3">
                        <div class="form-group select-custom">
                          <label class="bmd-label-floating">Estatus</label>
                          <select class="custom-select" name="estatus-venta">
                            <option value="" selected disabled></option>
                            <option value="abierto">Abierto</option>
                            <option value="autorizado">Autorizado</option>
                            <option value="concluir">Concluir</option>
                            <option value="cerrado">Cerrado</option>
                          </select>                          
                        </div>
                      </div>  
                      <div class="col-3 col-xl-1">
                        <div class="input-group no-border">                            
                          <button type="submit" class="btn btn-white btn-round btn-just-icon">
                            <i class="material-icons">search</i>
                            <div class="ripple-container"></div>
                          </button>
                        </div>
                      </div> 
                      <div class="col-3 col-xl-1">                        
                        <button type="button" class="btn btn-white btn-round btn-just-icon" id="btn-excel" title="Generar excel">
                        <i class="material-icons" style="color: green">insert_drive_file</i>
                        </button>                        
                      </div>
                    </div>  
                  </form>
                <div class="chart" id="ranking-ventas-vendedor"></div>
                </div>                
              </div>
            </div>
            <div class="col-md-4 col-lg-3">
              <div class="row">
                <div class="col-6 col-md-12">
                  <div class="card card-stats">
                    <div class="card-header card-header-success card-header-icon">
                      <div class="card-icon">
                        <span class="material-icons">store</span>
                      </div>
                      <p class="card-category">Ventas Totales</p>
                      <h3 class="card-title" id="ventas-totales">$0.00</h3>
                    </div>
                  </div>
                </div>            
                <div class="col-6 col-md-12">
                  <div class="card card-stats">
                    <div class="card-header card-header-warning card-header-icon">
                      <div class="card-icon">
                        <span class="material-icons">error_outline</span>
                      </div>
                      <p class="card-category">Ordenes Abiertas</p>
                      <h3 class="card-title" id="ventas-abiertas">$0.00</h3>                                      
                    </div>                                       
                  </div> 
                </div>          
                <div class="col-md-12">
                  <div class="card card-stats">
                    <div class="card-header card-header-primary card-header-icon">
                      <div class="card-icon">
                        <span class="material-icons">check_circle_outline</span>
                      </div>
                      <p class="card-category">Ordenes Cerradas</p>
                      <h3 class="card-title" id="ventas-cerradas">$0.00</h3>
                    </div>                   
                  </div>
                </div>
              </div>                
            </div>              
          </div>          
          <div class="row">
            <div class="col-md-7">
              <div class="card card-chart">
                <div class="card-header card-header-warning">
                <h4 class="card-title">Ranking Ventas por SKU</h4>           
                </div>
                <div class="card-body">
                  <form id="form-ranking-ventas-sku">
                    <div class="row">
                      <div class="col-6 col-sm-4 col-lg-6 col-xl-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Rango de fechas</label>
                          <input type="text" class="form-control datefilter" name="date-range-sku">
                          <input type="hidden" name="sku-startDate">
                          <input type="hidden" name="sku-endDate">
                        </div>
                      </div>                   
                      <div class="col-6 col-xl-3">
                        <div class="form-group select-custom">
                          <label class="bmd-label-floating">Vendedor</label>
                          <select class="custom-select slt-vendedor" name="nombre-vendedor"></select>
                        </div>
                      </div>
                      <div class="col-5 col-xl-3">
                        <div class="form-group select-custom">
                          <label class="bmd-label-floating">Médico</label>                          
                          <select class="custom-select slt-medico" name="nombre-medico"></select>
                        </div>
                      </div>
                      <div class="col-4 col-xl-2">
                        <div class="form-group select-custom">
                          <label class="bmd-label-floating">Estatus</label>
                          <select class="custom-select" name="estatus-venta">
                            <option value="" selected disabled></option>
                            <option value="abierto">Abierto</option>
                            <option value="autorizado">Autorizado</option>
                            <option value="concluir">Concluir</option>
                            <option value="cerrado">Cerrado</option>
                          </select>                          
                        </div>
                      </div>  
                      <div class="col-3 col-xl-1">
                        <div class="input-group no-border">                            
                          <button type="submit" class="btn btn-white btn-round btn-just-icon">
                            <i class="material-icons">search</i>
                            <div class="ripple-container"></div>
                          </button>
                        </div>
                      </div> 
                    </div>  
                  </form>
                  <div class="table-responsive" style="height: 390px">
                    <table id="tbl-ranking-sku" class="table table-hover">
                      <thead class="text-primary">
                        <th>Sku</th>
                        <th>Concepto</th>
                        <th>Cant/Unid</th>
                        <th>Vta cerrada</th>
                        <th>Vta parcial</th>
                        <th>Vta total</th>
                      </thead>
                      <tbody></tbody>
                    </table>
                  </div>
                </div>                
              </div>
            </div>        
            <div class="col-md-5">
              <div class="card card-chart">
                <div class="card-header card-header-warning">
                <h4 class="card-title">Ranking Ventas por SKU</h4>                                   
                </div>
                <div class="card-body">                  
                  <div class="chart" id="ranking-ventas-sku"></div>
                </div>               
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <div class="card card-chart">
                <div class="card-header card-header-danger">
                  <h4 class="card-title">Ranking Médicos</h4>
                </div>
                <div class="card-body">
                  <form id="form-ranking-medico">
                    <div class="row">
                      <div class="col-6 col-sm-4 col-lg-6 col-xl-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Rango de fechas</label>
                          <input type="text" class="form-control datefilter" name="date-range-medico">
                          <input type="hidden" name="medico-startDate">
                          <input type="hidden" name="medico-endDate">
                        </div>
                      </div>                   
                      <div class="col-6 col-xl-3">
                        <div class="form-group select-custom">
                          <label class="bmd-label-floating">Vendedor</label>
                          <select class="custom-select slt-vendedor" name="nombre-vendedor"></select>
                        </div>
                      </div>
                      <div class="col-5 col-xl-3">
                        <div class="form-group select-custom">
                          <label class="bmd-label-floating">Médico</label>                          
                          <select class="custom-select slt-medico" name="nombre-medico"></select>
                        </div>
                      </div>
                      <div class="col-4 col-xl-3">
                        <div class="form-group select-custom">
                          <label class="bmd-label-floating">Estatus</label>
                          <select class="custom-select" name="estatus-venta">
                            <option value="" selected disabled></option>
                            <option value="abierto">Abierto</option>
                            <option value="autorizado">Autorizado</option>
                            <option value="concluir">Concluir</option>
                            <option value="cerrado">Cerrado</option>
                          </select>                          
                        </div>
                      </div>  
                      <div class="col-3 col-xl-1">
                        <div class="input-group no-border">                            
                          <button type="submit" class="btn btn-white btn-round btn-just-icon">
                            <i class="material-icons">search</i>
                            <div class="ripple-container"></div>
                          </button>
                        </div>
                      </div> 
                    </div>  
                  </form>
                  <div class="chart" id="ranking-medicos" ></div>                                    
                </div>                
              </div>
            </div>          
          </div>
        </div>        
      </div>  

      <div hidden>

        <table id="tbl-toexcel">
          <thead>
            <tr>
              <th>Pedido</th>
              <th>Fecha</th>
              <th>Vendedor</th>
              <th>Médico</th>
              <th>Cantidad</th>
              <th>Prodcuto</th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
      </div>
</body>
<?php include '../footer.html' ?>
<script src="js/dashboard.js"></script>



</html>