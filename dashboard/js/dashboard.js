document.addEventListener('DOMContentLoaded', () => {
  initDashboardPageCharts();
  dateRangePickeer();
  setMedicosSelectList();
  setVendedores();
  setVentas();

});

$(window).resize(() => { initDashboardPageCharts() });

document.querySelector('#form-ranking-ventas-vendedor').addEventListener('submit', (event) => {
  event.preventDefault();
  filtrarRanking('form-ranking-ventas-vendedor', 'ranking-ventas-vendedor');
})
document.querySelector('#form-ranking-ventas-sku').addEventListener('submit', (event) => {

  event.preventDefault();
  filtrarRanking('form-ranking-ventas-sku', 'ranking-ventas-sku');

})
document.querySelector('#form-ranking-medico').addEventListener('submit', (event) => {
  event.preventDefault();
  filtrarRanking('form-ranking-medico', 'ranking-medicos');
})

document.querySelector('#btn-excel').addEventListener('click', () => getExcel());


function getAllCharts() {
  return document.querySelectorAll('.chart');
}
function initDashboardPageCharts() {

  // Obtiene todos los nombres de los charts y obtiene su data para posterior crearlas con createChart()
  const ALL_CHARTS = getAllCharts();
  const FORM_DATA = new FormData();
	ALL_CHARTS.forEach( async chart => {
    const DATA = await getDataCharts(chart.id, FORM_DATA);    
    createChart(chart.id, DATA);    
	});

}
function dateRangePickeer() {

  // Inicializa todos los dateRnagePicker con clase dateFilter
  $('.datefilter').daterangepicker({
    autoUpdateInput: false,
    locale: {
        cancelLabel: 'Clear'
    }
  });
  // Se inicializa las funcionalidades del dateRangePicker()
  applyDateRangePicker();

}
function applyDateRangePicker() {

  // Esta funcion se ejecuta cuando haces click en el botón de apply de dateRangePicker
  // esto es para ranking vendedor--------------------------------------------------------------
  $('input[name="date-range-vendedor"]').on('apply.daterangepicker', (ev, picker) => {
    let start_date = picker.startDate.format('YYYY-MM-DD');
    let end_date = picker.endDate.format('YYYY-MM-DD');
    document.querySelector('[name=date-range-vendedor]').parentElement.classList.add('is-filled');
    $('input[name="date-range-vendedor"]').val(start_date + ' - ' + end_date);      
    document.querySelector('[name=vendedor-startDate]').value = start_date;
    document.querySelector('[name=vendedor-endDate]').value = end_date;
  });
  
  $('input[name="date-range-vendedor"]').on('cancel.daterangepicker', (ev, picker) => {
    $('input[name="date-range-vendedor"]').val('');    
  });

  // esto es de ranking sku-----------------------------------------------------------------------
  $('input[name="date-range-sku"]').on('apply.daterangepicker', (ev, picker) => {
    let start_date = picker.startDate.format('YYYY-MM-DD');
    let end_date = picker.endDate.format('YYYY-MM-DD');
    document.querySelector('[name=date-range-sku]').parentElement.classList.add('is-filled');
    $('input[name="date-range-sku"]').val(start_date + ' - ' + end_date);  
    document.querySelector('[name=sku-startDate]').value = start_date;
    document.querySelector('[name=sku-endDate]').value = end_date;    
  });
  
  $('input[name="date-range-sku"]').on('cancel.daterangepicker', (ev, picker) => {
    $('input[name="date-range-sku"]').val('');    
  });

  // esto es para rankin medicos------------------------------------------------------------------
  $('input[name="date-range-medico"]').on('apply.daterangepicker', (ev, picker) => {
    let start_date = picker.startDate.format('YYYY-MM-DD');
    let end_date = picker.endDate.format('YYYY-MM-DD');
    document.querySelector('[name=date-range-medico]').parentElement.classList.add('is-filled');
    $('input[name="date-range-medico"]').val(start_date + ' - ' + end_date);   
    document.querySelector('[name=medico-startDate]').value = start_date;
    document.querySelector('[name=medico-endDate]').value = end_date;     
  });
  
  $('input[name="date-range-medico"]').on('cancel.daterangepicker', (ev, picker) => {
    $('input[name="date-range-medico"]').val('');    
  });

}

// Paquete de colores barras de vendedores------------------------------------------------------
function getMaxValue(data_json) {

  let values = [];
  data_json.forEach(element => {
    values.push(element.Total);
  });
  return Math.max(...values);

}
function getColor(cantidad, max_value) {

  let PRIMARY_COLOR = '';
  let porcentaje = ( cantidad*100 ) / max_value;

  if(porcentaje <= 100 && porcentaje > 80 ) { PRIMARY_COLOR = '#47a34b' }
  if(porcentaje <= 80 && porcentaje > 60 ) { PRIMARY_COLOR = '#8EC656' }
  if(porcentaje <= 60) { PRIMARY_COLOR = '#C3C656' }
  return PRIMARY_COLOR;

}
//END-------------------------------------------------------------------------------------------

// Paquete de options medicos------------------------------------------------------
function getMedicos() {

  const FORM_DATA = new FormData()
  FORM_DATA.append('userId', localStorage.getItem('user_id'))
  return fetch('php/select-doctors.php', { method: 'POST', body: FORM_DATA })
  .then(data => data.json())
  .then(data => {
      return data
  });

}
async function setMedicosSelectList() {

  const MEDICOS = await getMedicos();
  const OPTIONS = getOptiosMedicos(MEDICOS);
  document.querySelectorAll('.slt-medico').forEach(slt => {
    slt.innerHTML = OPTIONS;
  })

}
function getOptiosMedicos(medicos) {

  let options = `<option selected disabled></option>`;
  medicos.forEach(medico => {
      options += `<option value="${medico.DocId}">${medico.Nombre} ${medico.Apellido}</option>`;        
  });
  return options;

}
//END-------------------------------------------------------------------------------

// Paquete de options vendedores-----------------------------------------------------
function getVendedores() {
    
  return fetch('php/select-vendedores.php')
  .then(data => data.json())
  .then(data => {
      return data;
  })
}
async function setVendedores() {

  const DATA_VENDEDORES = await getVendedores();
  const OPTIONS = getVendedoresOptions(DATA_VENDEDORES); 
  document.querySelectorAll('.slt-vendedor').forEach(slt => {    
    slt.innerHTML = OPTIONS;
  })
}
function getVendedoresOptions(vendedores) {
  
  let option = '<option selected disabled></option>';
  vendedores.forEach(vendedor => {
      option += `<option value="${vendedor.userId}"> ${vendedor.Nombre} ${vendedor.Apellido}</option>`        
  });
  return option;
  
}
// END-------------------------------------------------------------------------------

async function filtrarRanking(form_id, chart_id) {

  // se hace una consulta segun los filtros y se pinta nueva data en chart
  const FORM_DATA = new FormData(document.querySelector(`#${form_id}`));
  const DATA = await getDataCharts(chart_id, FORM_DATA);
  if (DATA == 0 ) {
    document.getElementById('ranking-ventas-vendedor').innerHTML = 'Nos hay resultado de esta busqueda';
  } else {
    createChart(chart_id, DATA);   
  }
 

}
function getDataCharts(chart_id, form_data) {

  return fetch(`php/${chart_id}.php`, {method: 'POST', body: form_data })
  .then(data => data.json())
  .then(data => {
    return data;   
  })

}
function createChart(chart_id, response) {
  
  let data_chart
  switch (chart_id) {
    case 'ranking-ventas-vendedor':      
      data_chart = createDataToVendedorChart(response);
      drawChartVendedor(data_chart);      	
      break;
    case 'ranking-ventas-sku':
      data_chart = createDataToSkuChart(response);
      setTableSku(response)
      drawChartSku(data_chart); 
      break;
    case 'ranking-medicos':      
      data_chart = createDataToMedicosChart(response);      
      drawChartMedicos(data_chart); 
      break;
    // case 'ranking-medicos':
    //   data_chart = createDataToMedicoChart(response);
    //   drawChartMedico(data_chart); 
    //   break;
  }
  
}


// chart vendedor  -------------------------------------------------------------
function drawChartVendedor(data_chart) {

  google.charts.load("current", {packages:["corechart"]});
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable(data_chart);
    var view = new google.visualization.DataView(data);
    view.setColumns([0, 1, { calc: "stringify", sourceColumn: 1, type: "string", role: "annotation" }, 2]);
    var options = {
      width: '100%',
      height: 300,
      bars: 'horizontal', // Required for Material Bar Charts.
      bar: {groupWidth: "33%"},
      legend: { position: "none" },             
    }                
    var chart = new google.visualization.BarChart(document.getElementById('ranking-ventas-vendedor'));
    chart.draw(view, options);  
  }

}
function createDataToVendedorChart(data_json) {

  let response = [];
  let max_value = getMaxValue(data_json);
  //esta linea es parte del código necesario de la libreria de google charts
  response.push(['Element', 'Cantidad Total', { role: 'style' } ]);
  data_json.forEach(element => {
    const COLOR = getColor(element.Total, max_value)    
    response.push([element.Vendedor, parseFloat(element.Total), COLOR]);
  });
  return response;

}
//------------------------------------------------------------------------------


// chart sku  -------------------------------------------------------------
function drawChartSku(data_chart) {

  google.charts.load('current', {'packages':['bar']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
      var data = google.visualization.arrayToDataTable(data_chart);
      var options = {         
        bars: 'horizontal', // Required for Material Bar Charts.
        colors: ['#30a5db', '#50b154']
      };
      var chart = new google.charts.Bar(document.getElementById('ranking-ventas-sku'));
      chart.draw(data, google.charts.Bar.convertOptions(options));
    }

}
function createDataToSkuChart(data_json) {

  let response = [];
  //esta linea es parte del código necesario de la libreria de google charts
  response.push( ['Sku', 'Vta Parcial', 'Vta Cerrada']);
  data_json.forEach(element => {
    response.push([element.Sku, parseFloat(element.VtaPar), parseFloat(element.VtaCer)]);
  });
  return response;

}
function setTableSku(data_json) {

  const ROWS = getRowsTblRankingSku(data_json);
  document.querySelector('#tbl-ranking-sku tbody').innerHTML = ROWS;
}

function getRowsTblRankingSku(data_json) {
  
  let linea = ''
  data_json.forEach( row => {
    linea += `
      <tr>
        <td>${row.Sku}</td>
        <td>${row.Concepto}</td>
        <td>${row.CantidadU}</td>      
        <td>
          <span class="material-icons" style="vertical-align: text-top; font-size: 18px !important">attach_money</span>
          <span>
            ${row.VtaCer}</td>
          </span>
          </td>
        <td>
          <span class="material-icons" style="vertical-align: text-top; font-size: 18px !important">attach_money</span>
          <span>
            ${row.VtaPar}</td>
          </span>          
        <td>
          <span class="material-icons" style="vertical-align: text-top; font-size: 18px !important">attach_money</span>
          <span>
            ${row.Total}</td>
          </span>
          
      </tr>
    `;
  })
  return linea;

}
//------------------------------------------------------------------------------



// chart medicos  -------------------------------------------------------------
function drawChartMedicos(data_chart) {

  google.charts.load("current", {packages:["corechart"]});
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable(data_chart);
    var view = new google.visualization.DataView(data);
    view.setColumns([0, 1, { calc: "stringify", sourceColumn: 1, type: "string", role: "annotation" }, 2]);
    var options = {
      width: '100%',
      height: 300,
      bars: 'horizontal', // Required for Material Bar Charts.
      bar: {groupWidth: "33%"},
      legend: { position: "none" },             
    }                
    var chart = new google.visualization.BarChart(document.getElementById('ranking-medicos'));
    chart.draw(view, options);  
  }

}
function createDataToMedicosChart(data_json) {
 
  let response = [];
  let max_value = getMaxValue(data_json);
  //esta linea es parte del código necesario de la libreria de google charts
  response.push(['Element', 'Cantidad Total', { role: 'style' } ]);
  data_json.forEach(element => {
    const COLOR = getColor(element.Total, max_value)    
    response.push([element.Medico, parseFloat(element.Total), COLOR]);
  });
  return response;

}
//------------------------------------------------------------------------------

async function setVentas() {

  const VENTAS = await getVentas();

  VENTAS.forEach(tipo => {
    switch (tipo.Estado) {
      case 'Total':
        document.querySelector('#ventas-totales').innerHTML = ` 
        <span class="material-icons" style="margin-top: 10px; font-size: 15px !important">attach_money</span>
        ${formatter.currencyFormat(tipo.Total)}`
        break;
      case 'Abierto':
        document.querySelector('#ventas-abiertas').innerHTML = `
        <span class="material-icons" style="margin-top: 10px; font-size: 15px !important">attach_money</span>
         ${formatter.currencyFormat(tipo.Total)}`
        break;
      case 'Cerrado':
        document.querySelector('#ventas-cerradas').innerHTML = `
        <span class="material-icons" style="margin-top: 10px; font-size: 15px !important">attach_money</span>
        ${formatter.currencyFormat(tipo.Total)}`
        break;
    }
  });  

}

function getVentas() {

  return fetch('php/select-totales.php')
  .then(data => data.json())
  .then(data => {
    return data;
  })

}

function downloadExcel(tableID) {

  var uri = 'data:application/vnd.ms-excel;base64,'
  , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
  , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
  , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }

  var table = tableID;
  var name = 'nombre_hoja_calculo';

  if (!table.nodeType) table = document.getElementById(table)
   var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
   window.location.href = uri + base64(format(template, ctx))            

}

async function getExcel() {

  // validate if exist rango de fecha and vendedor
  const fechas = document.querySelector('[name=date-range-vendedor]').value;
  const vendedor = document.querySelector('[name=nombre-vendedor]').value ? document.querySelector('[name=nombre-vendedor]').value : 'todos';

  if(fechas && vendedor) {

    const FORM_DATA = new FormData();
    FORM_DATA.append('fecha_ini', fechas.split(' - ')[0]);
    FORM_DATA.append('fecha_fin', fechas.split(' - ')[1]);
    FORM_DATA.append('vendedor', vendedor)
    const DATA =  await fetch('php/select-excel.php', {method: 'POST', body: FORM_DATA });
    const PEDIDOS = await DATA.json();
    let lineas = ``;

    if (PEDIDOS.length) {
      PEDIDOS.forEach(pedido => {
        lineas += `
          <tr>
            <td>${pedido.OrderId}</td>
            <td>${pedido.DateCreated}</td>
            <td>${pedido.Vendedor}</td>
            <td>${pedido.Medico}</td>
            <td>${pedido.Cantidad_Original}</td>
            <td>${pedido.Concepto}</td>
          </tr>
        `;
      }) 
      document.querySelector('#tbl-toexcel tbody').innerHTML = lineas;
      downloadExcel('tbl-toexcel'); 
    } else {
      const msn = new Message('danger', 'No se encontró datos para mostrar');
      msn.showMessage(); 
    }          

  } else {
    const msn = new Message('danger', 'Selecciona un rango de fechas y un vendedor para obtener un excel');
    msn.showMessage(); 
  }


}

