document.addEventListener('DOMContentLoaded', () => { setPacientes() })
document.querySelector('#form-search').addEventListener('submit', (event) => { event.preventDefault(); setPacientes() });
document.querySelector('#btn-new-paciente').addEventListener('click', () => { goToNewPaciente() });

async function setPacientes() {

    const PACIENTES = await ssearchPacientes();
    if(PACIENTES.length > 0) {
        const FILAS = createRowsTable(PACIENTES);     
        document.querySelector('#tbl-lista-pacientes tbody').innerHTML = FILAS;
    } else {
        const MSN = new Message('warning', 'No hay pacientes con ese nombre');
        MSN.showMessage();
    }

}

function createRowsTable(pacientes) {

    let filas = '';
    pacientes.forEach(paciente => {
        filas += `
            <tr>
                <td><a href="#" class="text-danger btn-tbl-accion" onclick="deletePaciente(${paciente.PacId})"><span class="material-icons">delete</span>Eliminar</a></td>
                <td><a href="#" class="text-info btn-tbl-accion" onclick="editPaciente(${paciente.PacId})"><span class="material-icons">create</span>Editar</a></td>
                <td class="text-primary">${paciente.PacId}</td>
                <td>${paciente.Nombre} ${paciente.Apellido}</td>
                <td>${paciente.Edad}</td>
                <td>${paciente.Diagnostico}</td>
                <td>${paciente.Sexo}</td>             
                <td class="text-success">${paciente.Estado}</td>
            </tr>
        `
    });
    return filas;

}

function editPaciente(docId) {
    window.location = `/crm/area de inmunoterapia/paciente/editar-paciente/index.php?DocId=${docId}`;
}

function deletePaciente(docId) {

    var r = confirm("Seguro que desea eliminarlo?");
    if (r == true) {
        const FORM_DATA = new FormData();
        FORM_DATA.append('pacId', docId);
        fetch('php/delete-paciente.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
            const MSN = new Message(data.type, data.msn);
            MSN.showMessage();        
            setPacientes();
        });
    }
}

function ssearchPacientes() {

     const FORM_DATA = new FormData(document.querySelector('#form-search'))
     return fetch('php/select-paciente.php', { method: 'POST', body: FORM_DATA })
     .then(data => data.json())
     .then(data => {
         return data
     })

}

function goToNewPaciente() {
    window.location = `/crm/area de inmunoterapia/paciente/nuevo-paciente/index.php`;
}