document.addEventListener('DOMContentLoaded', () => { setPacienteInfo() })
document.querySelector('#form-edit-paciente').addEventListener('submit', (event) => { event.preventDefault(); editPaciente() })
document.querySelector('#btn-lista-pacientes').addEventListener('click', () => { returnToListPacientes() })
document.querySelector('#btn-pdf').addEventListener('click', () => { getPDF() })

function editPaciente() {
    
    const PARAMS_URL = new URLSearchParams(window.location.search);
    const FORM_DATA = new FormData(document.querySelector('#form-edit-paciente'))
    FORM_DATA.append('PacId', PARAMS_URL.get('DocId')  )
    fetch('php/edit-paciente.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
    })

}

function setPacienteInfo() {

    const PARAMS_URL = new URLSearchParams(window.location.search);    
    const FORM_DATA = new FormData();
    FORM_DATA.append('PacId', PARAMS_URL.get('DocId')  )
    fetch('php/select-paciente.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        putDataInInputs(data[0]);
    })

}

function putDataInInputs(data) {
    
    addClassIsFilled();
    
    document.querySelector('[name=fecha]').value = data.CreatedDate

    document.querySelector('[name=nombres]').value = data.Nombre
    document.querySelector('[name=apellidos]').value = data.Apellido
    document.querySelector('[name=edad]').value = data.Edad
    document.querySelector('[name=diagnostico]').value = data.Diagnostico
    document.querySelector('[name=sexo]').value = data.Sexo

}

function addClassIsFilled() {

    //funcion para hacer el efecto de focus en input de datos
    const DIVS = document.querySelectorAll('.bmd-form-group');
    DIVS.forEach(element => {
        element.classList.add('is-filled');        
    });

}

function returnToListPacientes() {
    window.location = '/crm/area de inmunoterapia/paciente/lista-pacientes/index.php';
}

function getPDF() {

    const FORM_DATA = new FormData(document.querySelector('#form-edit-paciente'))
    FORM_DATA.append('title', 'DATOS DEL PACIENTE');
    FORM_DATA.append('plantilla_type', 'paciente-inmuno');
    fetch('/crm/assets/js/plugins/mpdf/print.php', { method: 'POST', body: FORM_DATA })
    .then( data => {
        if(data.status == 200) {
            window.open('/crm/assets/js/plugins/mpdf/formato.pdf','_blanck'); 
            // class DeleteFile needs URL parameter
            const FILE = new DeleteFile('../js/plugins/mpdf/formato.pdf');
            FILE.deteleFile();
        }
    })
    
}
