<!DOCTYPE html>
<html lang="en">

<head>
  <?php include '../../../header.html' ?>
</head>

<body class="">
  <div class="wrapper ">
    <?php include '../../../sidebar/sidebar.html' ?>
    <div class="main-panel">
      <?php include '../../../sidebar/navbar.html' ?>
      <div class="content">
        <div class="container-fluid">
          <div class="row" id="captura-pdf">
            <div class="col-md-10 offset-md-1">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Edición de datos del paciente</h4>
                  <p class="card-category">Edita la información</p>
                </div>
                <div class="card-body">                  
                  <form id="form-edit-paciente">
                    <div class="row">
                      <div class="col-md-5 offset-md-7">
                        <div class="form-group">
                          <label class="bmd-label-floating">Fecha</label>
                          <input type="text" class="form-control" name="fecha" readonly style="background-color: white">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <h3>Datos del paciente</h3>
                      </div>
                    </div>                                                          
                    <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Nombres</label>
                          <input type="text" class="form-control" name="nombres">
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Apellidos</label>
                          <input type="text" class="form-control" name="apellidos">
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label class="bmd-label-floating">Edad</label>
                          <input type="text" class="form-control" name="edad">
                        </div>
                      </div>  
                      <div class="col-md-9">
                        <div class="form-group">
                          <label class="bmd-label-floating">Diagnostico</label>
                          <input type="text" class="form-control" name="diagnostico">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group select-custom">
                          <label class="bmd-label-floating">Sexo</label>                          
                          <select class="custom-select" name="sexo">
                            <option value="" selected disabled></option>
                            <option value="Hombre">Hombre</option>
                            <option value="Mujer">Mujer</option>
                            <option value="Incluyente">Incluyente</option>
                          </select>
                        </div>                      
                      </div>
                    </div>
                    <br>
                    <br> 
                    <div class="row">
                      <div class="col-12">
                        <button type="submit" class="btn btn-primary pull-right" id="btn-save"><span class="material-icons">save_alt</span>Actualizar datos</button>                        
                        <button type="button" class="btn btn-primary float-left" id="btn-lista-pacientes"><span class="material-icons">arrow_back_ios</span>Lista de pacientes</button>
                        <button type="button" class="btn btn-primary pull-right float-left" id="btn-pdf"><span class="material-icons">content_copy</span> PDF</button>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
</body>
<?php include '../../../footer.html' ?>
<script src="js/editar-paciente.js"></script>

</html>