<?php 
header('Content-type: application/json');
include_once('../../../../assets/db/conexion.php');

if($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        //vars from form
        $nombre= $_POST['nombres'];
        $apellido= $_POST['apellidos'];
        $diagnostico= $_POST['diagnostico'];
        $edad= $_POST['edad'];
        $sexo= $_POST['sexo'];

        // sql query for INSERT INTO Paciente
        $sql ="INSERT INTO `Pacientes`( `Nombre`, `Apellido`, `Diagnostico`, `Edad`,`Sexo`) VALUES ('$nombre','$apellido','$diagnostico','$edad','$sexo')";
        

        // Performs the $sql query on the server to insert the values
        if ($conn->query($sql) === TRUE) {

            $result = ['type' => "success", 'msn' => "Paciente creado correctamente"];
        }
        else {
            $result = ['type' => "danger", 'msn' => "Problema del query"];
        }

        $conn->close();
    } catch (PDOException  $e) {
        $result = ['type' => "danger", 'msn' => "Problema de la conexión"];
    }
    $result["query"] = $sql;

    echo json_encode($result);
}
?>