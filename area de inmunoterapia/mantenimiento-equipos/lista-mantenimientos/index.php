<!DOCTYPE html>
<html lang="en">

<head>
  <?php include '../../../header.html' ?>
</head>

<body class="">
  <div class="wrapper ">
    <?php include '../../../sidebar/sidebar.html' ?>
    <div class="main-panel">
      <?php include '../../../sidebar/navbar.html' ?>
      <div class="content">
        <div class="container-fluid">
          <div class="row">
          <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Mantenimientos</h4>
                  <p class="card-category">Ingresa la información de mantenimiento</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive" style="min-height: 100%">
                    <table class="table" id="tbl-mantenimiento">
                        <thead class=" text-primary">    
                        <th colspan=2></th>                            
                        <th>Descripción</th>
                        <th>Marca</th>
                        <th>Modelo</th>
                        <th># de serie</th>
                        <th>Entregado</th>
                        <th>Mantenimiento</th>
                        <th>Proveedor</th>
                        <th>Contacto</th>  
                        <th>Comentarios</th>  
                        <th>Fecha</th>  
                        <th>Estatus</th>                                 
                        </thead>
                        <tbody></tbody>
                    </table>
                  </div>
                  <br> 
                  <div class="row">
                    <div class="col-12">
                      <!-- <button class="btn btn-primary float-right" id="btn-add-mantenimiento">Agregar</button> -->
                      <button class="btn btn-primary float-right" id="btn-new-mantenimiento"><span class="material-icons">add</span>Nuevo mantenimiento</button>                                                                   
                    </div>
                  </div>              
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
</body>
<?php include '../../../footer.html' ?>
<script src="js/lista-mantenimiento.js"></script>

</html>