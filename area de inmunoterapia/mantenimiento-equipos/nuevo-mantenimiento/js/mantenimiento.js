

document.addEventListener('DOMContentLoaded', () => {
    setTableRows(3);
})

document.querySelector('#btn-add-mantenimiento').addEventListener('click', () => { addInmunoterapia() })
document.querySelector('#btn-lista-mantenimiento').addEventListener('click', () => { goToListMantenimiento() })

function setTableRows(num_rows) {

    const ROWS = getTableRows(num_rows);    
    if (num_rows == 3) {
        document.querySelector('#tbl-mantenimiento tbody').innerHTML = ROWS;
    } else {
        $('#tbl-mantenimiento tbody').append(ROWS);
    }

}

function getTableRows(num_filas) {

    let rows = '';
    for (let index = 0; index < num_filas; index++) {
        rows += 
        `<tr>
            <td contentEditable class="background-td-editable" onkeyup="validateLastRow(this)"></td>
            <td contentEditable class="background-td-editable"></td>
            <td contentEditable class="background-td-editable"></td>
            <td contentEditable class="background-td-editable"></td>
            <td class="background-td-editable"><input type="date"></td>
            <td class="background-td-editable"><input type="date"></td>
            <td contentEditable class="background-td-editable"></td>
            <td contentEditable class="background-td-editable"></td>
            <td contentEditable class="background-td-editable"></td>
        </tr>`
    }
    return rows;    

}

function addInmunoterapia() {

    const TRS = document.querySelectorAll('#tbl-mantenimiento tbody tr');
    TRS.forEach(row => {
        
        if (row.children[0].textContent != '') {
            let form_data = new FormData()
            form_data.append('descripcion', row.children[0].textContent);
            form_data.append('marca', row.children[1].textContent);
            form_data.append('modelo', row.children[2].textContent);
            form_data.append('serie', row.children[3].textContent);
            form_data.append('entregado', row.children[4].children[0].value);
            form_data.append('mantenimiento', row.children[5].children[0].value);
            form_data.append('proveedor', row.children[6].textContent);
            form_data.append('contacto', row.children[7].textContent);
            form_data.append('comentarios', row.children[8].textContent);

            fetch('php/insert-mante.php', { method: 'POST', body: form_data })
            .then(data => data.json())
            .then(data => {
                const MSN = new Message(data.type, data.msn);
                MSN.showMessage();
            })
        }
    });

}

function validateLastRow(td) {

    let rowIndex = td.parentElement.rowIndex
    let rowsLength = document.querySelectorAll('#tbl-mantenimiento tbody tr').length;
    if (rowIndex == rowsLength ) {
        setTableRows(1);
    }

}

function goToListMantenimiento() {
    window.location = `/crm/area de inmunoterapia/mantenimiento-equipos/lista-mantenimientos/index.php`;
}