<?php 
header('Content-type: application/json');
include_once('../../../../assets/db/conexion.php');

if($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        //vars from form
        $id= $_POST['id'];
        $descripcion= $_POST['descripcion'];
        $marca= $_POST['marca'];
        $modelo= $_POST['modelo'];
        $noserie= $_POST['serie'];
        $entregado= $_POST['entregado'];
        $mantenimiento= $_POST['mantenimiento'];
        $provedor= $_POST['proveedor'];
        $contacto= $_POST['contacto'];
        $comentarios= $_POST['comentarios'];

        // sql query for INSERT INTO Medicos
        $sql ="UPDATE `Mantenimiento` SET `Descripcion`='$descripcion',`Marca`='$marca',`Modelo`='$modelo',`NoSerie`='$noserie',`Entregado`='$entregado',`Mantenimiento`='$mantenimiento',`Provedor`='$provedor',`Contacto`='$contacto',`Comentarios`='$comentarios' where Id = $id";

        // Performs the $sql query on the server to insert the values
        if ($conn->query($sql) === TRUE) {
            $result = ['type' => "success", 'msn' => "Mantenimiento actualizado correctamente"];
        }
        else {
            $result = ['type' => "danger", 'msn' => "Problema del query"];
        }

        $conn->close();
    } catch (PDOException  $e) {
        $result = ['type' => "danger", 'msn' => "Problema de la conexión"];
    }
    $result["query"] = $sql;

    echo json_encode($result);
}
?>