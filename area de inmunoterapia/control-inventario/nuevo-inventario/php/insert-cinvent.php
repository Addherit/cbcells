<?php 
header('Content-type: application/json');
include_once('../../../../assets/db/conexion.php');

if($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        //vars from form
        $producto= $_POST['producto'];
        $presentacion= $_POST['presentacion'];
        $catalogo= $_POST['catalogo'];
        $nuevo= $_POST['inventarioNuevo'];
        $uso= $_POST['inventarioUso'];
        $entrega= $_POST['entrega'];
        $lote= $_POST['lote'];
        $caducidad= $_POST['caducidad'];
        $provedor= $_POST['provedor'];
        $contacto= $_POST['contacto'];
        $observaciones= $_POST['observaciones'];

        // sql query for INSERT INTO Medicos
        $sql = "INSERT INTO `ControlInvent`( `Producto`, `Presentacion`, `Catalogo`,`Nuevo`, `Uso`, `Entrega`, `Lote`, `Caducidad`, `Provedor`, `Contacto`,`Observaciones`, `Seccion`) VALUES ('$producto','$presentacion','$catalogo','$nuevo','$uso','$entrega','$lote','$caducidad','$provedor','$contacto','$observaciones','inmunoterapia')";
         
        // Performs the $sql query on the server to insert the values
        if ($conn->query($sql) === TRUE) {
            $result = ['type' => "success", 'msn' => "Inventario creado correctamente"];
        }
        else {
            $result = ['type' => "danger", 'msn' => "Problema del query"];
        }

        $conn->close();
    } catch (PDOException  $e) {
        $result = ['type' => "danger", 'msn' => "Problema de la conexión"];
    }
    $result["query"] = $sql;

    echo json_encode($result);
}
?>