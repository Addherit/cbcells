

document.addEventListener('DOMContentLoaded', () => {
    setTableRows(3);
})
document.querySelector('#btn-add-inventario').addEventListener('click', () => { addInmunoterapia() })
document.querySelector('#btn-lista-inventario').addEventListener('click', () => { goToListInventario() })

function setTableRows(num_rows) {

    const ROWS = getTableRows(num_rows);    
    if (num_rows == 3) {
        document.querySelector('#tbl-inventario tbody').innerHTML = ROWS;
    } else {
        $('#tbl-inventario tbody').append(ROWS);
    }

}

function getTableRows(num_filas) {

    let rows = '';
    for (let index = 0; index < num_filas; index++) {
        rows += 
        `<tr>
            <td contentEditable class="background-td-editable" onkeyup="validateLastRow(this)"></td>
            <td contentEditable class="background-td-editable"></td>
            <td contentEditable class="background-td-editable"></td>
            <td contentEditable class="background-td-editable"></td>
            <td contentEditable class="background-td-editable"></td>
            <td contentEditable class="background-td-editable"></td>
            <td contentEditable class="background-td-editable"></td>
            <td class="background-td-editable"><input type="date"></td>
            <td contentEditable class="background-td-editable"></td>
            <td contentEditable class="background-td-editable"></td>
            <td contentEditable class="background-td-editable"></td>
        </tr>`
    }
    return rows;    

}

function addInmunoterapia() {

    const TRS = document.querySelectorAll('#tbl-inventario tbody tr');
    TRS.forEach(row => {
        
        if (row.children[0].textContent != '') {
            let form_data = new FormData()
            form_data.append('producto', row.children[0].textContent);
            form_data.append('presentacion', row.children[1].textContent);
            form_data.append('catalogo', row.children[2].textContent);
            form_data.append('inventarioNuevo', row.children[3].textContent);
            form_data.append('inventarioUso', row.children[4].textContent);
            form_data.append('entrega', row.children[5].textContent);
            form_data.append('lote', row.children[6].textContent);
            form_data.append('caducidad', row.children[7].children[0].value);
            form_data.append('provedor', row.children[8].textContent);
            form_data.append('contacto', row.children[9].textContent);
            form_data.append('observaciones', row.children[10].textContent);

            fetch('php/insert-cinvent.php', { method: 'POST', body: form_data })
            .then(data => data.json())
            .then(data => {
                const MSN = new Message(data.type, data.msn);
                MSN.showMessage();
            })
        }
    });

}

function validateLastRow(td) {

    let rowIndex = td.parentElement.rowIndex
    let rowsLength = document.querySelectorAll('#tbl-inventario tbody tr').length;
    if (rowIndex == rowsLength ) {
        setTableRows(1);
    }

}

function goToListInventario() {
    window.location = `/crm/area%20de%20inmunoterapia/control-inventario/lista-inventario/index.php`;
}