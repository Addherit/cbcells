<?php 
header('Content-type: application/json');
include_once('../../../../assets/db/conexion.php');

if($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        //vars from form
        $id= $_POST['id'];
        $producto= $_POST['producto'];
        $presentacion= $_POST['presentacion'];
        $catalogo= $_POST['catalogo'];
        $nuevo= $_POST['nuevo'];
        $uso= $_POST['uso'];
        $entrega= $_POST['entrega'];
        $lote= $_POST['lote'];
        $caducidad= $_POST['caducidad'];
        $provedor= $_POST['provedor'];
        $contacto= $_POST['contacto'];
        $observaciones= $_POST['observaciones'];



        // sql query for INSERT INTO Medicos
        $sql = "UPDATE `ControlInvent` SET `Producto`='$producto',`Presentacion`='$presentacion',`Catalogo`='$catalogo',`Nuevo`='$nuevo',`Uso`='$uso',`Entrega`='$entrega',`Lote`='$lote',`Caducidad`='$caducidad',`Provedor`='$provedor',`Contacto`='$contacto',`Observaciones`='$observaciones' WHERE id=$id";
         
        // Performs the $sql query on the server to insert the values
        if ($conn->query($sql) === TRUE) {
            $result = ['type' => "success", 'msn' => "Inventario Actualizado Correctamente"];
        }
        else {
            $result = ['type' => "danger", 'msn' => "Problema del query"];
        }

        $conn->close();
    } catch (PDOException  $e) {
        $result = ['type' => "danger", 'msn' => "Problema de la conexión"];
    }
    $result["query"] = $sql;

    echo json_encode($result);
}
?>