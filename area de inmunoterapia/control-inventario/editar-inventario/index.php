<!DOCTYPE html>
<html lang="en">

<head>
  <?php include '../../../header.html' ?>
</head>

<body class="">
  <div class="wrapper ">
    <?php include '../../../sidebar/sidebar.html' ?>
    <div class="main-panel">
      <?php include '../../../sidebar/navbar.html' ?>
      <div class="content">
        <div class="container-fluid">
          <div class="row">
          <div class="col-md-10 offset-md-1">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Datos del inventario</h4>
                  <p class="card-category">Información del inventario</p>
                </div>
                <div class="card-body">
                    <div class="table-responsive" style="min-height: 100%">
                        <table class="table" id="tbl-inventario">
                            <thead class=" text-primary">                                
                                <th>Producto</th>
                                <th>Presentación</th>
                                <th>Catálogo</th>
                                <th>Inventario Nuevo</th>
                                <th>Inventario Uso</th>
                                <th>Entrega</th>
                                <th>Lote</th>
                                <th>Caducidad</th>
                                <th>Provedor</th>  
                                <th>Contacto</th>       
                                <th>Observaciones</th>                                                      
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <br> 
                    <div class="row">
                      <div class="col-12">
                        <button class="btn btn-primary float-right" id="btn-edit-inventario">Actualizar</button> 
                        <button type="button" class="btn btn-primary float-left" id="btn-lista-inventario"><span class="material-icons">arrow_back_ios</span>Lista del inventario</button>   
                      </div>
                    </div>              
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
</body>
<?php include '../../../footer.html' ?>
<script src="js/editar-inventario.js"></script>

</html>