document.addEventListener('DOMContentLoaded', () => { setInventario() })
document.querySelector('#btn-edit-inventario').addEventListener('click', () => { editItem() })
document.querySelector('#btn-lista-inventario').addEventListener('click', () => { goToListaInventario() })


function getInventario() {

const FORM_DATA = new FormData();
const PARAMS_URL = new URLSearchParams(window.location.search);
FORM_DATA.append('id', PARAMS_URL.get('id'));
    return fetch('php/select-cinvet.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        return data;
    })

}

async function setInventario() {

    const DATA = await getInventario();
    const ROWS = getRows(DATA);
    document.querySelector('#tbl-inventario tbody').innerHTML = ROWS;

}

function getRows(data) {

    let linea = ``;
    data.forEach(element => {
        linea += `
            <tr class="background-td-editable">
                <td contenteditable>${element.Producto}</td>
                <td contenteditable>${element.Presentacion}</td>
                <td contenteditable>${element.Catalogo}</td>
                <td contenteditable>${element.Nuevo}</td>
                <td contenteditable>${element.Uso}</td>
                <td contenteditable>${element.Entrega}</td>
                <td contenteditable>${element.Lote}</td>
                <td contenteditable>${element.Caducidad}</td>
                <td contenteditable>${element.Provedor}</td>
                <td contenteditable>${element.Contacto}</td>
                <td contenteditable>${element.Observaciones}</td>
            </tr>
        `
    });
    return linea;

}

function editItem() {

    const PARAMS_URL = new URLSearchParams(window.location.search);
    const FORM_DATA = new FormData();
    const TBL = document.querySelector('#tbl-inventario tbody tr')
    FORM_DATA.append('producto', TBL.children[0].textContent)
    FORM_DATA.append('presentacion', TBL.children[1].textContent)
    FORM_DATA.append('catalogo', TBL.children[2].textContent)
    FORM_DATA.append('nuevo', TBL.children[3].textContent)
    FORM_DATA.append('uso', TBL.children[4].textContent)
    FORM_DATA.append('entrega', TBL.children[5].textContent)
    FORM_DATA.append('lote', TBL.children[6].textContent)
    FORM_DATA.append('caducidad', TBL.children[7].textContent)
    FORM_DATA.append('provedor', TBL.children[8].textContent)
    FORM_DATA.append('contacto', TBL.children[9].textContent)
    FORM_DATA.append('observaciones', TBL.children[10].textContent)
    FORM_DATA.append('id', PARAMS_URL.get('id'))

    fetch('php/edit-cinvent.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
    })

}

function goToListaInventario() {
    window.location = `/crm/area de inmunoterapia/control-inventario/lista-inventario/index.php`;
}