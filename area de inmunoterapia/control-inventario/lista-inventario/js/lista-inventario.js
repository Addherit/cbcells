document.addEventListener('DOMContentLoaded', () => { 
    setInvetarios();
})

document.querySelector('#btn-new-inventario').addEventListener('click', () => { goToNewInventario() })


function goToNewInventario() {
    window.location = `/crm/area de inmunoterapia/control-inventario/nuevo-inventario/index.php`;
}

function getInventarios() {
    
    return fetch('php/select-cinvent.php')
    .then(data => data.json())
    .then(data => {
        return data
    })

}

async function setInvetarios() {

    const DATA = await getInventarios();
    const ROWS = getRows(DATA);
    document.querySelector('#tbl-cinvent tbody').innerHTML = ROWS;
}

function getRows(data) {

    let linea = '';
    data.forEach(inventario => {
        linea += `
            <tr>
                <td><a href="#" class="text-danger btn-tbl-accion" onclick="deleteItem(${inventario.id})"><span class="material-icons">delete</span> Eliminar</a></td>
                <td><a href="#" class="text-info btn-tbl-accion" onclick="editItem(${inventario.id})"><span class="material-icons">visibility</span> Ver</a></td>
                <td>${inventario.Producto}</td>
                <td>${inventario.Presentacion}</td>
                <td>${inventario.Catalogo}</td>
                <td>${inventario.Nuevo}</td>
                <td>${inventario.Uso}</td>
                <td>${inventario.Entrega}</td>
                <td>${inventario.Lote}</td>
                <td>${inventario.Caducidad}</td>
                <td>${inventario.Provedor}</td>
                <td>${inventario.Contacto}</td>
                <td>${inventario.Observaciones}</td>
            </tr>
        `
    });
    return linea;

}

function deleteItem(item_id) {
    
    const FORM_DATA = new FormData()
    FORM_DATA.append('Id', item_id)
    fetch('php/delete-cinvent.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();        
        setInvetarios();
    })
}

function editItem(item_id) {
    window.location = `/crm/area de inmunoterapia/control-inventario/editar-inventario/index.php?id=${item_id}`;
}