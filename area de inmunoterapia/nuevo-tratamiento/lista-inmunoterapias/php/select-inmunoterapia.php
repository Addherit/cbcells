<?php 
header('Content-type: application/json');
include_once('../../../../assets/db/conexion.php');

try {
  $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $final_data=[];

  $query = "SELECT i.`Id`,i.`FirstName`,i.`LastName`,i.`Edad`,i.`Diagnostico`,i.`Sexo`, i.`FechaObtencion`,i.`ComposicionMuestra`,i.`OrigenMuestra`,i.`NameFamiliar`, i.`SexoFamiliar`,i.`EdadFamiliar`,Concat(d.Nombre,' ',d.Apellido) 'Medico',i.`PrimerDosis`,i.`SegundaDosis`,i.`Cedula`,i.`CodigoReferencia` FROM `Inmunoterapia` i left join Doctores d on i.`Medico` = d.DocId WHERE i.Estado = 'Activo' ";
  $con->exec("set names utf8");
  $datos = $con->query($query)->fetchAll(PDO::FETCH_ASSOC );

  foreach($datos as $row){
    $final_data[] = $row;
  }
    echo json_encode($final_data);
} catch(PDOException $e) {
    echo $result = ["mensaje" => "Error: ".$e->getMessage()];
}
?>