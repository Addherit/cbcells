document.addEventListener('DOMContentLoaded', () => {
    setTerapias('general');
})

document.querySelector('#btn-new-inmunoterapia').addEventListener('click', () => { goToNewInmunoterapia() });
document.querySelector('#form-search').addEventListener('submit', (event) => { event.preventDefault(); setTerapias('buscador') });

function goToNewInmunoterapia() {
    window.location = `/crm/area de inmunoterapia/nuevo-tratamiento/nueva-inmunoterapia/index.php`;
}

function getTerapias() {

    return fetch('php/select-inmunoterapia.php')
    .then(data => data.json())
    .then(data => {
        return data;
    })

}

function serchTerapias() {

    const FORM_DATA = new FormData(document.querySelector('#form-search')) //send search-field
    return fetch('php/select-inmunoterapia.php', {method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        return data;
    })

}

async function setTerapias(type) {
    let data = ``;
    if (type == 'buscador') { data = await serchTerapias() }
    if (type == 'general') { data = await getTerapias() }

    const ROWS = getRowsTerapias(data);
    document.querySelector('#tbl-inmunoterapia tbody').innerHTML = ROWS;
   

}

function getRowsTerapias(data) {

    let linea = ``;
    
    data.forEach(element => {
        linea += `
        <tr>
            <td><a href="#" class="text-danger btn-tbl-accion" onclick="deleteInmunoterapia(${element.Id})"><span class="material-icons">delete</span> Eliminar</a></td>
            <td><a href="#" class="text-info btn-tbl-accion" onclick="editInmunoterapia(${element.Id})"><span class="material-icons">create</span> Editar</a></td>
            <td>${element.Id}</td>
            <td>${element.FirstName} ${element.LastName}</td>
            <td>${element.Diagnostico}</td>
            <td>${element.Edad}</td>
            <td>${element.Medico}</td>
            <td>${element.PrimerDosis}</td>
            <td>${element.SegundaDosis}</td>
            <td>${element.CodigoReferencia}</td>
        </tr>
        `;
    });
    return linea;

}

function deleteInmunoterapia(id) {

    var r = confirm("Seguro que desea eliminarla?");
    if (r == true) {
        const FORM_DATA = new FormData()
        FORM_DATA.append('Id', id);
        fetch('php/delete-inmunoterapia.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
            const MSN = new Message(data.type, data.msn);
            MSN.showMessage();
            setTerapias();
        })
    }

}

function editInmunoterapia(id) {
    window.location = `/crm/area de inmunoterapia/nuevo-tratamiento/editar-inmunoterapia/index.php?id=${id}`;
}

