<!DOCTYPE html>
<html lang="en">

<head>
  <?php include '../../../header.html' ?>
</head>

<body class="">
  <div class="wrapper ">
    <?php include '../../../sidebar/sidebar.html' ?>
    <div class="main-panel">
      <?php include '../../../sidebar/navbar.html' ?>
      <div class="content">
        <div class="container-fluid">
          <div class="row" style="width: 100%">
            <div class="col-md-5 offset-md-7">
              <form class="navbar-form" id="form-search">
                <div class="input-group no-border">
                  <input type="text" name="search-field" class="form-control" placeholder="Buscar Inmunoterapia">
                  <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                  </button>
                </div>
              </form>
            </div>
          </div>      
          <div class="row">
          <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Inmunoterapias</h4>
                </div>
                <div class="card-body">
                  <div class="table-responsive" style="min-height: 100%">
                      <table class="table" id="tbl-inmunoterapia">
                          <thead class=" text-primary">                                
                            <th colspan="2"></th>
                            <th>ID</th>
                            <th>Paciente</th>
                            <th>Diagnostico</th>
                            <th>Edad</th>
                            <th>Médico</th>
                            <th>1ra Dósis</th> 
                            <th>2da Dósis</th> 
                            <th>Código de referencia</th>
                          </thead>
                          <tbody></tbody>
                      </table>
                  </div> 
                  <div class="row">
                      <div class="col-12">
                          <button class="btn btn-primary float-right" id="btn-new-inmunoterapia"><span class="material-icons">add</span>Nueva Inmunoterapia</button>                                                                   
                      </div>
                  </div>           
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
</body>
<?php include '../../../footer.html' ?>
<script src="js/inmunoterapia.js"></script>

</html>