<?php 
header('Content-type: application/json');
include_once('../../../../assets/db/conexion.php');

if($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        //vars from form
        $medico= $_POST['medico'];
        $nombre= $_POST['nombres'];
        $apellido= $_POST['apellidos'];
        $edad= $_POST['edad'];
        $diagnostico= $_POST['diagnostico'];
        $sexo= $_POST['sexo'];
        $fecha_obtencion= $_POST['fecha-obtencion'];
        $composicion= $_POST['compsicion-muestra'];
        $origen= $_POST['origen-muestra'];
        $nombref= $_POST['nombre-donante'];
        $sexof= $_POST['sexo-familiar'];
        $primeradosis= $_POST['primera-dosis'];
        $edadf= $_POST['edad-familiar'];
        $codigoreferencia= $_POST['codigo-referencia'];
        $primeradosis= $_POST['primera-dosis'];
        $segundadosis= $_POST['segunda-dosis'];

        // sql query for INSERT INTO Paciente
        $sql ="INSERT INTO `Inmunoterapia`( `FirstName`, `LastName`, `Edad`, `Diagnostico`,`Sexo`, `FechaObtencion`, `ComposicionMuestra`, `OrigenMuestra`,`NameFamiliar`, `SexoFamiliar`, `EdadFamiliar`, `Medico`, `PrimerDosis`, `SegundaDosis`, `CodigoReferencia`) VALUES ('$nombre','$apellido','$edad','$diagnostico','$sexo','$fecha_obtencion','$composicion','$origen','$nombref','$sexof','$edadf','$medico','$primeradosis','$segundadosis','$codigoreferencia')";
        

        // Performs the $sql query on the server to insert the values
        if ($conn->query($sql) === TRUE) {

            $result = ['type' => "success", 'msn' => "Inmunoterapia creado correctamente"];
        }
        else {
            $result = ['type' => "danger", 'msn' => "Problema del query"];
        }

        $conn->close();
    } catch (PDOException  $e) {
        $result = ['type' => "danger", 'msn' => "Problema de la conexión"];
    }
    $result["query"] = $sql;

    echo json_encode($result);
}
?>