

document.addEventListener('DOMContentLoaded', () => { getDateNow(); setMedicosSelectList(); rellenarSelectEdad();})
document.querySelector('#form-add-inmunoterapia').addEventListener('submit', (event) => {
    event.preventDefault();
    insertInmunoterapia();
})
document.querySelector('#btn-lista-inmunoterapia').addEventListener('click', () => { goToListInmunoterapia() })

function getDateNow() {

    setTimeout(() => {
        document.querySelector('[name=fecha-obtencion]').parentElement.classList.add('is-filled');
        document.querySelector('[name=primera-dosis]').parentElement.classList.add('is-filled');
        document.querySelector('[name=segunda-dosis]').parentElement.classList.add('is-filled');
        document.querySelector('[name=fecha]').parentElement.classList.add('is-filled');
    }, 400);
    
    setInterval(() => { 
        document.querySelector('[name=fecha]').value = moment().format('DD-MM-YYYY h:mm:ss a');
    }, 1000);

}

function insertInmunoterapia() {

    const FORM_DATA = new FormData(document.querySelector('#form-add-inmunoterapia'))

    fetch('php/insert-inmunoterapia.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
    })

}

function goToListInmunoterapia() {
    window.location = `/crm/area de inmunoterapia/nuevo-tratamiento/lista-inmunoterapias/index.php`;
}

function getMedicos() {

    return fetch('php/select-doctor.php')
    .then(data => data.json())
    .then(data => {
        return data;
    });

}

async function setMedicosSelectList() {

    const MEDICOS = await getMedicos();
    const OPTIONS = getOptiosMedicos(MEDICOS);
    document.querySelector('[name=medico]').innerHTML = OPTIONS;

}

function getOptiosMedicos(medicos) {

    let options = `<option selected disabled>Selecciona un médico</option>`;
    medicos.forEach(medico => {
        options += `<option value="${medico.DocId}">${medico.Nombre} ${medico.Apellido}</option>`;        
    });
    return options;

}

function rellenarSelectEdad() {
    
    let options = `<option value="" selected disabled></option>`
    for (let index = 0; index <= 100; index++) {
        options += `<option value="${index}">${index}</option>`        
    }
    document.querySelector('[name=edad]').innerHTML = options;
    document.querySelector('[name=edad-familiar]').innerHTML = options;

}