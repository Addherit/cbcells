<?php 
header('Content-type: application/json');
include_once('../../../../assets/db/conexion.php');

if($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        //vars from form
        $id= $_POST['id'];
        $medico= $_POST['medico'];
        $nombre= $_POST['nombres'];
        $apellido= $_POST['apellidos'];
        $edad= $_POST['edad'];
        $diagnostico= $_POST['diagnostico'];
        $sexo= $_POST['sexo'];
        $fecha_obtencion= $_POST['fecha-obtencion'];
        $composicion= $_POST['compsicion-muestra'];
        $origen= $_POST['origen-muestra'];
        $nombref= $_POST['nombre-donante'];
        $sexof= $_POST['sexo-familiar'];
        $edadf= $_POST['edad-familiar'];
        $codigo= $_POST['codigo-referencia'];
        $primeradosis= $_POST['primera-dosis'];
        $segundadosis= $_POST['segunda-dosis'];

        // sql query for INSERT INTO Paciente
        $sql ="UPDATE `Inmunoterapia` SET `Medico`='$medico',`FirstName`='$nombre',`LastName`='$apellido',`Edad`='$edad',`Diagnostico`='$diagnostico',`Sexo`='$sexo',`FechaObtencion`='$fecha_obtencion', `ComposicionMuestra`='$composicion',`OrigenMuestra`='$origen',`NameFamiliar`='$nombref',`SexoFamiliar`='$sexof',`EdadFamiliar`='$edadf',`CodigoReferencia`='$codigo', `PrimerDosis`='$primeradosis',`SegundaDosis`='$segundadosis' WHERE Id= $id";
        


        // Performs the $sql query on the server to insert the values
        if ($conn->query($sql) === TRUE) {

            $result = ['type' => "success", 'msn' => "Inmunoterapia Actualizada correctamente"];
        }
        else {
            $result = ['type' => "danger", 'msn' => "Problema del query"];
        }

        $conn->close();
    } catch (PDOException  $e) {
        $result = ['type' => "danger", 'msn' => "Problema de la conexión"];
    }
    $result["query"] = $sql;

    echo json_encode($result);
}
?>