<!DOCTYPE html>
<html lang="en">

<head>
  <?php include '../../../header.html' ?>
</head>

<body class="">
  <div class="wrapper ">
    <?php include '../../../sidebar/sidebar.html' ?>
    <div class="main-panel">
      <?php include '../../../sidebar/navbar.html' ?>
      <div class="content">
        <div class="container-fluid">
          <div class="row">
          <div class="col-md-10 offset-md-1">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Solicitud de Inmunoterapia</h4>
                  <p class="card-category">Ingresa la información del paciente</p>
                </div>
                <div class="card-body">
                  <form id="form-update-inmunoterapia">

                    <div class="row">
                      <div class="col-md-5 offset-md-7">
                        <div class="form-group">
                          <label class="bmd-label-floating">Fecha</label>
                          <input type="text" class="form-control" name="fecha" readonly style="background-color: white">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <h3>Datos del médico</h3>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-10">
                        <div class="form-group select-custom">
                          <label class="bmd-label-floating">Nombre del médico</label>                          
                          <select class="custom-select" name="medico"></select>
                        </div>                      
                      </div>                     
                      <div class="col-md-2">
                        <div class="form-group select-custom">
                          <label class="bmd-label-floating">Cedula profesional</label>                          
                          <input type="text" class="form-control" name="cedula-profesional">
                        </div>                      
                      </div>                     
                    </div>  
                    <div class="row">
                      <div class="col-md-12">
                        <h3>Datos del paciente</h3>
                      </div>
                    </div>                                                          
                    <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Nombres</label>
                          <input type="text" class="form-control" name="nombres">
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Apellidos</label>
                          <input type="text" class="form-control" name="apellidos">
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group select-custom">
                          <label class="bmd-label-floating">Edad</label>
                          <select class="custom-select" name="edad">                          
                          </select>
                        </div>
                      </div>  
                      <div class="col-md-9">
                        <div class="form-group">
                          <label class="bmd-label-floating">Diagnostico</label>
                          <input type="text" class="form-control" name="diagnostico">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group select-custom">
                          <label class="bmd-label-floating">Sexo</label>                          
                          <select class="custom-select" name="sexo">
                            <option value="" selected disabled></option>
                            <option value="Hombre">Hombre</option>
                            <option value="Mujer">Mujer</option>
                          </select>
                        </div>                      
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <h3>Información de la muestra</h3>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5 offset-md-7">
                        <div class="form-group">
                          <label class="bmd-label-floating">Fecha de obtención</label>
                          <input type="date" class="form-control" name="fecha-obtencion">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group select-custom">
                          <label class="bmd-label-floating">Composición de la Muestra</label>                          
                          <select class="custom-select" name="compsicion-muestra">
                            <option value="" selected disabled></option>
                            <option value="Concentrado Leucocitario">Concentrado Leucocitario</option>
                            <option value="Sangre Total">Sangre Total</option>
                          </select>
                        </div>
                      </div>     
                      <div class="col-md-6">
                        <div class="form-group select-custom">
                          <label class="bmd-label-floating">Origen de la muestra</label>                          
                          <select class="custom-select" name="origen-muestra">
                            <option value="" selected disabled></option>
                            <option value="Propia">Propia</option>
                            <option value="Familiar">Familiar</option>
                            <option value="Banco de sangre">Donante sano</option>
                          </select>
                        </div>
                      </div>    
                    </div>
                    <div class="row">
                      <div class="col-md-8">
                        <div class="form-group">
                          <label class="bmd-label-floating">Nombre del donante</label>
                          <input type="text" class="form-control" name="nombre-donante">
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group select-custom">
                          <label class="bmd-label-floating">Sexo</label>
                          <select class="custom-select" name="sexo-familiar">
                            <option value="" selected disabled></option>
                            <option value="Masculino">Masculino</option>
                            <option value="Femenino">Femenino</option>
                            <option value="Incluyente">Incluyente</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group select-custom">
                          <label class="bmd-label-floating">Edad</label>
                          <select class="custom-select" name="edad-familiar">                          
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Código referencia</label>
                          <input type="text" class="form-control" name="codigo-referencia">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">1ra Dosis</label>
                          <input type="date" class="form-control" name="primera-dosis">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">2da Dosis</label>
                          <input type="date" class="form-control" name="segunda-dosis">
                        </div>
                      </div>
                    </div>
                    <br> 
                    <div class="row">
                      <div class="col-12">
                        <button type="submit" class="btn btn-primary pull-right"><span class="material-icons">save_alt</span>Actualizar datos</button>                      
                        <button type="button" class="btn btn-primary float-left" id="btn-lista-inmunoterapia"><span class="material-icons">arrow_back_ios</span>Lista de inmunoterapias</button>
                        <button type="button" class="btn btn-primary pull-right float-left" id="btn-pdf"><span class="material-icons">content_copy</span> PDF</button>
                      </div>
                    </div>                     
                    
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
</body>
<?php include '../../../footer.html' ?>
<script src="js/inmunoterapia.js"></script>

</html>