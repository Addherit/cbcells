document.addEventListener('DOMContentLoaded', () => { 

    setMedicosSelectList(); 
    rellenarSelectEdad();
    setTimeout(() => {
        setInmunoterapia();
    }, 800);

})

document.querySelector('#form-update-inmunoterapia').addEventListener('submit', (event) => {
    event.preventDefault();
    updateInmunoterapia();
})
document.querySelector('#btn-lista-inmunoterapia').addEventListener('click', () => { goToListInmunoterapia() })
document.querySelector('#btn-pdf').addEventListener('click', () => { getPDF() })

function goToListInmunoterapia() {
    window.location = `/crm/area de inmunoterapia/nuevo-tratamiento/lista-inmunoterapias/index.php`;
}

function getMedicos() {

    return fetch('php/select-doctor.php')
    .then(data => data.json())
    .then(data => {
        return data;
    });

}

async function setMedicosSelectList() {

    const MEDICOS = await getMedicos();
    const OPTIONS = getOptiosMedicos(MEDICOS);
    document.querySelector('[name=medico]').innerHTML = OPTIONS;

}

function getOptiosMedicos(medicos) {

    let options = `<option selected disabled>Selecciona un médico</option>`;
    medicos.forEach(medico => {
        options += `<option value="${medico.DocId}">${medico.Nombre} ${medico.Apellido}</option>`;        
    });
    return options;

}

function rellenarSelectEdad() {
    
    let options = `<option value="" selected disabled></option>`
    for (let index = 0; index <= 100; index++) {
        options += `<option value="${index}">${index}</option>`        
    }
    document.querySelector('[name=edad-familiar]').innerHTML = options;
    document.querySelector('[name=edad]').innerHTML = options;

}

function getInmunoterapia(id) {

    const FORM_DATA = new FormData()
    FORM_DATA.append('Id', id)
    return fetch('php/select-inmunoterapia.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        return data[0];
    })

}

async function setInmunoterapia() {
    
    addClassIsFilled()
    const PARAMS_URL = new URLSearchParams(window.location.search);
    const DATA = await getInmunoterapia(PARAMS_URL.get('id'));

    document.querySelector('[name=fecha]').value = DATA.Deatecreated
    document.querySelector('[name=medico]').value = DATA.Medico
    document.querySelector('[name=cedula-profesional]').value = DATA.Cedula
    document.querySelector('[name=nombres]').value = DATA.FirstName
    document.querySelector('[name=apellidos]').value = DATA.LastName
    document.querySelector('[name=edad]').value = DATA.Edad
    document.querySelector('[name=diagnostico]').value = DATA.Diagnostico
    document.querySelector('[name=sexo]').value = DATA.Sexo
    document.querySelector('[name=fecha-obtencion]').value = DATA.FechaObtencion
    document.querySelector('[name=compsicion-muestra]').value = DATA.ComposicionMuestra
    document.querySelector('[name=origen-muestra]').value = DATA.OrigenMuestra
    document.querySelector('[name=nombre-donante]').value = DATA.NameFamiliar
    document.querySelector('[name=sexo-familiar]').value = DATA.SexoFamiliar
    document.querySelector('[name=edad-familiar]').value = DATA.EdadFamiliar
    document.querySelector('[name=codigo-referencia]').value = DATA.CodigoReferencia
    document.querySelector('[name=primera-dosis]').value = DATA.PrimerDosis
    document.querySelector('[name=segunda-dosis]').value = DATA.SegundaDosis

}

function updateInmunoterapia() {

    const PARAMS_URL = new URLSearchParams(window.location.search);
    const FORM_DATA = new FormData(document.querySelector('#form-update-inmunoterapia'));
    FORM_DATA.append('id', PARAMS_URL.get('id'))

    fetch('php/update-inmunoterapia.php', {method: "POST", body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
        const MSN = new Message(data.type, data.msn);
        MSN.showMessage();
    })

}


function addClassIsFilled() {

    //funcion para hacer el efecto de focus en input de datos
    const DIVS = document.querySelectorAll('.bmd-form-group');
    DIVS.forEach(element => {
        element.classList.add('is-filled');        
    });

}

function getPDF() {

    const FORM_DATA = new FormData(document.querySelector('#form-update-inmunoterapia'))
    FORM_DATA.append('title', 'SOLICITUD DE INMUNOTERAPIA');
    FORM_DATA.append('plantilla_type', 'inmuno');
    fetch('/crm/assets/js/plugins/mpdf/print.php', { method: 'POST', body: FORM_DATA })
    .then( data => {
        if(data.status == 200) {
            window.open('/crm/assets/js/plugins/mpdf/formato.pdf','_blanck'); 
            
            // class DeleteFile needs URL parameter
            const FILE = new DeleteFile('../js/plugins/mpdf/formato.pdf');
            FILE.deteleFile();
        }
    })

    
}